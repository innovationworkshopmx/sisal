<?php
  $lat = $_POST['lat'];
  $lng = $_POST['lng']
?>
<script>
    var lat = <?php echo $lat ?>;
    var lng = <?php echo $lng ?>;
    function initialize() {
      //var fenway = {lat: 20.5922759, lng: -100.38529249999999};
      var fenway = {lat: lat, lng: lng};
      var map = new google.maps.Map(document.getElementById('map'), {
        center: fenway,
        zoom: 14
      });
      var panorama = new google.maps.StreetViewPanorama(
          document.getElementById('pano'), {
            position: fenway,
            pov: {
              heading: 34,
              pitch: 10
            }
          });
      map.setStreetView(panorama);
    }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2QpIePlSoi0C6XzY3qJp7egB9BJfSS1M&signed_in=true&callback=initialize"></script>
</script>