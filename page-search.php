<?php 
	get_header(); 
	wp_reset_query();
	//login
	global $current_user; 
	$display_name = $current_user->display_name;
	$role_user = $current_user->roles[0];
	//obtener get manuales;
	$tipog = $_GET['tipo'];
	$zonagob = $_GET['zona'];
	$recamarasg = $_GET['recamaras'];
	$preciog = $_GET['price'];
	$bag = $_GET['ba'];
	$terrenog = $_GET['terreno'];
	$superficieg = $_GET['construnccion'];
	$price_tipog = $_GET['price_tipo'];
	$ordenag = $_GET['ordenar'];
	//obtener link actual
	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$exp_link = explode("?", $actual_link);
	$gets = explode("&", $exp_link[1]);
	//guardar link actual en session
	$_SESSION['urls'] =$actual_link; 
	//
	$contado_cf = 0;
	$attributos_array = array();
	foreach ($gets as $get) {
		$exp_get = explode("=", $get);
		if($exp_get[0]!="Tipo" && $exp_get[0]!="tipo" && $exp_get[0]!="zona"){
			$atributos = array(
				'name'	  	=> $exp_get[0],
				'value'	  	=> $exp_get[1],
			);
	        $attributos_array[] = $atributos;
			$contado_cf++;
		}
	}
	if($contado_cf>1){
		$relation_cf = 'AND';
	}else{
		$relation_cf = 'OR';
	}
	$array_cf = array();
	//recamaras
	if($recamarasg!=""){
		if($recamarasg=="rang4"){
			$cf_ele = array(
				'key'	  	=> 'recamaras',
				'value'	  	=> 7,
				'compare' 	=> '>=',
			);
		}else{
			$expl_precio = explode("_", $recamarasg);
			$rag1 = $expl_precio[0];
			$rag2 = $expl_precio[1];
			$cf_ele = array(
				'key'	  	=> 'recamaras',
				'value'	  	=> array( $rag1, $rag2 ),
				'compare' 	=> 'BETWEEN',
				'type' => 'NUMERIC'
			);
	    }
	    $array_cf[] = $cf_ele;
	}
	//baños
	if($bag!=""){
		if($bag=="opc5"){
			$cf_ele = array(
				'key'	  	=> 'ba',
				'value'	  	=> 6,
				'compare' 	=> '>',
			);
		}else{
			$expl_precio = explode("_", $bag);
			$rag1 = $expl_precio[0];
			$rag2 = $expl_precio[1];
			$cf_ele = array(
				'key'	  	=> 'ba',
				'value'	  	=> array( $rag1, $rag2 ),
				'compare' 	=> 'BETWEEN',
				'type' => 'NUMERIC'
			);
	    }
	    $array_cf[] = $cf_ele;
	}
	//precio
	if($preciog!=""){
		if($tipog!=""){
			$term_tipo = get_term_by( 'id', $tipog, "Tipo" );
			if($term_tipo->parent!=0){
				$term_tipo_parent = get_term_by( 'id', $term_tipo->parent, "Tipo" );
				$slugf = $term_tipo_parent->slug;
			}else{
				$slugf = $term_tipo->slug;
			}
		}else{
			$slugf = "venta";
		}
		if($slugf=="renta"){
			$key = 'precio_renta';
		}
		if($slugf=="venta"){
			$key = 'precio';
		}
		$expl_precio = explode("_", $preciog);
		$rag1 = $expl_precio[0];
		$rag2 = $expl_precio[1];
		$cf_price = array(
			'key'	  	=> $key,
			'value'	  	=> array( $rag1, $rag2 ),
			'compare' 	=> 'BETWEEN',
			'type' => 'NUMERIC'
		);
		$array_cf[] = $cf_price;
	}
	//terreno
	if($terrenog!=""){
		$expl_terr = explode("_", $terrenog);
		$rag1 = $expl_terr[0];
		$rag2 = $expl_terr[1];
		$cf_price = array(
			'key'	  	=> 'temperatura',
			'value'	  	=> array( $rag1, $rag2 ),
			'compare' 	=> 'BETWEEN',
			'type' => 'NUMERIC'
		);
		$array_cf[] = $cf_price;
	}
	//superficie
	if($superficieg!=""){
		$expl_constr = explode("_", $superficieg);
		$rag1 = $expl_constr[0];
		$rag2 = $expl_constr[1];
		$cf_price = array(
			'key'	  	=> 'superficie_construccion',
			'value'	  	=> array( $rag1, $rag2 ),
			'compare' 	=> 'BETWEEN',
			'type' => 'NUMERIC'
		);
		$array_cf[] = $cf_price;
	}
	//tipo zona
	if(is_numeric($zonagob)){
		$zonag=$zonagob;
	}else{
		$explode=explode(",", $zonagob);
			$tam=count($explode);
			if($tam>=2){
				$get=$tam-1;
				$zona_hijo=$explode[0];
				$zona_hijo = trim($zona_hijo);
				$zona_padre=$explode[$get];
				$zona_padre = trim($zona_padre);
				$zonaP = get_term_by( 'name', $zona_padre, 'zona' );
				$idzonaP = $zonaP->term_id;
				//$name_padre_z = $zonaP->name;
				$taxonomy_name = 'zona';
				$termchildren = get_term_children( $idzonaP, $taxonomy_name );
				foreach ( $termchildren as $child ) {
					$term = get_term_by( 'id', $child, $taxonomy_name );
					$nombre = $term->name;
					if($nombre==$zona_hijo){
						$zonag = $term->term_id;
					}
				}
			}else{
				$zona1 = get_term_by( 'name', $zonagob, 'zona' );
				$zonag = $zona1->term_id;
			}
	}
	//ordenar
	$ordenar_por = "";
	$up_dow = "";
	$tipo_or = "";
	$orby = "";
	if($ordenag!=""){
		$exp_ord = explode("_", $ordenag);
		$tipo_or = $exp_ord[1];
		if($exp_ord[1]=="price"){
			$orde_x = "precio";
			$orby = "meta_value_num";
		}
		if($exp_ord[1]=="terreno"){
			$orde_x = "temperatura";
			$orby = "meta_value_num";
		}
		if($exp_ord[1]=="construnccion"){
			$orde_x = "superficie_construccion";
			$orby = "meta_value_num";
		}
		if($exp_ord[1]=="recamaras"){
			$orde_x = "recamaras";
			$orby = "meta_value_num";
		}
		if($exp_ord[1]=="recamaras"){
			$orde_x = "recamaras";
			$orby = "meta_value_num";
		}
		if($exp_ord[1]=="fecha"){
			$orde_x = "";
			$orby = "post_date";
		}
		if($exp_ord[1]=="lugar"){
			$orde_x = "";
			$orby = "zona";
		}
		$ordenar_por = $orde_x;
		$up_dow = $exp_ord[0];
	}
	//obtener get
	$arra_post = array();
	foreach($_GET as $nombre_campo => $valor){
	   $asignacion = "\$" . $nombre_campo . "='" . $valor . "';";
	   array_push($arra_post, $valor);
	   eval($asignacion);
	}
	//obtener todas las taxonomies
	$taxonomies = get_taxonomies();
	$all_tax=array();
	foreach ($taxonomies as $tax) {
		if($tax!='category' && $tax!="post_tag" && $tax!="nav_menu" && $tax!="link_category" && $tax!="post_format"){
			array_push($all_tax, $tax);
		}
	}
	//creacion de array concepts (que contiene los datos de las categorias obtenidas en el GET)
	$juntar = array();
	$concepts = array();
	$id_term = "";
	if($tipog!=""){
		$term = get_term_by( 'id', $tipog, "Tipo" );
		$id_term = $tipog;
		$concept = array( 
            'term_id' => $term->term_id,
            'name' => $term->name,
            'taxonomy' => 'Tipo',
            'parent' => $term->parent,
            'count' => $term->count
        );
        $concepts[] = $concept;
        //array_push($concepts, $concept);
	}
	if($zonag!=""){
		if(is_numeric($zonag)){
			$id_term = $zonag;
		}else{
			$explode=explode(",", $zonag);
			$tam=count($explode);
			if($tam>=2){
				$get=$tam-1;
				$zona_hijo=$explode[0];
				$zona_hijo = trim($zona_hijo);
				$zona_padre=$explode[$get];
				$zona_padre = trim($zona_padre);
				$zonaP = get_term_by( 'name', $zona_padre, 'zona' );
				$idzonaP = $zonaP->term_id;
				//$name_padre_z = $zonaP->name;
				$taxonomy_name = 'zona';
				$termchildren = get_term_children( $idzonaP, $taxonomy_name );
				foreach ( $termchildren as $child ) {
					$term = get_term_by( 'id', $child, $taxonomy_name );
					$nombre = $term->name;
					if($nombre==$zona_hijo){
						$id_term = $term->term_id;
					}
				}
			}else{
				$zona1 = get_term_by( 'name', $zonag, 'zona' );
				$id_term = $zona1->term_id;
			}
		}
		$term = get_term_by( 'id', $id_term, "zona" );
		$id_term = $tipog;
		$concept2 = array( 
            'term_id' => $term->term_id,
            'name' => $term->name,
            'taxonomy' => $term->taxonomy,
            'parent' => $term->parent,
            'count' => $term->count
        );
        $concepts[] = $concept2;
	}
	//creacion de array loop
	$myloops = array();
	foreach ($concepts as $key ) {
		if($key['term_id']!=""){
			$myloop = [
                'taxonomy' => $key['taxonomy'],
				'field' => 'id',
				'terms' => $key['term_id']
            ];
            $myloops [] = $myloop;
		}
	}
	//Query a base de datos
	if($zonag==""&$tipo==""){
		//si no hay categorias seleccionadas muestra todas las propiedades
		$args = array(
					'post_type' => 'propiedades',
					'meta_key'    => $ordenar_por,
					'orderby'			=> $orby,
					'order'      => $up_dow,
					'meta_query'	=> array(
					'relation'		=> $relation_cf,
					$array_cf,
					),
				);
		$myquery1 = new WP_Query($args);
		//$myquery1 = new WP_Query("post_type=propiedades");
	}else{
			// de lo contrario hace la consulta mendiante la información del array $myloops (el cual se genera por medio de los gets obtenidos)
			$args = array(
				'post_type' => 'propiedades',
				'meta_key'    => $ordenar_por,
				'orderby'			=> $orby,
				'order'      => $up_dow,
				'tax_query' => $myloops,
				'meta_query'	=> array(
				'relation'		=> $relation_cf,
				$array_cf,
			),
		);
		$myquery1 = new WP_Query($args);
	}
	//contar elementos resultante de la busqueda
	$contelementos=0;
	if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
		$contelementos++;
	endwhile; endif;
?>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin div-busqueda">
	<div class="large-3 medium-3 small-12 columns">
		<?php 
		$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
		foreach ($crumbs as $key => $crumb) {
			if($crumb=="sisal"){ ?>
			  <a class="tipografia blue" href="<?php echo home_url(); ?>">Home/</a>
			<?php
			}else{ 
				
					
				if($crumb=="search"){ ?>
				<a class="tipografia gray-light">Búsqueda</a>
		<?php
				}
			}
		}
		?>
		<div class="large-12 medium-12 small-12 columns">
			<h3 class="tipografia gray info-prop light"><i class="fa fa-angle-double-right right-filtros"></i> Filtros Aplicados</h3>
			<a class="show-filtrado">Ver filtros</a>
		</div>
		<div class="large-12 medium-12 small-12 columns">
			<div class="p-left">
				<?php 	
						// muestra las categorias que se encuentran seleccionadas
						// la clase delet se obtiene mediante JS y se toma su id que contiene el tipo de taxonomia y su categoria padre.
						// si no tiene padre se deja en vacio, para que en la logica de JS se tome los valores de la categoria de su tipo que se encuentra seleccionada actualmente.
						foreach ($concepts as $key ) { 
							if($key['term_id']!=""){ 
								if($key['parent']!=0){ ?>
									<a class="tipografia gray clav-search delet" id="<?php echo $key['taxonomy'] ?>_<?php echo $key['parent']; ?>"><?php echo $key['name']; ?> <i class="fa fa-times"></i></a>
								<?php
								}else{ ?>
									<a class="tipografia gray clav-search delet" id="<?php echo $key['taxonomy'] ?>_"><?php echo $key['name']; ?> <i class="fa fa-times"></i></a>
								<?php
								}
							} 
						}
						foreach ($attributos_array as $attr_cf) { 
							if($attr_cf['name']=="price"){ 
								$explode = explode("_", $attr_cf['value']); ?>
								<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>">$<?php echo number_format($explode[0],2); ?> - $<?php echo number_format($explode[1],2); ?> <i class="fa fa-times"></i></a>
							<?php
							} 
							if($attr_cf['name']=="recamaras"){ 
								$explode = explode("_", $attr_cf['value']); 
								if($explode[0]!="rang4"){ ?>
								<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>"><?php echo $explode[0]; ?> - <?php echo $explode[1]; ?> Recamaras <i class="fa fa-times"></i></a>
							<?php }else{ ?>
									<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>">Más de 7 Recamaras <i class="fa fa-times"></i></a>
							<?php
									}
							}
							if($attr_cf['name']=="ba"){ 
								$explode = explode("_", $attr_cf['value']); 
								if($explode[0]!="opc5"){ ?>
								<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>"><?php echo $explode[0]; ?> - <?php echo $explode[1]; ?> Baños <i class="fa fa-times"></i></a>
							<?php }else{ ?>
									<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>">Más de 5 Baños <i class="fa fa-times"></i></a>
							<?php
									}
							}
							if($attr_cf['name']=="terreno"){ 
								$explode = explode("_", $attr_cf['value']);  ?>
								<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>"><?php echo $explode[0]; ?> - <?php echo $explode[1]; ?> m2 de terreno<i class="fa fa-times"></i></a>
							<?php
							}
							if($attr_cf['name']=="construnccion"){ 
								$explode = explode("_", $attr_cf['value']);  ?>
								<a class="tipografia gray clav-search delet-cf" id="<?php echo $attr_cf['name'] ?>/<?php echo $attr_cf['value']; ?>"><?php echo $explode[0]; ?> - <?php echo $explode[1]; ?> m2 de contrucción<i class="fa fa-times"></i></a>
							<?php
							}
						}
						// esta parte es para generar la logitud y lactitud para el mapa.
						// en caso de que el get zona venga solo se toma la lat y long de México pais.
						$padreID = "";
						$tems_zona = get_term_by( 'id', $zonag, 'zona' );
						if($tems_zona->parent==0){
							$padreID = $zonag;
						}else{
							$padreID = $tems_zona->parent;
						}
						$imagen = get_field('imagen', $cat->taxonomy."_".$cat->term_id);
						$cooderPadre = get_field('mapa_categorias',"zona_".$padreID);
						if($cooderPadre!=""){
							$latP = $cooderPadre['lat'];
							$lngP = $cooderPadre['lng'];
							$zoom = 8;
						}else{
							$latP = 23.634501;
							$lngP = -102.55278399999997;
							$zoom = 5;
						}
						?>
			</div>
		</div>
		<div class="large-12 medium-12 small-12 columns box-filtrado">
			<div>
					<!-- cantidad de elementos en la consulta-->
					<?php
					$ba1_2 = 0;
					$ba3_4 = 0;
					$ba5_6 = 0;
					$bamore6 = 0;
					$zonacant = array();
					$contItem = 0;
					if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
						$IDP=$post->ID;
						$banos=get_field('ba',$IDP); 
						if($banos>=1 && $banos <=2){
							$ba1_2++;
						}
						if($banos>=3 && $banos <=4){
							$ba3_4++;
						}
						if($banos>=5 && $banos <=6){
							$ba5_6++;
						}
						if($banos>6){
							$bamore6++;
						}
						//
						$zona = get_the_terms( $post->ID, 'zona' );
						array_push($zonacant, $zona[$contItem]->term_id);
						//$getidpadre = $zona[1]->term_id;
						//$nombre_ubicacion_padre = get_field('abreviatura',"zona_".$getidpadre);
						$contItem++;
					endwhile; endif;
					?>
					<!-- filtrado de categora Tipo -->
					<!-- Si el get de categoria Tipo esta vacio muestra todos los filtros de Tipo -->
					<!-- De lo contrario si no se encuentra vacio comprueba que el get de Tipo sea padre si es padre muestra sus subcategorias, si no es padre ya no muestra nada del filtro  -->
					<!-- Para seleccinar un filtro se realiza mediante la clase tipo-change y se toman sus valores en JS -->
					<?php if($tipog==""){  ?>
						<div class="panel-acc">
							<div class="sub-panel-acc">
								<a class="light gray clav-search words-filtrado words-padre" id="ejem1">Tipo de operación <i class="fa fa-chevron-down graybajo"></i></a>
								<div id="pejem1" class="acc1">
									<?php 	
											$all_tipo = get_categories( array('orderby' => 'name', 'parent' => 0, 'taxonomy' => 'Tipo'));
											foreach ($all_tipo as $tipr) { ?>
												<a class="light clav-search words-filtrado select-filtro-s cselect tipo-change" id="Tipo_<?php echo $tipr->term_id; ?>"><?php echo $tipr->name; ?></a>
												<?php $term_brothers = get_term_children( $tipr->term_id, "Tipo"); 
													foreach ( $term_brothers as $child ) { 
														$term = get_term_by( 'id', $child, "Tipo" ); ?>
														<!--<a class="light clav-search words-filtrado select-filtro-s cselect tipo-change" id="Tipo_<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>-->
											 		<?php 
											 		}
											} ?>
								</div>
							</div>
						</div>
					<?php }else{ 
							$term_tipog = get_term_by( 'id', $tipog, 'Tipo' );
							if($term_tipog->parent==0){ ?>
								<div class="panel-acc">
									<div class="sub-panel-acc">
										<a class="light gray clav-search words-filtrado words-padre" id="ejem1">Tipo de propiedad <i class="fa fa-chevron-down graybajo"></i></a>
										<div id="pejem1" class="acc1">
											<?php
												$term_childrens = get_term_children( $term_tipog->term_id, "Tipo");
												foreach ( $term_childrens as $child ) {
													$term = get_term_by( 'id', $child, "Tipo" ); ?>
													<a class="light clav-search words-filtrado select-filtro-s cselect tipo-change" id="Tipo_<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>
												<?php
												}
												?>
										</div>
									</div>
								</div>
					<?php   } 
						  }
					?>
					<?php 
					// Muestra de filtros de la categoria zona
					// Si el get zona esta vacio muestra todas las zonas padre.
					// Si no esta vacio conprueba si el get zona tiene hijos si es asi los muestra, si no los tiene no muestra nada.
					// Para seleccinar un filtro se realiza mediante la clase tipo-change y se toman sus valores en JS 
					if($zonag==""){ ?>
						<div class="panel-acc">
							<div class="sub-panel-acc">
								<a class="light gray clav-search words-filtrado words-padre" id="ejem2">Estado <i class="fa fa-chevron-down graybajo"></i></a>
								<div id="pejem2" class="acc1">
									 	<?php
										$all_tipo = get_categories( array('orderby' => 'name', 'parent' => 0, 'taxonomy' => 'zona'));
										foreach ($all_tipo as $tipr) { ?>
											<a class="light clav-search words-filtrado select-filtro-s cselect tipo-change" id="zona_<?php echo $tipr->term_id; ?>"><?php echo $tipr->name; ?></a>
											<?php $term_brothers = get_term_children( $tipr->term_id, "zona"); foreach ( $term_brothers as $child ) { 
													$term = get_term_by( 'id', $child, "zona" ); ?>
										 		<?php 
										 		}
										}
									?>
								</div>
							</div>
						</div>
					<?php }else{
							if(is_numeric($zonag)){
						 		$term_childrens = get_term_children( $zonag, "zona");
						 	}else{
						 		$zona1 = get_term_by( 'name', $zonag, 'zona' );
								$id_term = $zona1->term_id;
								$term_childrens = get_term_children( $id_term, "zona");
							}
							if(count($term_childrens)>0){ ?>
								<div class="panel-acc">
									<div class="sub-panel-acc">
										<a class="light gray clav-search words-filtrado words-padre" id="ejem1">Zona <i class="fa fa-chevron-down graybajo"></i></a>
										<div id="pejem1" class="acc1">
											<?php
												foreach ( $term_childrens as $child ) { 
													$term = get_term_by( 'id', $child, "zona" );
													$term_padre = get_term_by( 'id', $term->parent, "zona" );
													$nombre_padre = $term_padre->name;
													if($term->parent==$zonag  || $nombre_padre==$zonag){  ?>
														<a class="light clav-search words-filtrado select-filtro-s cselect tipo-change" id="zona_<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>
												<?php
													}
												}
												?>
										</div>
									</div>
								</div>
							<?php
							}
						} ?>
					<div class="panel-acc price-div">
						<div class="sub-panel-acc" style="padding-bottom: 10px;">
							<a class="light gray clav-search words-filtrado words-padre">Precio <i class="fa fa-chevron-down graybajo"></i></a>
							<div id="pejem12" class="acc1">
								<!--<div id="slider"></div>-->
								<div class="large-4 medium-12 small-12 columns padding0">
									<label class="tipografia cselect rangelett">desde <strong>MXN</strong></label>
								</div>
								<div class="large-8 medium-12 small-12 columns padding0">
									<input type="number" name="minprice" class="inpurange" id="minprice" placeholder="Min" >
								</div>
								<div class="large-4 medium-12 small-12 columns padding0">
									<label class="tipografia cselect rangelett">hasta <strong>MXN</strong></label>
								</div>
								<div class="large-8 medium-12 small-12 columns padding0">
									<input type="number" name="maxprice" class="inpurange" id="maxprice" placeholder="Max" >
								</div>
								
								<div class="div-range1 text-center">
									<a><img src="<?php bloginfo('template_url'); ?>/img/price.png"></a><a class="light gray" id="menorp"></a><a>-</a><a class="light gray" id="mayorp"></a>
								</div>
								<div class="msj-vacio">
									<div class="pico"></div>
									<label class="tipografia msj-error">Selecciona un rango</label>
								</div>
								<div class="large-12 medium-12 small-12 columns div-fil-price">
									<a class="tipografia filt-price click_price">Filtrar</a>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="light gray clav-search words-filtrado words-padre" id="ejem3">Recámaras <i class="fa fa-chevron-down graybajo"></i></a>
							<div id="pejem2" class="acc1">
								<div class="large-4 medium-12 small-12 columns padding0">
									<label class="tipografia cselect rangelett">desde <strong></strong></label>
								</div>
								<div class="large-8 medium-12 small-12 columns padding0">
									<input type="number" name="minrecamaras" class="inpurange" id="minrecamaras" placeholder="Min" >
								</div>
								<div class="large-4 medium-12 small-12 columns padding0">
									<label class="tipografia cselect rangelett">hasta <strong></strong></label>
								</div>
								<div class="large-8 medium-12 small-12 columns padding0">
									<input type="number" name="maxrecamaras" class="inpurange" id="maxrecamaras" placeholder="Max" >
								</div>
								<div class="msj-vacior">
									<div class="pico"></div>
									<label class="tipografia msj-error">Selecciona un rango</label>
								</div>
								<div class="large-12 medium-12 small-12 columns div-fil-price">
									<a class="tipografia filt-price click_recamaras">Filtrar</a>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="light gray clav-search words-filtrado words-padre" id="ejem4">Baños <i class="fa fa-chevron-down graybajo"></i></a>
							<div id="pejem4" class="acc1">
								<a class="light cselect select-filtro-s clav-search words-filtrado op-ba" id="1_2">1-2 <!--(<?php echo $ba1_2; ?>)--></a>
								<a class="light cselect select-filtro-s clav-search words-filtrado op-ba" id="3_4">3-4 <!--(<?php echo $ba3_4; ?>)--></a>
								<a class="light cselect select-filtro-s clav-search words-filtrado op-ba" id="5_6">5-6 <!--(<?php echo $ba5_6; ?>)--></a>
								<a class="light cselect select-filtro-s clav-search words-filtrado op-ba" id="opc5">Más de 6 <!--(<?php echo $bamore6; ?>)--></a>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="light gray clav-search words-filtrado words-padre" id="ejem5">Superficie del terreno <i class="fa fa-chevron-down graybajo"></i></a>
							<div id="pejem5" class="acc1">
								<!--<div id="slider2"></div>-->
								<div class="large-12 medium-12 small-12 columns padding0">
									<div class="large-3 medium-12 small-12 columns padding0">
										<label class="tipografia cselect rangelett">desde</label>
									</div>
									<div class="large-9 medium-12 small-12 columns padding0">
										<input type="number" name="minterreno" class="inpurange" id="minterreno" placeholder="Min">
									</div>
									<div class="large-3 medium-12 small-12 columns padding0">
										<label class="tipografia cselect rangelett">hasta</label>
									</div>
									<div class="large-9 medium-12 small-12 columns padding0">
										<input type="number" name="maxterreno" class="inpurange" id="maxterreno" placeholder="Max">
									</div>
									<div class="div-range2 text-center">
										<a><img src="<?php bloginfo('template_url'); ?>/img/rule.png"></a><a class="light gray" id="menor"></a><a>-</a><a class="light gray" id="mayor"></a>
									</div>
									<div class="msj-vacio2">
										<div class="pico"></div>
										<label class="tipografia msj-error">Selecciona un rango</label>
									</div>
									<div class="large-12 medium-12 small-12 columns div-fil-price1">
										<a class="tipografia filt-price click_terreno">Filtrar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="light gray clav-search words-filtrado words-padre" id="ejem5">Superficie del construcción <i class="fa fa-chevron-down graybajo"></i></a>
							<div id="pejem5" class="acc1">
								<!--<div id="slider3"></div>-->
								<div class="large-3 medium-12 small-12 columns padding0">
									<label class="tipografia cselect rangelett">desde</label>
								</div>
								<div class="large-9 medium-12 small-12 columns padding0">
									<input type="number" name="minsuper" class="inpurange" id="minsuper" placeholder="Min">
								</div>
								<div class="large-3 medium-12 small-12 columns padding0">
									<label class="tipografia cselect rangelett">hasta</label>
								</div>
								<div class="large-9 medium-12 small-12 columns padding0">
									<input type="number" name="maxsuper" class="inpurange" id="maxsuper" placeholder="Max">
								</div>
								<div class="div-range3 text-center">
									<a><img src="<?php bloginfo('template_url'); ?>/img/rule.png"></a><a class="light gray" id="menorc"></a><a>-</a><a class="light gray" id="mayorc"></a>
								</div>
								<div class="msj-vacio3">
									<div class="pico"></div>
									<label class="tipografia msj-error">Selecciona un rango</label>
								</div>
								<div class="large-12 medium-12 small-12 columns div-fil-price1">
									<a class="tipografia filt-price click_superficie">Filtrar</a>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<div class="large-9 medium-9 small-12 columns">
		<div class="large-3 medium-12 small-12 columns padding0 hide-for-small-only">
			<h3 class="tipografia gray resultados light"><?php echo $contelementos; ?> Resultados</h3>
		</div>
		<div class="large-9 medium-12 small-12 columns search-h-right">
			<div class="large-6 medium-6 small-12 columns padding0">
				<div class="div-s">
					<h5 class="tipografia gray light ordenarpor">ordenar por:</h5>
				</div>
				<div class="div-s">
					<select id="precio-orden">
						<?php if($tipo_or=="price"){ ?>
							<option selected class="light" value="price">Precio</option>
						<?php }else{ ?>
							<option class="light" value="price">Precio</option>
						<?php } ?>
						<?php if($tipo_or=="terreno"){ ?>
							<option selected class="light" value="terreno">M2 Terreno</option>
						<?php }else{ ?>
							<option class="light" value="terreno">M2 Terreno</option>
						<?php } ?>
						<?php if($tipo_or=="construnccion"){ ?>
							<option selected class="light" value="construnccion">M2 Construcción</option>
						<?php }else{ ?>
							<option class="light" value="construnccion">M2 Construcción</option>
						<?php } ?>
						<?php if($tipo_or=="recamaras"){ ?>
							<option selected class="light" value="recamaras">Recamaras</option>
						<?php }else{ ?>
							<option class="light" value="recamaras">Recamaras</option>
						<?php } ?>
						<?php if($tipo_or=="fecha"){ ?>
							<option selected class="light" value="fecha">Fecha de publicación</option>
						<?php }else{ ?>
							<option class="light" value="fecha">Fecha de publicación</option>
						<?php } ?>
						<?php if($tipo_or=="lugar"){ ?>
							<option selected class="light" value="lugar">Colonia</option>
						<?php }else{ ?>
							<option class="light" value="lugar">Colonia</option>
						<?php } ?>
					</select>
					<div class="up_down text-center">
						<label class="gray flechas_ord" id="ASC"><i class="fa fa-chevron-up" aria-hidden="true"></i></label>
						<label class="gray flechas_ord" id="DESC"><i class="fa fa-chevron-down" aria-hidden="true" style="    padding-left: 0px;"></i></label>
					</div>
				</div>
			</div>
			<div class="large-6 medium-6 small-12 columns padding0">
				<div class="div-s">
					<h5 class="tipografia light gray">ver:</h5>
				</div>
				<div class="div-s divs-rigth">
					<div class="div-sub-s">
						<a id="showlistaIcono"><img src="<?php bloginfo('template_url'); ?>/img/listaIcono.png"></a>
					</div>
					<div class="div-sub-s">
						<a id="showmapaIcono"><img src="<?php bloginfo('template_url'); ?>/img/mapaIcono.png"></a>
					</div>
					<div class="div-sub-s">
						<a id="showcuadrosIconos"><img src="<?php bloginfo('template_url'); ?>/img/cuadrosIconos.png"></a>
					</div>
					<div class="div-sub-s">
						<a id="showmasCuadrosIcono"><img src="<?php bloginfo('template_url'); ?>/img/masCuadrosIcono.png"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos padding0" id="listaIcono">
			<?php 
					
					if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
						$IDP=$post->ID;
						$id_p=get_field('id',$IDP);
						$precio=get_field('precio',$IDP);
						if($precio!=""){
							$precio = number_format($precio, 2);
						} 
						$precio_renta=get_field('precio_renta',$IDP);
						if($precio_renta!=""){ 
							$precio_renta = number_format($precio_renta, 2);
						}
						$recamaras=get_field('recamaras',$IDP); 
						$banos=get_field('ba',$IDP); 

						$temperatura=get_field('temperatura',$IDP); 
						$superficie_construccion=get_field('superficie_construccion',$IDP);
						$galeria=get_field('galeria',$IDP);
						$elementos = get_field('elementos');
						$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						<div class="large-12 medium-12 small-12 columns padding0 marg-elem padding0 elmh">
							<div class="large-4 medium-4 small-12 columns padding0L divimgop1">
								<a href="<?php echo the_permalink(); ?>">
									<div class="bgop1 this_<?php echo $IDP; ?>" style="background: url('<?php echo $imagen[0]; ?>')no-repeat;">
										<?php if ( is_user_logged_in() ) { 
												if($role_user=="agentes_inmobiliarios"){
													$agregar_msg = "Agregar a PDF";
													$icon_msg = '<i class="fa fa-file-pdf-o spac-icon" aria-hidden="true"></i>';
												}else{
													$agregar_msg = "Agregar a favoritos";
													$icon_msg = '<i class="fa fa-star-o spac-icon" aria-hidden="true"></i>';
												}
												?>
												<div class="add-milist">
													<a class="tipografia blanco add-mylist" user="<?php echo $current_user->ID; ?>" inmueble="<?php echo $IDP; ?>" numadd="1"><?php echo $agregar_msg; ?> <?php echo $icon_msg; ?></a>
												</div>
										<?php } ?>
									</div>
									<!--<img src="<?php echo $imagen[0]; ?>">-->
								</a>
							</div>
							<div class="large-6 medium-6 small-12 columns margi-lft-elmnt">
								<div class="large-12 medium-12 small-12 columns padding0">
									<a href="<?php echo the_permalink(); ?>"><h3 class="tipografia black title-1"><i class="fa fa-angle-double-right flech-lft"></i> <?php echo get_the_title(); ?></h3></a>
									<h5 class="tipografia gray-light id1">id# <?php echo $id_p; ?></h5>
								</div>
								<div class="large-12 medium-12 small-12 columns padding0 cont-search-price">
									<?php if($precio!=""){ ?>
										<label class="tipografia gray-light search-price"><span class="beige">Precio Venta:</span> $<?php echo $precio; ?> MXN</label>
									<?php } ?>
									<?php if($precio_renta!=""){ ?>
										<label class="tipografia gray-light search-price"><span class="beige">Precio Renta:</span> $<?php echo $precio_renta; ?> MXN</label>
									<?php } ?>
								</div>
								<div class="large-12 medium-12 small-12 columns padding0">
								</div>
								<?php if($recamaras!="" && $banos!="" && $temperatura!="" &&  $superficie_construccion!=""){ ?>
									<div class="large-12 medium-12 small-12 columns padding0">
										<div class="large-3 medium-4 small-12 columns padding0">
											<label class="gray"><i class="fa fa-bed pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $recamaras; ?> Recamaras</span></label>
										</div>
										<div class="large-3 medium-4 small-12 columns padding0">
											<label class="gray"><i class="fa fa-shopping-basket pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $banos; ?> Baños</span></label>
										</div>
										<div class="large-3 medium-4 small-12 columns padding0">
											<label class="gray"><i class="fa fa-sun-o pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $temperatura; ?> ft2</span></label>
										</div>
										<div class="large-3 medium-4 small-12 columns padding0">
											<label class="gray"><i class="fa fa-sun-o pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $superficie_construccion; ?> ft2</span></label>
										</div>
									</div>
								<?php } ?>
								<div class="large-12 medium-12 small-12 columns padding0 div-descrips-search">
									<label class="tipografia gray-light info-prop2"><?php the_excerpt(); ?></label>
								</div>
								<div class="large-12 medium-12 small-12 columns padding0 div-prog-search">
									<a href="<?php echo the_permalink(); ?>" class="tipografia gray progm-visita">Ver inmueble</a>
								</div>
							</div>
							<div class="large-2 medium-2 small-12 columns"></div>
						</div>
					<?php
					endwhile; endif;
			?>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos marg-elem padding0" id="cuadrosIconos">
			<div class="large-12 medium-12 small-12 columns padding0">
				<?php 
					
						if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
							$IDP = $post->ID;
							$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
							$zona = get_the_terms( $IDP, 'zona' );
							$nombre_ubicacion=$zona[0]->name;
							//$nombre_ubicacion_padre=$zona[1]->name;
							$getidpadre = $zona[1]->term_id;
							$nombre_ubicacion_padre = get_field('abreviatura',"zona_".$getidpadre);
							$precio=get_field('precio',$IDP);
							if($precio!=""){
								$precio = number_format($precio, 2);
							} 
							$precio_renta=get_field('precio_renta',$IDP);
							if($precio_renta!=""){ 
								$precio_renta = number_format($precio_renta, 2);
							}

						?>
							<div class="cuadros-prop1">
								<div class="bg-destacados2 this2_<?php echo $IDP; ?>" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
									<div class="capa-filter2">
										<div class="info-destacada2">
											<a href="<?php echo the_permalink(); ?>" class="tipografia blanco title-cuadros"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?> <span class="preci-cadros">$<?php echo $precio; ?> USD</span></a>
										</div>
									</div>
								</div>
								<?php if ( is_user_logged_in() ) { 
										if($role_user=="agentes_inmobiliarios"){
											$agregar_msg = "Agregar a PDF";
											$icon_msg = '<i class="fa fa-file-pdf-o spac-icon" aria-hidden="true"></i>';
										}else{
											$agregar_msg = "Agregar a favoritos";
											$icon_msg = '<i class="fa fa-star-o spac-icon" aria-hidden="true"></i>';
										}
										?>
										<div class="add-milist">
											<a class="tipografia blanco add-mylist" numadd="2" user="<?php echo $current_user->ID; ?>" inmueble="<?php echo $IDP; ?>"><?php echo $agregar_msg; ?> <?php echo $icon_msg; ?></a>
										</div>
								<?php } ?>
							</div>
					<?php endwhile; endif; ?>
				<?php  ?>
			</div>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos marg-elem padding0" id="mapaIcono">
			<div id="map"></div>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos marg-elem padding0" id="masCuadrosIcono">
			<div class="large-12 medium-12 small-12 columns padding0">
				<?php 
					
						if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
							$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
							$IDP=$post->ID;
							$id_p=get_field('id',$IDP);
							$precio=get_field('precio',$IDP);
							$zona = get_the_terms( $post->ID, 'zona' );
							$getidpadre = $zona[1]->term_id;
							$nombre_ubicacion_padre = get_field('abreviatura',"zona_".$getidpadre);
							if($precio!=""){
								$precio = number_format($precio, 2);
							} 
							$precio_renta=get_field('precio_renta',$IDP);
							if($precio_renta!=""){ 
								$precio_renta = number_format($precio_renta, 2);
							}
							$recamaras=get_field('recamaras',$IDP); 
							$banos=get_field('ba',$IDP); 
							$temperatura=get_field('temperatura',$IDP); 
							$galeria=get_field('galeria',$IDP); 
							if($precio!=""){
								$precio = number_format($precio, 2);
							} 
							$precio_renta=get_field('precio_renta',$IDP);
							if($precio_renta!=""){ 
								$precio_renta = number_format($precio_renta, 2);
							}
						?>
							<a href="<?php echo the_permalink(); ?>">
								<div class="cuadros-prop">
									<div class="bg-destacados3 this3_<?php echo $IDP; ?>" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
										<?php if ( is_user_logged_in() ) { 
											if($role_user=="agentes_inmobiliarios"){
												$agregar_msg = "Agregar a PDF";
												$icon_msg = '<i class="fa fa-file-pdf-o spac-icon" aria-hidden="true"></i>';
											}else{
												$agregar_msg = "Agregar a favoritos";
												$icon_msg = '<i class="fa fa-star-o spac-icon" aria-hidden="true"></i>';
											}
											?>
											<div class="add-milist">
												<a class="tipografia blanco add-mylist" numadd="3" user="<?php echo $current_user->ID; ?>" inmueble="<?php echo $IDP; ?>"><?php echo $agregar_msg; ?> <?php echo $icon_msg; ?></a>
											</div>
										<?php } ?>
									</div>
									<div class="large-12 medium-12 small-12 columns padding0 bg-cafe">
										<div class="large-7 medium-7 small-12 columns padding0">
											<h5 class="tipografia blanco"><?php the_title(); ?></h5>
										</div>
										<div class="large-5 medium-5 small-12 columns text-right padding0">
											<label class="light blanco">id#<?php echo $id_p; ?></label>
										</div>
										<div class="large-6 medium-6 small-12 columns padding0">
											<label class="light blanco precio-morecuadros">Renta: <?php echo $precio_renta; ?></label>
										</div>
										<div class="large-6 medium-6 small-12 columns padding0">
											<label class="light blanco precio-morecuadros">Venta: <?php echo $precio; ?></label>
										</div>
										<div class="large-12 medium-12 small-12 columns padding0">
											<label class="light blanco"><?php the_excerpt(); ?></label>
										</div>
										<div class="large-12 medium-12 small-12 columns padding0 text-right">
											<a href="<?php echo the_permalink(); ?>" class="light blanco"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?></a>
										</div>
									</div>
								</div>
							</a>
					<?php endwhile; endif; ?>
			
			</div>
		</div>
	</div>
</div>
<!-- array para mapa interactivo -->
<?php
	$elementos = array();
	$precio_arr = array();
	$sterreno_arr = array();
	$scontrunccion_arr = array();
	$slug = "";
	if($tipog!=""){
		$term_tipo = get_term_by( 'id', $tipog, "Tipo" );
		if($term_tipo->parent!=0){
			$term_tipo_parent = get_term_by( 'id', $term_tipo->parent, "Tipo" );
			$slug = $term_tipo_parent->slug;
		}else{
			$slug = $term_tipo->slug;
		}
	}
	if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post(); 
		$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$title = $post->post_title;
		$IDP=$post->ID;
		$id_p=get_field('id',$IDP);
		$precio=get_field('precio',$IDP);
		$precio_venta = get_field('precio',$IDP);
		if($precio!=""){
			$precio = number_format($precio, 2);
		} 
		$precio_renta=get_field('precio_renta',$IDP);
		if($precio_renta!=""){ 
			$precio_renta = number_format($precio_renta, 2);
		}
		$precio_renta_f = get_field('precio_renta',$IDP);
		if($slug == "venta"){
			$price = $precio_venta;
		}
		if($slug == "renta"){
			$price = $precio_renta_f;
		}
		if($slug == ""){
			$price = $precio_venta;
		}
		$recamaras=get_field('recamaras',$IDP); 
		$banos=get_field('ba',$IDP); 
		$temperatura=get_field('temperatura',$IDP); 
		$superficie_construccion=get_field('superficie_construccion',$IDP);
		$descipcion_corta = get_the_excerpt($IDP);
		$permalink = $post->post_name;
		$location = get_field('mapa',$IDP);
		if($location!=""){
			$lat = $location['lat'];
			$lng = $location['lng'];
		}else{
			$lat = "";
			$lng = "";
		}
		$elemento = [
			'imagen' => $imagen[0],
            'title' => $title,
			'idp' => $id_p,
			'precio' => $precio,
			'precio_renta' => $precio_renta,
			'recamaras' => $recamaras,
			'banos' => $banos,
			'temperatura' => $temperatura,
			'descipcion_corta' => $descipcion_corta,
			'permalink' => $permalink,
			'lat' => $lat,
			'lng' => $lng,
        ];
        $elementos [] = $elemento;
        //guradar precio 
        array_push($precio_arr, $price);
        array_push($sterreno_arr, $temperatura);
        array_push($scontrunccion_arr, $superficie_construccion);
	endwhile; endif;
?>
<!-- fin array para mapa interactivo -->
<!-- para el filtrado de atributos-->
<?php
	$max_price = max($precio_arr);
	//mayor terreno
	$max_terreno = max($sterreno_arr);
	//mayor superficie
	$max_super = max($scontrunccion_arr);
?>
<input id="precio_max" value="<?php echo $max_price; ?>" style="display: none;"></input>
<!-- fin filtrado de atributos-->
<?php get_sidebar('destacados'); ?>
<?php get_footer(); ?>
<script>
	var addressValue;
	var gets = <?php echo json_encode($myloops); ?>;
	var cant = gets.length;
	var arr = [];
	var zona = "<?php echo $zonag ?>";
	var tipo = "<?php echo $tipog ?>";
	var url_now = "<?php echo $actual_link ?>";
	var sterreno_arr = "<?php echo $max_terreno ?>";
	var scontrunccion_arr = "<?php echo $max_super ?>";
	var slug = "<?php echo $slug ?>";
	for (var i=0; i<cant; i++) {
	    var obj = gets[i];
	    arr.push(obj.terms);
	}
	$('.words-padre').click(function(){
		$(this).next().slideToggle(300);
	});
	$('.tipo-change').click(function(){ 
		//obtiene el id de la clase
		var ID = $(this).attr("id");
		//separa el id por el elemento "_"
		var explo = ID.split("_");
		if(explo[0]=='Tipo'){ tipo="tipo"; }else{ tipo="zona";}
		//
		var exp_link = url_now.split("?");
		var gets = exp_link[1].split("&");
		var urlmodif;
		$.each( gets, function( key, value ) {
		  var exp_get = value.split("=");
		  
		  if(exp_get[0]==tipo){
		  	addressValue = url_now.replace(""+tipo+"="+exp_get[1]+"", ""+tipo+"="+explo[1]+"");
		  	document.location.href = addressValue;
		  }
		});
	});
	$('.delet').click(function(){
		var ID = $(this).attr("id");
		var explode = ID.split("_");
		var cat;
		var tipo;
		///
		if(explode[0]=='Tipo'){ tipo="tipo"; }else{ tipo="zona";}
		//
		var price_rang = valu1+"_"+valu2;
		var exp_link = url_now.split("?");
		var gets = exp_link[1].split("&");
		var urlmodif;
		$.each( gets, function( key, value ) {
		  var exp_get = value.split("=");
		  if(exp_get[0]==tipo){
		  	addressValue = url_now.replace(""+tipo+"="+exp_get[1]+"", ""+tipo+"="+explode[1]+"");
		  	document.location.href = addressValue;
		  }
		});
	});
	$('.delet-cf').click(function(){
		var ID = $(this).attr('id');
		var expl_id = ID.split("/");
		//
		var exp_link = url_now.split("?");
		var gets = exp_link[1].split("&");
		$.each( gets, function( key, value ) {
		  var exp_get = value.split("=");
		  if(exp_get[0]==expl_id[0]){
		  	addressValue = url_now.replace("&"+expl_id[0]+"="+expl_id[1]+"", "");
		  	document.location.href = addressValue;
		  }
		});

	});
	//recamaras
	$('.click_recamaras').click(function(){
		var valu1 = $('#minrecamaras').val();
		var valu2 = $('#maxrecamaras').val();
		if(valu1!="" && valu2!=""){
			var price_rang = valu1+"_"+valu2;
			var exp_link = url_now.split("?");
			var gets = exp_link[1].split("&");
			///
			var a = 0;
			var elm = [[]];
			$.each( gets, function( key, value ) {
			  var exp_get = value.split("=");
			  	elm[a]=exp_get[0];
			  a++;
			});
		    var op = elm.indexOf("recamaras");
		    if(op==-1){
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  addressValue=url_now+"&recamaras="+price_rang;
				  //addressValue = suburl+"&price_tipo="+slug;
				});
		    }else{
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  if(exp_get[0]=="recamaras"){
				  	addressValue = url_now.replace("recamaras="+exp_get[1]+"", "recamaras="+price_rang+"");
				  	//addressValue = url_now.replace("price="+exp_get[1]+"", "price="+price_rang+"");
				  }
				});
		    }
		    document.location.href = addressValue;
		}else{
			$('.msj-vacior').show();
			$('.msj-vacior').addClass('animated pulse');
		}
	});
	$('#minrecamaras').click(function(){
		$('.msj-vacior').hide();
		$('.msj-vacior').removeClass('animated pulse');
	});
	$('#maxrecamaras').click(function(){
		$('.msj-vacior').hide();
		$('.msj-vacior').removeClass('animated pulse');
	});
	//precio
	var precio_max = "<?php echo $max_price ?>";
	var media = precio_max/2;
	var valu1;
	var valu2;
	$('.click_price').click(function(){
		valu1 = $('#minprice').val();
		valu2 = $('#maxprice').val();
		if(valu1!="" || valu2!=""){
			var price_rang = valu1+"_"+valu2;
			var exp_link = url_now.split("?");
			var gets = exp_link[1].split("&");
			///
			var a = 0;
			var elm = [[]];
			$.each( gets, function( key, value ) {
			  var exp_get = value.split("=");
			  	elm[a]=exp_get[0];
			  a++;
			});
		    var op = elm.indexOf("price");
		    if(op==-1){
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  addressValue=url_now+"&price="+price_rang;
				  //addressValue = suburl+"&price_tipo="+slug;
				});
		    }else{
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  if(exp_get[0]=="price"){
				  	addressValue = url_now.replace("price="+exp_get[1]+"", "price="+price_rang+"");
				  	//addressValue = suburl+"&price_tipo="+slug;
				  }
				});
		    }
		    document.location.href = addressValue;
		}else{
			$('.msj-vacio').show();
			$('.msj-vacio').addClass('animated pulse');
		}
	});
	$('#minprice').click(function(){
		$('.msj-vacio').hide();
		$('.msj-vacio').removeClass('animated pulse');
	});
	$('#maxprice').click(function(){
		$('.msj-vacio').hide();
		$('.msj-vacio').removeClass('animated pulse');
	});
	//superficie terreno
	var valu1m;
	var valu2m;
	$('.click_terreno').click(function(){
		valu1m = $('#minterreno').val();
		valu2m = $('#maxterreno').val();
		if(valu1m!="" || valu2m!=""){	
			var price_rang = valu1m+"_"+valu2m;
			var exp_link = url_now.split("?");
			var gets = exp_link[1].split("&");
			///
			var a = 0;
			var elm = [[]];
			$.each( gets, function( key, value ) {
			  var exp_get = value.split("=");
			  	elm[a]=exp_get[0];
			  a++;
			});
		    var op = elm.indexOf("terreno");
		    if(op==-1){
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  addressValue=url_now+"&terreno="+price_rang;
				});
		    }else{
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  if(exp_get[0]=="terreno"){
				  	addressValue = url_now.replace("terreno="+exp_get[1]+"", "terreno="+price_rang+"");
				  }
				});
		    }
		    document.location.href = addressValue;
		}else{
			$('.msj-vacio2').show();
			$('.msj-vacio2').addClass('animated pulse');
		}
	});
	$('#minterreno').click(function(){
		$('.msj-vacio2').hide();
		$('.msj-vacio2').removeClass('animated pulse');
	});
	$('#maxterreno').click(function(){
		$('.msj-vacio2').hide();
		$('.msj-vacio2').removeClass('animated pulse');
	});
	
	//superficie terreno
	var valu1s;
	var valu2s;
	$('.click_superficie').click(function(){
		valu1s = $('#minsuper').val();
		valu2s = $('#maxsuper').val();
		if(valu1s!="" || valu2s!=""){	
			var price_rang = valu1s+"_"+valu2s;
			var exp_link = url_now.split("?");
			var gets = exp_link[1].split("&");
			///
			var a = 0;
			var elm = [[]];
			$.each( gets, function( key, value ) {
			  var exp_get = value.split("=");
			  	elm[a]=exp_get[0];
			  a++;
			});
		    var op = elm.indexOf("construnccion");
		    if(op==-1){
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  addressValue=url_now+"&construnccion="+price_rang;
				});
		    }else{
		    	$.each( gets, function( key, value ) {
				  var exp_get = value.split("=");
				  if(exp_get[0]=="construnccion"){
				  	addressValue = url_now.replace("construnccion="+exp_get[1]+"", "construnccion="+price_rang+"");
				  }
				});
		    }
		    document.location.href = addressValue;
		}else{
			$('.msj-vacio3').show();
			$('.msj-vacio3').addClass('animated pulse');
		}
	});
	$('#minsuper').click(function(){
		$('.msj-vacio3').hide();
		$('.msj-vacio3').removeClass('animated pulse');
	});
	$('#maxsuper').click(function(){
		$('.msj-vacio3').hide();
		$('.msj-vacio3').removeClass('animated pulse');
	});
	
	$('.op-ba').click(function(){
		var ID = $(this).attr('id');
		var exp_link = url_now.split("?");
		var gets = exp_link[1].split("&");
		var a = 0;
		var elm = [[]];
		$.each( gets, function( key, value ) {
		  var exp_get = value.split("=");
		  	elm[a]=exp_get[0];
		  a++;
		});
	    var op = elm.indexOf("ba");
	    if(op==-1){
	    	$.each( gets, function( key, value ) {
		 		var exp_get = value.split("=");
		    	addressValue=url_now+"&ba="+ID;
			});
	    }else{
	    	$.each( gets, function( key, value ) {
		 		var exp_get = value.split("=");
		    	if(exp_get[0]=="ba"){
			  		addressValue = url_now.replace("ba="+exp_get[1]+"", "ba="+ID+"");
			  	}
		    	
			});
	    }
	    document.location.href = addressValue;
	});
	//ordenar
	$('.flechas_ord').click(function(){
		var ID = $(this).attr('id');
		var a_ordenar = $('select#precio-orden').val();
		var compuesta = ID+"_"+a_ordenar;
		///
		var exp_link = url_now.split("?");
		var gets = exp_link[1].split("&");
		var a = 0;
		var elm = [[]];
		$.each( gets, function( key, value ) {
		  var exp_get = value.split("=");
		  	elm[a]=exp_get[0];
		  a++;
		});
	    var op = elm.indexOf("ordenar");
	    if(op==-1){
	    	$.each( gets, function( key, value ) {
		 		var exp_get = value.split("=");
		    	addressValue=url_now+"&ordenar="+ID;
			});
	    }else{
	    	$.each( gets, function( key, value ) {
		 		var exp_get = value.split("=");
		    	if(exp_get[0]=="ordenar"){
			  		addressValue = url_now.replace("ordenar="+exp_get[1]+"", "ordenar="+compuesta+"");
			  	}
		    	
			});
	    }
	    document.location.href = addressValue;

	});
	//guardar url
    //sessionStorage["urls"]=url_now;
</script>
<script type="text/javascript">
	$('#showmapaIcono').click(function(){
		var elementos = <?php echo json_encode($elementos); ?>;
		var latP = <?php echo $latP; ?>;
		var lngP = <?php echo $lngP; ?>;
		var zoom = <?php echo $zoom; ?>;
		//var arrcontenido = [];
		var locations = [];
		$.each( elementos, function( key, value ) {
			var imagen = value.imagen;
			var title = value.title;
			var idp = value.idp;
			var precio = value.precio;
			var precio_renta = value.precio_renta;
			var recamaras = value.recamaras;
			var banos = value.banos;
			var temperatura = value.temperatura;
			var descipcion_corta = value.descipcion_corta;
			var permalink = value.permalink;
			var lat = value.lat;
			var lng = value.lng;
			lat = parseFloat(lat);
			lng = parseFloat(lng);
			var contenido = '<div class="large-12 medium-12 small-12 columns padding0 padding0" style="height: 310px;">'+
				'<div class="large-12 medium-12 small-12 columns">'+
					'<div class="large-12 medium-12 small-12 columns backg-modal" style="background: url('+imagen+')no-repeat;"></div>'+
					/*'<img class="img-modal" src="'+imagen+'">'+*/
				'</div>'+
				'<div class="large-12 medium-12 small-12 columns">'+
					'<div class="large-12 medium-12 small-12 columns padding0">'+
						'<h3 class="tipografia black title-1"><i class="fa fa-angle-double-right"></i>'+title+'</h3>'+
						'<h5 class="tipografia gray-light id1">id# '+idp+'</h5>'+
					'</div>'+
					'<div class="large-12 medium-12 small-12 columns padding0">'+
						'<label class="tipografia gray-light search-price"><span class="beige">Precio Venta:</span> $'+precio+' MXN</label>'+
						'<label class="tipografia gray-light search-price"><span class="beige">Precio Renta:</span> $'+precio_renta+' MXN</label>'+
					'</div>'+
					
					'<div class="large-12 medium-12 small-12 columns">'+
						'<div class="large-4 medium-4 small-12 columns padding0">'+
							'<label class="gray contien-modal">'+
								'<i class="fa fa-bed pad-rec gray-light"></i>'+ 
								'<span class="beige">'+recamaras+' Recamaras</span>'+
							'</label>'+
						'</div>'+
						'<div class="large-4 medium-4 small-12 columns padding0">'+
							'<label class="gray contien-modal">'+
								'<i class="fa fa-shopping-basket pad-rec gray-light"></i>'+ 
								'<span class="beige">'+banos+' Baños</span>'+
							'</label>'+
						'</div>'+
						'<div class="large-4 medium-4 small-12 columns padding0">'+
							'<label class="gray contien-modal">'+
								'<i class="fa fa-sun-o pad-rec gray-light"></i>'+
								'<span class="beige">'+temperatura+'</span>'+
							'</label>'+
						'</div>'+
					'</div>'+
					'<div class="large-12 medium-12 small-12 columns div-decrmodal">'+
						'<label class="light gray-light">'+descipcion_corta+'</label>'+
					'</div>'+
					'<div class="large-12 medium-12 small-12 columns div-prog-search">'+
						'<a href="<?php echo bloginfo('url') ?>/propiedades/'+permalink+'" class="tipografia gray progm-visita">Ver más detalles</a>'+
					'</div>'+
				'</div>'+
			'</div>';
			locations[key] = [contenido,lat,lng]; 
			//$("#aqui").html(contenido);
		});
		$('.vistas-modelos').fadeOut(300);
		$('#mapaIcono').fadeIn(300);
	
		var directorio = "<?php bloginfo('template_url'); ?>/img/";
		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: zoom,
		  center: new google.maps.LatLng(latP, lngP),
		  styles: [{"elementType":"geometry","stylers":[{"hue":"#ff4400"},{"saturation":-68},{"lightness":-4},{"gamma":0.72}]},{"featureType":"road","elementType":"labels.icon"},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"hue":"#0077ff"},{"gamma":3.1}]},{"featureType":"water","stylers":[{"hue":"#00ccff"},{"gamma":0.44},{"saturation":-33}]},{"featureType":"poi.park","stylers":[{"hue":"#44ff00"},{"saturation":-23}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"hue":"#007fff"},{"gamma":0.77},{"saturation":65},{"lightness":99}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"gamma":0.11},{"weight":5.6},{"saturation":99},{"hue":"#0091ff"},{"lightness":-86}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"lightness":-48},{"hue":"#ff5e00"},{"gamma":1.2},{"saturation":-23}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"saturation":-64},{"hue":"#ff9100"},{"lightness":16},{"gamma":0.47},{"weight":2.7}]}],
		  mapTypeId: google.maps.MapTypeId.ROADMAP,
		  scrollwheel: false,
		});
		var contenedor = '<p><img src="'+directorio+'/masCuadrosIcono.png"></img>IW antes</p>';
		var infowindow = new google.maps.InfoWindow();
		var image = { url: ''+directorio+'marker_blue.png' };
		for (i = 0; i < locations.length; i++) {  
		  marker = new google.maps.Marker({
		    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		    icon: image,
		    map: map
		  });
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
		    var popup = new google.maps.InfoWindow({
		        content:'<div id="hook">'+locations[i][0]+'</div>'
		    });
		    return function() {
		      infowindow.setContent(popup.content);
		      infowindow.open(map, marker);
		    }
		  })(marker, i));
		}
		/*
		setTimeout(function(){
		$("#hook").parent().addClass("averhide");
		$("#hook").parent().parent().addClass("averhide");
		},4000);*/
	});
</script>

