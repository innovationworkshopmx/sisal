<?php get_header(); ?>
<div class="wrapper large-12 medium-12 small-12 columns" style="margin-top:400px;">
	<form onsubmit="$('#hero-demo').blur();return false;" class="pure-form" >
        <input id="hero-demo" autofocus type="text" name="q" placeholder="Programming languages ...">
	</form>
</div>
<?php get_footer(); ?>
<script>
	$(function(){
            $('#hero-demo').autoComplete({
                minChars: 1,
                source: function(term, suggest){
                    term = term.toLowerCase();
                    var choices = ['Ade','Ade2','Jenn'];
                    var suggestions = [];
                    for (i=0;i<choices.length;i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);
                }
            });
        });
</script>