<?php
	get_header(); 
	wp_reset_query();
?>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin padd-bott">
	<div class="row">
		<div class="large-6 medium-6 small-12 columns">
			<div class="large-12 medium-12 small-12 columns">
				<h3 class="tipografia gray info-prop light"><i class="fa fa-angle-double-right right-filtros"></i> Registro</h3>
			</div>
			<div class="large-10 push-2 medium-8 small-12 columns div-userg">
				<div class="large-10 columns">
					<h4 class="bold black">Usuario General</h4>
					<label class="tipografia gray desc-usergen">Como usuario general podrás hacer uso de la mayor parte de las ventajas de Sisal BR como guardar y consultar tus propiedades favoritas.</label>
					<form id="form-userg">
						<label class="bold beige">Primer nombre</label>
						<input type="text" name="nombre" placeholder="Nombre" id="nombre">
						<label class="bold beige">Apellidos</label>
						<input type="text" name="apellidos" placeholder="Apellidos" id="apellidos">
						<label class="bold beige">Dirección de email</label>
						<input type="text" name="email" placeholder="Tu dirección de email" id="email">
						<label class="bold beige">Contraseña</label>
						<input type="password" name="passwordug" placeholder="********" id="passwordug">
					</form>
					<a class="tipografia gray registrarm1">Registrarme como usuario</a>
				</div>
			</div>
		</div>
		<div class="large-6 medium-6 small-12 columns divp-agen">
			<div class="large-10 medium-10 small-12 columns div-userinm">
				<h4 class="bold blanco">Agentes inmobiliarios</h4>
				<label class="tipografia blanco desc-usergen">Regístrate como Agente Inmobiliario para sacar el máximo provecho de nuestra plataforma web. Guarda propiedades en tu perfil y crea PDF´s automatizados de las propiedades.</label>
				<form id="form-userim">
						<label class="tipografia blanco">Primer nombre</label>
						<input type="text" name="nombreg" placeholder="Nombre" id="nombreg">
						<label class="tipografia blanco">Apellidos</label>
						<input type="text" name="apellidosg" placeholder="Apellidos" id="apellidosg">
						<label class="tipografia blanco">Dirección de email</label>
						<input type="text" name="emailg" placeholder="Tu dirección de email" id="emailg">
						<label class="tipografia blanco">Contraseña</label>
						<input type="password" name="passwordg" placeholder="********" id="passwordg">
						<label class="tipografia blanco">Agencia</label>
						<input type="text" name="agencia" id="agencia">
				</form>
				<a class="tipografia gray registrarm2">Registrarme</a>
			</div>
		</div>
	</div>
</div>
<div class="modal-black close-modal"></div>
<div class="send-info">
	<div class="sending">
		<div class="spinner">
		  <div class="cube1"></div>
		  <div class="cube2"></div>
		</div>
		<h5 class="tipografia blanco">Creando usuario...</h5>
	</div>
	<div class="mensaje text-center">
		<h4 class="bold" id="titulo"></h4>
		<label class="tipografia" id="explicacion"></label>
		<a class="button secondary close-modal text-right cerrar-b">Cerrar</a>
	</div>
</div>
<?php
	get_footer();
?>
<script>
	//var url_add = "../modelos/add-usuario.php";
	var url_add = "<?php echo content_url('themes/sisal.git/modelos/add-usuario.php'); ?>";
	//usuario general
	var nombreV = new LiveValidation('nombre');
	nombreV.add( Validate.Presence );
	var apellidoV = new LiveValidation('apellidos');
	apellidoV.add( Validate.Presence );
	var emailV = new LiveValidation('email');
	emailV.add( Validate.Presence );
	emailV.add( Validate.Email );
	var passwordV = new LiveValidation('passwordug');
	passwordV.add( Validate.Presence );
	var m = new mandrill.Mandrill('lFwDXkU9JM-pHtBjKKUV9g');
	$('.registrarm1').click(function(){
		var areAllValid = LiveValidation.massValidate( [nombreV,emailV,apellidoV,passwordV] );
		if(areAllValid!=false){ 
			$('.modal-black').fadeIn(400);
			$('.send-info').fadeIn(400);
			var datos = $('#form-userg').serialize();
			var email = $('#email').val();
			$('#titulo').text('');
			$('#explicacion').html('');
			$.ajax({
				type:'POST',
				url:''+url_add+'',
				data: datos,
				beforeSend:function(){
				},
				success:function(resp){
					$('.sending').hide();
					$('.mensaje').fadeIn(400);
					if(resp=="ya existe"){
						$('#titulo').text('Error al crear el usuario');
						$('#explicacion').html('Los datos que has ingresado para tu usuario ya existen, intenta con otros datos o dirígete al apartado <strong>Ingresar</strong> para recuperar tu contraseña.');
					}else{
						var idRes=parseInt(resp);
						var url_cont = "<?php bloginfo('url') ?>/activar-cuenta/?idconf="+idRes+"&type=gen";
						$('#titulo').text('Usuario creado satisfactoriamente');
						$('#explicacion').html('Te hemos enviado un correo a <strong>'+email+'</strong> para activar tu cuenta y así finalizar el proceso de registro.');
						var hmtlCod='<html class="no-js" lang="en">'+
								  '<head>'+
								    '<meta charset="utf-8" />'+
								    '<link href="https://fonts.googleapis.com/css?family=Quicksand:400,300,700" rel="stylesheet" type="text/css">'+
								  '</head>'+
								  '<body style="font-family: "Open Sans", sans-serif;">'+
								      '<div id="info" style="width: 800px;color: #707070;font-size: 14px;    position: relative;    margin: 0 auto;top: 42px;">'+
									      '<a href="'+url_cont+'" style="width: 100%;padding:20px;text-decoration: none;font-size: 33px;border: 1px solid #DE285C;">Activar cuenta</a><br><br>'+
								      '</div>'+
								    '</div>'+
								  '</body>'+
								'</html>'						 
								;
						var params = {
						  "message": {
						      "from_email":'bienesraíces@sisal.com.mx',
						      "to":[{"email":""+email+""}],
						      "subject": "Registro sisal: ",
						       "html": hmtlCod
						  }
						};
						m.messages.send(params, function(res) {
						  console.log(res);
						  $("#nombre").val("");
						  $("#email").val("");
						  $("#password").val("");
						  $("#apellidos").val("");
						}, function(err) {
						  console.log(err);
						});
					}
				}
			});
		}
	});
	//usuario agente inmobiliario
	var nombreV2 = new LiveValidation('nombreg');
	nombreV2.add( Validate.Presence );
	var apellidoV2 = new LiveValidation('apellidosg');
	apellidoV2.add( Validate.Presence );
	var emailV2 = new LiveValidation('emailg');
	emailV2.add( Validate.Presence );
	emailV2.add( Validate.Email );
	var passwordV2 = new LiveValidation('passwordg');
	passwordV2.add( Validate.Presence );
	var agenciaV = new LiveValidation('agencia');
	agenciaV.add( Validate.Presence );
	$('.registrarm2').click(function(){
		var areAllValid = LiveValidation.massValidate( [nombreV2,emailV2,apellidoV2,passwordV2,agenciaV] );
		if(areAllValid!=false){ 
			$('.modal-black').fadeIn(400);
			$('.send-info').fadeIn(400);
			var datos = $('#form-userim').serialize();
			var email = $('#emailg').val();
			$('#titulo').text('');
			$('#explicacion').html('');
			$.ajax({
				type:'POST',
				url:''+url_add+'',
				data: datos,
				beforeSend:function(){
				},
				success:function(resp){
					$('.sending').hide();
					$('.mensaje').fadeIn(400);
					if(resp=="ya existe"){
						$('#titulo').text('Error al crear el usuario');
						$('#explicacion').html('Los datos que has ingresado para tu usuario ya existen, intenta con otros datos o dirígete al apartado <strong>Ingresar</strong> para recuperar tu contraseña.');
					}else{
						var idRes=parseInt(resp);
						var url_cont = "<?php bloginfo('url') ?>/activar-cuenta/?idconf="+idRes+"&type=agen";
						$('#titulo').text('Usuario creado satisfactoriamente');
						$('#explicacion').html('Te hemos enviado un correo a <strong>'+email+'</strong> para activar tu cuenta y así finalizar el proceso de registro.');
						var hmtlCod='<html class="no-js" lang="en">'+
								  '<head>'+
								    '<meta charset="utf-8" />'+
								    '<link href="https://fonts.googleapis.com/css?family=Quicksand:400,300,700" rel="stylesheet" type="text/css">'+
								  '</head>'+
								  '<body style="font-family: "Open Sans", sans-serif;">'+
								      '<div id="info" style="width: 800px;color: #707070;font-size: 14px;    position: relative;    margin: 0 auto;top: 42px;">'+
									      '<a href="'+url_cont+'" style="width: 100%;padding:20px;text-decoration: none;font-size: 33px;border: 1px solid #DE285C;">Activar cuenta</a><br><br>'+
								      '</div>'+
								    '</div>'+
								  '</body>'+
								'</html>'						 
								;
						var params = {
						  "message": {
						      "from_email":'bienesraíces@sisal.com.mx',
						      "to":[{"email":""+email+""}],
						      "subject": "Registro sisal: ",
						       "html": hmtlCod
						  }
						};
						m.messages.send(params, function(res) {
						  console.log(res);
						  $("#nombreg").val("");
						  $("#emailg").val("");
						  $("#passwordg").val("");
						  $("#apellidosg").val("");
						}, function(err) {
						  console.log(err);
						});
					}
				}
			});
		}
	});
</script>