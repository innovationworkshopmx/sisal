<!DOCTYPE html>
<html>
<head>
	<title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
	<meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/foundation.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery.auto-complete.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/logo.ico" type="image/x-icon">
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/img/logo.ico" type="image/x-icon">
	<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/magnific-popup.css">
</head>
<body>
	<?php
		include 'funciones.php';
		$Header = new Header();
		if($post->post_type=="desarrollos"){
			$class = "single-header";
		}
	?>
	<div class="wrapper large-12 medium-12 small-12 columns header <?php echo $class; ?>" 
		<?php if (is_home() ) {  echo ' id="home-index"';}?> >
		<div class="large-3 medium-3 small-12 columns hd-left">
			<a href="<?php echo home_url(); ?>"><img src="<?php echo $Header->logo; ?>"></a>
		</div>
		<div class="large-9 medium-9 small-12 columns text-right header-right">
			<a href="javascript:void(0)" class="icon">
			  <div class="hamburger">
			    <div class="menui top-menu"></div>
			    <div class="menui mid-menu"></div>
			    <div class="menui bottom-menu"></div>
			  </div>
			</a>
			<?php
				$menu = $Header->menu;
				foreach ($menu as $valuem) {
					$nombre=$valuem['nombre'];
					$url=$valuem['url'];
					$id=$valuem['id'];
					if($id=="cel"){ ?>
						<a class="tipografia menu-ligas cel-header"><i class="fa fa-phone"></i> <?php echo $nombre; ?></a>
					<?php }
					if($id!="menu" && $id!="cel"){ ?>
						<a class="tipografia menu-ligas" href="<?php echo $url; ?>"><?php echo $nombre; ?></a>
					<?php }
					if($id=="menu"){ ?>
						<a class="tipografia menu-ligas"><i class="fa fa-bars"></i><?php echo $nombre; ?></a>
						<?php	
					}
				}
			?>
		</div>
	</div>