<div class="large-12 medium-12 small-12 columns div-destacados">
	<div class="large-12 medium-12 small-12 columns">
		<h3 class="light text-center gray title-destacados"><i class="fa fa-angle-double-right"></i> Desarrollos</h3>
		<?php $my_recoment = new WP_Query( 'post_type=desarrollos' ); 
			if (have_posts()) : while ( $my_recoment->have_posts() ) : $my_recoment->the_post();
				$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				//$zona = get_the_terms( $post->ID, 'zona' );
				//$nombre_ubicacion=$zona[0]->name;
				//$nombre_ubicacion_padre=$zona[1]->name;
			?>
				<div class="large-3 medium-4 small-12 columns div-destac-c" id="<?php echo the_permalink(); ?>">
					<div class="bg-destacados" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
						<div class="capa-filter">
							<div class="info-destacada">
								<a href="<?php echo the_permalink(); ?>" class="light blanco"><?php the_title(); ?></a>
							</div>
						</div>
					</div>
				</div>
		<?php endwhile; endif; ?>
	</div>
</div>