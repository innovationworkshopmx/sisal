<?php
	add_theme_support( 'post-thumbnails' ); 
	function sh_the_content_by_id( $post_id=0, $more_link_text = null, $stripteaser = false ){
	    global $post;
	    $post = &get_post($post_id);
	    setup_postdata( $post, $more_link_text, $stripteaser );
	    the_content();
	    wp_reset_postdata( $post );
	}
	add_filter( 'woocommerce_checkout_fields' , 'custom_wc_checkout_fields' );
	function custom_wc_checkout_fields( $fields ) {
		unset($fields['billing']['billing_company']);
		return $fields;
	}
	// acortar descripcion
	function custom_excerpt_length( $length ) {
	   return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
	//cf a header y footer
	if(function_exists('acf_add_options_page')){
		acf_add_options_page();
		acf_add_options_page(array(
			'page_title' 	=> 'Theme General Settings',
			'menu_title'	=> 'Theme Settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Theme Header Settings',
			'menu_title'	=> 'Header',
			'parent_slug'	=> 'theme-general-settings',
		));
		
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Theme Footer Settings',
			'menu_title'	=> 'Footer',
			'parent_slug'	=> 'theme-general-settings',
		));
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Theme Footer Settings',
			'menu_title'	=> 'Index',
			'parent_slug'	=> 'theme-general-settings',
		));
	}
	//crear cp propiedades
	function custom_propiedades()
	{
	    $labels = array(
	        'name' => _x('Propiedades', 'post type general name'),
	        'singular_name' => _x('Propiedades', 'post type singular name'),
	        'add_new' => _x('Agregar Propiedad', 'Propiedades'),
	        'add_new_item' => __('Agregar nueva propiedad'),
	        'edit_item' => __('Editar propiedad'),
	        'new_item' => __('Nueva propiedad'),
	        'all_items' => __('Todas las propiedades'),
	        'view_item' => __('Ver propiedades'),
	        'search_items' => __('Buscar propiedades'),
	        'not_found' =>  __('No se han encontrado propiedades'),
	        'not_found_in_trash' => __('No hay propiedades en la papelera'),
	        'parent_item_colon' => '',
	        'menu_name' => 'Propiedades'
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'publicly_queryable' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'query_var' => true,
	        'rewrite' => true,
	        'capability_type' => 'post',
	        'has_archive' => true,
	        'hierarchical' => false,
	        'menu_position' => null,
	        'supports' => array('title', 'thumbnail' ,'editor')
	    );

	    register_post_type('propiedades',$args);
	}
	add_action('init', 'custom_propiedades');
	
	// taxonomy tipo propiedad
	function tipo_propiedad() {
	    $labels = array(
	    "name" => _x( "Tipo", "Taxonomy General Name", "text_domain" ),
	    "singular_name" => _x( "Tipo", "Taxonomy Singular Name", "text_domain" ),
	    "menu_name" => __( "Tipo", "text_domain" ),
	    "all_items" => __( "Todas los tipos de propiedades", "text_domain" ),
	    "parent_item" => __( "Tipo de propiedad", "text_domain" ),
	    "parent_item_colon" => __( "Tipo de propiedad:", "text_domain" ),
	    "new_item_name" => __( "Nombre Nuevo tipo propiedad", "text_domain" ),
	    "add_new_item" => __( "Añadir nuevo tipo de propiedad", "text_domain" ),
	    "edit_item" => __( "Editar tipo de propiedad", "text_domain" ),
	    "update_item" => __( "Actualizar tipo de propiedad", "text_domain" ),
	    "separate_items_with_commas" => __( "Separa los tipo de propiedad con comas", "text_domain" ),
	    "search_items" => __( "Buscar tipo de propiedad", "text_domain" ),
	    "add_or_remove_items" => __( "Añadir o borrar tipo de propiedad", "text_domain" ),
	    "choose_from_most_used" => __( "Elegir entre los tipo de propiedad más utilizadas", "text_domain" ),
	    "not_found" => __( "No se encuentra", "text_domain" ),
	    );
	    $rewrite = array(
	    "slug" => "tipo",
	    "with_front" => true,
	    "hierarchical" => true,
	    );
	    $args = array(
	    "labels" => $labels,
	    "hierarchical" => true,
	    "public" => true,
	    "show_ui" => true,
	    "show_admin_column" => true,
	    "show_in_nav_menus" => true,
	    "show_tagcloud" => true,
	    "rewrite" => $rewrite,
	    );
	    register_taxonomy( "Tipo", array( "propiedades" ), $args );
	}
	add_action( "init", "tipo_propiedad", 0 );
	// taxonomy zona
	function zona() {
	    $labels = array(
	    "name" => _x( "zona", "Taxonomy General Name", "text_domain" ),
	    "singular_name" => _x( "zona", "Taxonomy Singular Name", "text_domain" ),
	    "menu_name" => __( "zona", "text_domain" ),
	    "all_items" => __( "Todas las zona", "text_domain" ),
	    "parent_item" => __( "zona", "text_domain" ),
	    "parent_item_colon" => __( "zona:", "text_domain" ),
	    "new_item_name" => __( "Nombre Nuevo tipo zona", "text_domain" ),
	    "add_new_item" => __( "Añadir nuevo tipo de zona", "text_domain" ),
	    "edit_item" => __( "Editar tipo de zona", "text_domain" ),
	    "update_item" => __( "Actualizar tipo de zona", "text_domain" ),
	    "separate_items_with_commas" => __( "Separa los tipo de zona con comas", "text_domain" ),
	    "search_items" => __( "Buscar tipo de zona", "text_domain" ),
	    "add_or_remove_items" => __( "Añadir o borrar tipo de zona", "text_domain" ),
	    "choose_from_most_used" => __( "Elegir entre los tipo de zona más utilizadas", "text_domain" ),
	    "not_found" => __( "No se encuentra", "text_domain" ),
	    );
	    $rewrite = array(
	    "slug" => "zona",
	    "with_front" => true,
	    "hierarchical" => true,
	    );
	    $args = array(
	    "labels" => $labels,
	    "hierarchical" => true,
	    "public" => true,
	    "show_ui" => true,
	    "show_admin_column" => true,
	    "show_in_nav_menus" => true,
	    "show_tagcloud" => true,
	    "rewrite" => $rewrite,
	    );
	    register_taxonomy( "zona", array( "propiedades" ), $args );
	}
	add_action( "init", "zona", 0 );
	//custom post Contenido de propiedad
	//crear cp propiedades
	function custom_elementos()
	{
	    $labels = array(
	        'name' => _x('Elementos', 'post type general name'),
	        'singular_name' => _x('Elementos', 'post type singular name'),
	        'add_new' => _x('Agregar Elementos', 'Elementos'),
	        'add_new_item' => __('Agregar nuevo Elemento'),
	        'edit_item' => __('Editar Elemento'),
	        'new_item' => __('Nuevo Elemento'),
	        'all_items' => __('Todos los Elementos'),
	        'view_item' => __('Ver Elementos'),
	        'search_items' => __('Buscar Elemento'),
	        'not_found' =>  __('No se han encontrado Elementos'),
	        'not_found_in_trash' => __('No hay Elementos en la papelera'),
	        'parent_item_colon' => '',
	        'menu_name' => 'Elementos'
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'publicly_queryable' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'query_var' => true,
	        'rewrite' => true,
	        'capability_type' => 'post',
	        'has_archive' => true,
	        'hierarchical' => false,
	        'menu_position' => null,
	        'supports' => array('title', 'thumbnail')
	    );

	    register_post_type('elementos',$args);
	}
	add_action('init', 'custom_elementos');
	//crear cp desarrollos
	function custom_desarrollos()
	{
	    $labels = array(
	        'name' => _x('Desarrollos', 'post type general name'),
	        'singular_name' => _x('Desarrollos', 'post type singular name'),
	        'add_new' => _x('Agregar Desarrollos', 'Desarrollos'),
	        'add_new_item' => __('Agregar nuevo Desarrollo'),
	        'edit_item' => __('Editar Desarrollo'),
	        'new_item' => __('Nuevo Desarrollo'),
	        'all_items' => __('Todos los Desarrollos'),
	        'view_item' => __('Ver Desarrollos'),
	        'search_items' => __('Buscar Desarrollo'),
	        'not_found' =>  __('No se han encontrado Desarrollos'),
	        'not_found_in_trash' => __('No hay Desarrollos en la papelera'),
	        'parent_item_colon' => '',
	        'menu_name' => 'Desarrollos'
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'publicly_queryable' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'query_var' => true,
	        'rewrite' => true,
	        'capability_type' => 'post',
	        'has_archive' => true,
	        'hierarchical' => false,
	        'menu_position' => null,
	        'supports' => array('title', 'thumbnail','editor')
	    );

	    register_post_type('desarrollos',$args);
	}
	add_action('init', 'custom_desarrollos');

	function my_acf_load_field( $field )
	{
	    global $post;
	    $field['choices'] = array();
	    wp_reset_query();
	    $query = new WP_Query(array('show_posts' => 100));
	    foreach($query->posts as $product_id=>$macthed_product){
	            $choices[$macthed_product->ID] = $macthed_product->post_title;
	    }
	    $field['choices'] = array();

	    if( is_array($choices) )
	    {
	        foreach( $choices as $key=>$choice )
	        {
	            $field['choices'][$key] = $choice;
	        }
	    }
	     wp_reset_query();
	    return $field;
	}
	add_filter('acf/load_field/name=elementos', 'my_acf_load_field');

?>