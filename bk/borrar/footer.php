<?php $Footer = new Footer(); ?>
<div class="wrapper large-12 medium-12 small-12 columns footer">
	<div class="large-10 large-centered medium-4 medium-centered small-12 columns">
		<div class="large-5 medium-5 small-12 columns text-right div-footer-left">
			<img src="<?php echo $Footer->logo_footer['url']; ?>" class="img-foot">
		</div>
		<div class="large-7 medium-7 small-12 columns div-footer-right">
			<label class="tipografia lefooter" style="color:<?php echo $Footer->color_footer; ?>"><?php echo $Footer->direccion; ?></label>
			<label class="tipografia telefono-footer lefooter" style="color:<?php echo $Footer->color_footer; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $Footer->telefono; ?></label>
			<div class="aviso">
				<a href="" class="tipografia lefooter" style="color:<?php echo $Footer->color_footer; ?>"><?php echo $Footer->aviso_de_privacidad; ?></a>
			</div>
			<label class="tipografia copy lefooter" style="color:<?php echo $Footer->color_footer; ?>"><?php echo $Footer->copyright; ?></label>
		</div>
	</div>
</div>
<script src="<?php bloginfo('template_url'); ?>/js/vendor/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/foundation.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/swiper.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/skrollr.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/TweenMax.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/TimelineMax.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.auto-complete.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src="<?php bloginfo('template_url'); ?>/js/index.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.magnific-popup.js"></script>
<script>
	var swiper1 = new Swiper('.s1', {
        paginationClickable: false,
        simulateTouch:false,
        //autoplay: 3500,
        nextButton: '.swn',
        prevButton: '.swp',
        spaceBetween: 30
    });
    //nice scroll
    $(document).ready(
		function() {
		$("html").niceScroll();
		}
	);
	//skroll
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	 // some code..
	}else{
		var s = skrollr.init({forceHeight: false});
	}
	//sustituir iconos
	var directoriov = "<?php bloginfo('template_url'); ?>/img/";
	$('.fa-bed').addClass('bed');
	$('.fa-bed').removeClass('fa fa-bed');
	$('.bed').html('<img src="'+directoriov+'recamara.png"/>');
	//
	$('.fa-shopping-basket').addClass('ban');
	$('.fa-shopping-basket').removeClass('fa fa-shopping-basket');
	$('.ban').html('<img src="'+directoriov+'bano.png"/>');
	//
	$('.fa-sun-o').addClass('terreno');
	$('.fa-sun-o').removeClass('fa fa-sun-o');
	$('.terreno').html('<img src="'+directoriov+'terreno.png"/>');
	//
	$('.div-destac-c').click(function(){
		var url = $(this).attr('id');
		window.location.href =''+url+'';
	});
</script>
