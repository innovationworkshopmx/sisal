<?php
	require_once('dompdf/dompdf_config.inc.php');
  //require_once($_SERVER['DOCUMENT_ROOT'].'/sisal/wp-blog-header.php') ;
  require_once('/home/sisal/public_html/wp-blog-header.php') ;
  global $current_user; 
  $role_user = $current_user->roles[0];
  $userview = $current_user->ID;
  $user = $_POST['user'];
  $sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $userview ";
  $consulta = $wpdb->get_results( $sqlc );
  $logo = "img/logoSisal.png";

  function getSubString($string, $length=NULL)
  {
      //Si no se especifica la longitud por defecto es 50
      if ($length == NULL)
          $length = 50;
      //Primero eliminamos las etiquetas html y luego cortamos el string
      $stringDisplay = substr(strip_tags($string), 0, $length);
      //Si el texto es mayor que la longitud se agrega puntos suspensivos
      if (strlen(strip_tags($string)) > $length)
          $stringDisplay .= ' ...';
      return $stringDisplay;
  }
  ///
  $concat = array();

  foreach ($consulta as $key => $milist) { 
    $IDP = $milist->id_inmueble;
    $id_p=get_field('id',$IDP);
    $preciov=get_field('precio',$IDP);
    $precio_renta=get_field('precio_renta',$IDP);
    if($preciov!=""){
      $precio = $preciov;
    }else{
      $precio = $precio_renta;
    } 
    $precio = number_format($precio, 2);
    //elementos a prop
    $elementos_cat = get_field('elementos_cat',$IDP);
    //campos obrigatorios
    $recamaras=get_field('recamaras',$IDP); 
    if($recamaras!=""){
      $recam = $recamaras;
    }
    $banos=get_field('ba',$IDP);
    if($banos!=""){
      $ban = $banos;
    }
    $temperatura=get_field('temperatura',$IDP); 
    if($temperatura!=""){
      $terreno = $temperatura;
    }
    $superficie_construccion=get_field('superficie_construccion',$IDP);
    if($superficie_construccion!=""){
      $contrunccion = $superficie_construccion;
    }
    $galeria=get_field('galeria',$IDP);
    $imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
    //contenido
    $content_post = get_post($IDP);
    $content = $content_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $t = getSubString($content, 200);
    $text = $t;
    $title = get_the_title($IDP);
    $zona = get_the_terms( $IDP, 'zona' );
    $nombre_ubicacion=$zona[0]->name;
    $nombre_ubicacion_padre = $zona[1]->name;
    $headertabla1 = '<TABLE width="700px" align="center">
             <tr>';
        $sub1_bodytabla1 = '<td valign="top">
                  <div>
                    <img src="'.$logo.'" style="width: 100px;margin-bottom: 13px;">
                  </div>
                  <div>
                    <img src="'.$imagen[0].'" style="width: 400px;">
                  </div>
                  <table>
                    <tr>
                  <td>
                      <p><strong style="color:red;font-family: sans-serif;font-size:12px;">Zona:</strong><span style="color:gray;font-family: sans-serif;font-size:12px;"> '.$nombre_ubicacion.'</span><p>
                      <p><strong style="color:red;font-family: sans-serif;font-size:12px;">Ciudad:</strong><span style="color:gray;font-family: sans-serif;font-size:12px;"> Querétaro</span></p>
                  </td>
                </tr>
                <tr style="text-align: right;">
                  <td valign="top" width="175px;">
                    <p><strong style="color:#262626;font-family: sans-serif;">Recamaras: </strong><span style="font-family: sans-serif;color:gray">'.$recam.'</span><p>
                      <p><strong style="color:#262626;font-family: sans-serif;">Baños: </strong><span style="font-family: sans-serif;color:gray">'.$ban.'</span><p>
                      <p><strong style="color:#262626;font-family: sans-serif;">Sup Terreno: </strong><span style="font-family: sans-serif;color:gray">'.$terreno.'</span><p>
                      <p><strong style="color:#262626;font-family: sans-serif;">Sub Contruida: </strong><span style="font-family: sans-serif;color:gray">'.$contrunccion.'</span><p>
                  </td>
                  <td valign="top" width="175px;"> ';
    $elemn = array();
        if (is_array($elementos_cat) || is_object($elementos_cat)){
          foreach ($elementos_cat as $keyc => $valuec) {
              $titlec = get_the_title($valuec);
              $elmc = '
                    <p><strong style="color:#262626;font-family: sans-serif;">'.$titlec.': </strong><span style="font-family: sans-serif;color:gray">SI</span><p>';
                array_push($elemn, $elmc);
          }
        }

    $elemn = implode(",", $elemn);
        $sub2_bodytabla1=str_replace(',','',$elemn);

    $sub3_bodytabla1 ='
                </td>
                
                </tr>
                  </table>
                </td>';
        $sub4_bodytabla1 = '<td rowspan="1" valign="top">
                    <h2 style="border-bottom:1px solid gray; color:#262626;font-family: sans-serif;">SISAL BIENES RAICES</h2>
                    <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/phone.PNG">(442) 223-8285</em>
                    <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/email.PNG">bienesraíces@sisal.com.mx</em>
                    <h3 style="font-weight:bold; color:#262626;font-family: sans-serif;">'.$title.'</h3>
                    <label style="font-weight:bold; color:#262626;font-family: sans-serif;">id#'.$id_p.'</label>&nbsp;&nbsp;
                    <label style="font-weight:bold; color:red;font-family: sans-serif;">$'.$precio.'</label>
                    <label style="display:block;font-family: sans-serif;color:gray">'.$content.'</label>
                </td>';
        $footertabla1 = '</tr>
          </TABLE>';

        $sub_bodytabla1 = $sub1_bodytabla1.$sub2_bodytabla1.$sub3_bodytabla1.$sub4_bodytabla1;
        $bodytabla1 = $headertabla1.$sub_bodytabla1.$footertabla1;
        ////tabla dos (galeria)
        $headertabla2 = '<TABLE width="700px" align="center">
              <tr>';
        $gal = array();
        if (is_array($galeria) || is_object($galeria)){
          foreach ($galeria as $keyg => $valueg) { 
            if($keyg<3){
              $elm2Bg = '<td>
                         <img src="'.$valueg["url"].'" style="width: 210px;height: 160px;">
                      </td>';
          array_push($gal, $elm2Bg);
        }
           }
        }
        $gal_comas = implode(",", $gal);
        $bodytabla2=str_replace(',','',$gal_comas);

    $footertabla2 = '</tr>
                </TABLE>';

    $bodytabla2 = $headertabla2.$bodytabla2.$footertabla2;
        $elm = $bodytabla1.$bodytabla2;
      //parte elemntos del producto

      array_push($concat, $elm);
  }
  $separado_por_comas = implode(",", $concat);
  $body=str_replace(',','',$separado_por_comas); 
  $header = '<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Sisal</title>
            </head>
            <body>';
  $footer = '</body>
      </html>';
	$codigo = $header.$body.$footer;
//$codigo = "";
$codigo = utf8_decode($codigo);
$dompdf = new DOMPDF();
//$dompdf->load_html($codigo);
$dompdf->load_html($codigo);
ini_set("memory_limit", "102M");
$dompdf->render();
$dompdf->stream("sisal.pdf");
?>