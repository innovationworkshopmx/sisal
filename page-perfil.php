<?php
	get_header();
	global $current_user;
	$role_user = $current_user->roles[0];
	//
	$IDuser=$current_user->ID;
	$all_meta_for_user = get_user_meta( $current_user->ID ); 
	$first_name = $all_meta_for_user['first_name'][0];
	$last_name = $all_meta_for_user['last_name'][0];
	$agencia = $all_meta_for_user['agencia'][0];
	$empresa = $all_meta_for_user['empresa'][0];
	$email = $current_user->data->user_email;
?>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin div-busqueda ">
	<div class="large-12 medium-12 small-12 columns">
		<h3 class="tipografia gray info-prop light"><i class="fa fa-angle-double-right right-filtros"></i> Mi perfil</h3>
	</div>
	<div class="row row-tutorial">
		<div class="large-12 medium-12 small-12 columns genera-div">
			<h3 class="bold beige">Mis datos</h3>
		</div>
		<?php
		if($role_user=="agentes_inmobiliarios"){ ?>
			<div class="large-12 medium-12 small-12 columns leyenda-datos">
				<label class="gray-light tipografia">Estos datos aparecerán en los PDF´s que generes.</label>
			</div>
		<?php } ?>
		<div class="large-12 medium-12 small-12 columns formulario-div paddingle">
			<?php
			if($role_user=="agentes_inmobiliarios"){ 
				$row = "4";
			}else{
				$row = "6";
			}
			?>
			<form id="form-update-data">
				<div class="large-<?php echo $row; ?> medium-<?php echo $row; ?> small-12 columns paddingle">
					<label class="beige bold">Primer nombre</label>
					<input type="text" name="nombre" id="nombre" value="<?php echo $first_name; ?>" placeholder="Nombre">
					<label class="beige bold">Apellidos</label>
					<input type="text" name="apellidos" id="apellidos" value="<?php echo $last_name; ?>" placeholder="Apellidos">
					<?php if($role_user!="agentes_inmobiliarios"){  ?>
						<label class="beige bold">Dirección de email</label>
						<input type="text" name="email" id="email" value="<?php echo $email; ?>" placeholder="Tu dirección de email">
					<?php } ?>
				</div>
				<?php if($role_user=="agentes_inmobiliarios"){  ?>
				<div class="large-4 medium-4 small-12 columns paddingle">
					<label class="beige bold">Dirección de email</label>
					<input type="text" name="email" id="email" value="<?php echo $email; ?>" placeholder="Tu dirección de email">
					<label class="beige bold">Agencia</label>
					<input type="text" name="agencia" id="agencia" value="<?php echo $agencia; ?>" placeholder="Agencia">
				</div>
				<?php } ?>
				<div class="large-<?php echo $row; ?> medium-<?php echo $row; ?> small-12 columns paddingle">
					<div class="msj-vacio">
						<div class="pico"></div>
						<label class="tipografia msj-error">Las contraseñas no coinciden</label>
					</div>
					<label class="beige bold">Escribe tu nueva contraseña</label>
					<input type="password" name="password" id="password" placeholder="********">
					<label class="beige bold">Repite tu nueva contraseña</label>
					<input type="password" name="password2" id="password2" placeholder="********">
				</div>
			</form>
			<div class="large-12 medium-12 small-12 columns paddingle div-sencambios">
				<a id="sendform">Guardar cambios</a>
			</div>
		</div>
		<?php if($role_user=="agentes_inmobiliarios"){  ?>
			<div class="large-12 medium-12 small-12 columns genera-div">
				<h3 class="bold beige">Logo tipo</h3>
			</div>
			<div class="large-12 medium-12 small-12 columns leyenda-datos">
				<div class="large-5 medium-6 small-12 columns paddingle">
					<label class="gray-light tipografia">Aquí puedes subir tu logotipo para que aparezca en tus PDF´S. Puedes omitir este paso y el PDF se generará sólo con tu nombre y datos de contacto.</label>
				</div>
			</div>
			<div class="large-12 medium-12 small-12 columns leyenda-datos">
				<div class="large-12 medium-12 small-12 columns wrapperd">
				    <div class="drop">
				      <div class="cont">
				        <i class="fa fa-upload"></i>
				        <div class="tit">
				        </div>
				        <div class="desc">
				          <h5 class="bold gray-light">Arrastra aquí tu archivo</h5>
				          <label class="tipografia gray-light">Formatos:jpg y png</label>
				          <label class="tipografia gray-light">Tamaño:90px X 90px</label>
				        </div>

				      </div>
				      <form enctype="multipart/form-data" method="POST" id="form-upl">
				      	<input type="text" value="<?php echo $IDuser; ?>" name="user" style="opacity:0">
				      	<output id="list"></output><input id="files" type="file" name="uploadedfile" />
				      </form>
				      <label class="tipografia gray-light oexplora">o explora tus archivos</label>
				    </div>
			    </div>
			    <div class="large-12 medium-12 small-12 columns div-sub-btt">
			    	<a id="subirarchivo">Subir archivo</a>
			    </div>
			</div>
		<?php } ?>
	</div>
</div>
<div class="modal-black close-modal"></div>
<div class="send-info">
	<div class="sending">
		<div class="spinner">
		  <div class="cube1"></div>
		  <div class="cube2"></div>
		</div>
		<h5 class="tipografia blanco">Guardando...</h5>
	</div>
	<div class="mensaje text-center">
		<h4 class="bold" id="titulo"></h4>
		<label class="tipografia" id="explicacion"></label>
		<a class="button secondary close-modal text-right cerrar-b">Cerrar</a>
	</div>
</div>
<?php get_footer(); ?>
<script>
	var url_upd = "<?php echo content_url('themes/sisal.git/modelos/update-data.php'); ?>";
	var url_send = "<?php echo content_url('themes/sisal.git/modelos/subir-archivo.php'); ?>";
	var user = "<?php echo $IDuser ?>";
	$('#sendform').click(function(){
		var datos = $('#form-update-data').serialize();
		var password = $('#password').val();
		var password2 = $('#password2').val();
		if(password==password2){
			$('.modal-black').fadeIn(400);
			$('.send-info').fadeIn(400);
			$.ajax({
				type:'POST',
				url:''+url_upd+'',
				data: datos+'&user='+user,
				beforeSend:function(){
				},
				success:function(resp){
					if(resp=="password"){
					}else{
						location.reload();
					}
				}
			});
			console.log(datos);
		}else{
			$('.msj-vacio').show();
			$('.msj-vacio').addClass('animated pulse');
		}
		
	});
	$('#password').click(function(){
		$('.msj-vacio').hide();
		$('.msj-vacio').removeClass('animated pulse');
	});
	$('#password2').click(function(){
		$('.msj-vacio').hide();
		$('.msj-vacio').removeClass('animated pulse');
	});
</script>