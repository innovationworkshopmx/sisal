<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/sisal/wp-blog-header.php') ;
	$user = $_POST['user'];
	$sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $user ";
	$consulta = $wpdb->get_results( $sqlc );
	//funcion recortar
	function getSubString($string, $length=NULL)
	{
	    if ($length == NULL)
	        $length = 50;
	    $stringDisplay = substr(strip_tags($string), 0, $length);
	    if (strlen(strip_tags($string)) > $length)
	        $stringDisplay .= ' ...';
	    return $stringDisplay;
	}
?>
<?php
	$elementos = array();
	foreach ($consulta as $key => $inte) {
		$IDP = $inte->id_inmueble;
		$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
		$title = get_the_title($IDP);
		$id_p=get_field('id',$IDP);
		$precio=get_field('precio',$IDP);
		$precio_venta = get_field('precio',$IDP);
		if($precio!=""){
			$precio = number_format($precio, 2);
		} 
		$precio_renta=get_field('precio_renta',$IDP);
		if($precio_renta!=""){ 
			$precio_renta = number_format($precio_renta, 2);
		}
		$precio_renta_f = get_field('precio_renta',$IDP);
		$price = $precio_venta;
		$recamaras=get_field('recamaras',$IDP); 
		$banos=get_field('ba',$IDP); 
		$temperatura=get_field('temperatura',$IDP); 
		$superficie_construccion=get_field('superficie_construccion',$IDP);
		$descipcion_corta = get_the_excerpt($IDP);
		$post = get_post($IDP);
		$permalink = $post->post_name;
		$location = get_field('mapa',$IDP);
		//contenido
		$content_post = get_post($IDP);
		$content = $content_post->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		$t = getSubString($content, 140);
		$text = $t;
		$cooderPadre = get_field('mapa_categorias',"zona_".$IDP);
		if($cooderPadre!=""){
			$latP = $cooderPadre['lat'];
			$lngP = $cooderPadre['lng'];
			$zoom = 8;
		}else{
			$latP = 23.634501;
			$lngP = -102.55278399999997;
			$zoom = 5;
		}
		//
		if($location!=""){
			$lat = $location['lat'];
			$lng = $location['lng'];
		}else{
			$lat = "";
			$lng = "";
		}
		$elemento = [
			'imagen' => $imagen[0],
            'title' => $title,
			'idp' => $id_p,
			'precio' => $precio,
			'precio_renta' => $precio_renta,
			'recamaras' => $recamaras,
			'banos' => $banos,
			'temperatura' => $temperatura,
			'descipcion_corta' => $text,
			'permalink' => $permalink,
			'lat' => $lat,
			'lng' => $lng,
        ];
        $elementos [] = $elemento;
	}
	$data = json_encode($elementos);
	echo $data;
?>