<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/sisal/wp-blog-header.php') ;
	//
	$user=$_POST['user'];
	$all_meta_for_user = get_user_meta( $user);
	$nickname = $all_meta_for_user['nickname'][0];
	$nombre = $_POST['nombre'];
	$apellidos = $_POST['apellidos'];
	$email = $_POST['email'];
	$agencia = $_POST['agencia'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	if($nombre!=""){
		update_user_meta( $user, 'first_name', esc_attr($nombre) );
	}
	if($apellidos!=""){
		update_user_meta( $user, 'last_name', esc_attr($apellidos) );
	}
	if($email!=""){
		update_user_meta( $user, 'email', esc_attr($email) );
	}
	if($agencia!=""){
		update_user_meta( $user, 'agencia', esc_attr($agencia) );
	}
	if($password!=""){
		wp_set_password( $password, $user );
		$creds = array();
		$creds['user_login'] = $nickname;
		$creds['user_password'] = $password;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
	}
?>