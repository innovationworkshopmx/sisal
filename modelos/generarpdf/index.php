<?php
	require_once('dompdf/dompdf_config.inc.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/sisal/wp-blog-header.php') ;
  //require_once('/home/sisal/public_html/wp-blog-header.php') ;
  global $current_user; 
  $role_user = $current_user->roles[0];
  $userview = $current_user->ID;
  $user = $_POST['user'];
  $sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $userview ";
  $consulta = $wpdb->get_results( $sqlc );
  $logo = "img/logoSisal.png";
  $concat = array();
  foreach ($consulta as $key => $milist) { 
    $IDP = $milist->id_inmueble;
    $id_p=get_field('id',$IDP);
    $precio=get_field('precio',$IDP);
    if($precio!=""){
      $precio = number_format($precio, 2);
    } 
    $precio_renta=get_field('precio_renta',$IDP);
    if($precio_renta!=""){ 
      $precio_renta = number_format($precio_renta, 2);
    }
    //elementos a prop
    $elementos_cat = get_field('elementos_cat',$IDP);
    //campos obrigatorios
    $recamaras=get_field('recamaras',$IDP); 
    if($recamaras!=""){
      $recam = $recamaras;
    }
    $banos=get_field('ba',$IDP);
    if($banos!=""){
      $ban = $banos;
    }
    $temperatura=get_field('temperatura',$IDP); 
    if($temperatura!=""){
      $terreno = $temperatura;
    }
    $superficie_construccion=get_field('superficie_construccion',$IDP);
    if($superficie_construccion!=""){
      $contrunccion = $superficie_construccion;
    }

    //$galeriag=get_field('galeria',$IDP);
    //$galeria = array($galeriag[0]['url'],$galeriag[1]['url'],$galeriag[2]['url']);    
    $imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
    //contenido
    $text = get_field('descripcion_corta',$IDP);
    $title = get_the_title($IDP);
    $zona = get_the_terms( $IDP, 'zona' );
    $nombre_ubicacion=$zona[0]->name;
    $nombre_ubicacion_padre = $zona[1]->name;
    $urlpermalink = esc_url( get_permalink($IDP) );
    $headerB = '<TABLE width="700px" align="center">
                <tr>
                    <td>
                      <img src="'.$imagen[0].'" style="width: 700px;height: 400px;">
                    </td>
                 </tr>
              </TABLE>
              <TABLE width="700px" align="center">
                 <tr>
                   <td valign="top" width="300px">
                    <div>
                          <img src="'.$logo.'" style="width: 100px;margin-bottom: 13px;">
                      </div>
                      
                      <h3 style="font-weight:bold; color:#262626;font-family: sans-serif;">'.$title.'</h3>
                      <label style="font-weight:bold; color:#262626;font-family: sans-serif;">id#'.$id_p.'</label>&nbsp;&nbsp;
                      <label style="font-weight:bold; color:red;font-family: sans-serif;">$'.$precio.'</label>
                      <label style="display:block;font-family: sans-serif;color:gray">'.$text.'</label>
                   </td>
                   <td valign="top">
                        <a style="border-bottom:1px solid #d6d6d6; color:#262626;font-family: sans-serif; font-size:20px;font-weight: bold;padding-right: 10px;">SISAL BIENES RAICES</a>
                        <a style="color:#2199e8" href="'.$urlpermalink.'">Ver</a>
                        <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/phone.PNG">(442) 223-8285</em>
                        <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/email.PNG">bienesraíces@sisal.com.mx</em>
                        
                        <table>
                          <tr style="text-align: right;">
                              <td valign="top" width="175px;">
                                <p><strong style="color:#262626;font-family: sans-serif;">Recamaras: </strong><span style="font-family: sans-serif;color:gray">'.$recam.'</span><p>
                                  <p><strong style="color:#262626;font-family: sans-serif;">Baños: </strong><span style="font-family: sans-serif;color:gray">'.$ban.'</span><p>
                                  <p><strong style="color:#262626;font-family: sans-serif;">Sup Terreno: </strong><span style="font-family: sans-serif;color:gray">'.$terreno.'</span><p>
                                  <p><strong style="color:#262626;font-family: sans-serif;">Sub Contruida: </strong><span style="font-family: sans-serif;color:gray">'.$contrunccion.'</span><p>
                              </td>
                              <td valign="top" width="175px;">';
                              $elemn = array();
                              if (is_array($elementos_cat) || is_object($elementos_cat)){
                                foreach ($elementos_cat as $keyc => $valuec) {
                                    $titlec = get_the_title($valuec);
                                    $elmc = '
                                          <p><strong style="color:#262626;font-family: sans-serif;">'.$titlec.': </strong><span style="font-family: sans-serif;color:gray">SI</span><p>';
                                      array_push($elemn, $elmc);
                                }
                              }
                              $elemn = implode(",", $elemn);
                              $sub2_bodytabla1=str_replace(',','',$elemn);
                               $footerb ='
                              </td>
                            </tr>
                        </table>
                    </td>
                  </tr>
                </TABLE>';
                //$bodytabla2 = $headertabla2.$bodytabla2.$footertabla2;
                $elm = $headerB.$sub2_bodytabla1.$footerb;
                //parte elemntos del producto
                array_push($concat, $elm);
  }
  $separado_por_comas = implode(",", $concat);
  $body=str_replace(',','',$separado_por_comas); 
  $header = '<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Sisal</title>
            </head>
            <body>';
  $footer = '</body>
      </html>';
	//$codigo = $header.$body.$footer;
$codigo = $header.$body.$footer;
$codigo = utf8_decode($codigo);
$dompdf = new DOMPDF();
//$dompdf->load_html($codigo);
$dompdf->load_html($codigo);
ini_set("memory_limit", "102M");
$dompdf->render();
$dompdf->stream("sisal.pdf");
?>