<?php
	require_once('dompdf/dompdf_config.inc.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/sisal/wp-blog-header.php') ;
  global $current_user; 
  $role_user = $current_user->roles[0];
  $userview = $current_user->ID;
  $user = $_POST['user'];
  $sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $userview ";
  $consulta = $wpdb->get_results( $sqlc );
  $cabecera = '<TABLE width="700px" align="center">
  <tr>
      <td valign="top" width="350px">
          <div>
            <img src="'.$logo.'" style="width: 100px;margin-bottom: 13px;">
            <h2 style="border-bottom:1px solid gray; color:#262626;font-family: sans-serif;">SISAL BIENES RAICES</h2>
            <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/phone.PNG">(442) 223-8285</em>
            <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/email.PNG">bienesraíces@sisal.com.mx</em>
          </div>
      </td>
      <td width="350px">
        <div></div>
      </td>
  </tr>
 </TABLE>';
  //
  $IDuser=$current_user->ID;
  $all_meta_for_user = get_user_meta( $current_user->ID );
  $logo = $all_meta_for_user['url_imagen'][0]; 
  function getSubString($string, $length=NULL)
  {
      //Si no se especifica la longitud por defecto es 50
      if ($length == NULL)
          $length = 50;
      //Primero eliminamos las etiquetas html y luego cortamos el string
      $stringDisplay = substr(strip_tags($string), 0, $length);
      //Si el texto es mayor que la longitud se agrega puntos suspensivos
      if (strlen(strip_tags($string)) > $length)
          $stringDisplay .= ' ...';
      return $stringDisplay;
  }
  ///
  $concat = array();

  foreach ($consulta as $key => $milist) { 
    $IDP = $milist->id_inmueble;
    $id_p=get_field('id',$IDP);
    $precio=get_field('precio',$IDP);
    if($precio!=""){
      $precio = number_format($precio, 2);
    } 
    $precio_renta=get_field('precio_renta',$IDP);
    if($precio_renta!=""){ 
      $precio_renta = number_format($precio_renta, 2);
    }
    //elementos a prop
    $elementos_cat = get_field('elementos_cat',$IDP);
    //campos obrigatorios
    $recamaras=get_field('recamaras',$IDP); 
    if($recamaras!=""){
      $recam = $recamaras;
    }
    $banos=get_field('ba',$IDP);
    if($banos!=""){
      $ban = $banos;
    }
    $temperatura=get_field('temperatura',$IDP); 
    if($temperatura!=""){
      $terreno = $temperatura;
    }
    $superficie_construccion=get_field('superficie_construccion',$IDP);
    if($superficie_construccion!=""){
      $contrunccion = $superficie_construccion;
    }
    $galeria=get_field('galeria',$IDP);
    $imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
    //contenido
    $text = get_field('descripcion_corta',$IDP);
    $title = get_the_title($IDP);
    $zona = get_the_terms( $IDP, 'zona' );
    $nombre_ubicacion=$zona[0]->name;
    $nombre_ubicacion_padre = $zona[1]->name;
    $urlpermalink = esc_url( get_permalink($IDP) );
    $headerB = '<TABLE width="700px" align="center">
                <tr>
                    <td valign="">
                      <div>
                          <img src="'.$imagen[0].'" style="width: 315px; height: 200px;">
                        </div>
                    </td>
                    <td valign="top">
                        <h3 style="font-weight:bold; color:#262626;font-family: sans-serif;">'.$title.'</h3>
                        <label style="font-weight:bold; color:#262626;font-family: sans-serif;">id#'.$id_p.'</label>&nbsp;&nbsp;
                        <label style="font-weight:bold; color:red;font-family: sans-serif;">$'.$precio.'</label>
                        <div>
                          <a><strong style="color:#262626;font-family: sans-serif;">Recamaras: </strong><span style="font-family: sans-serif;color:gray">'.$recam.'</span></a>
                          <a><strong style="color:#262626;font-family: sans-serif;">Baños: </strong><span style="font-family: sans-serif;color:gray">'.$ban.'</span><a>
                          <a><strong style="color:#262626;font-family: sans-serif;">Sup Terreno: </strong><span style="font-family: sans-serif;color:gray">'.$terreno.'mt2</span><a>
                          <a><strong style="color:#262626;font-family: sans-serif;">Sub Contruida: </strong><span style="font-family: sans-serif;color:gray">'.$contrunccion.'mt2</span><a>
                        </div>
                        <label style="display:block;font-family: sans-serif;color:gray">'.$text.'</label>
                    </td>
                </tr>
              </TABLE>';
                $elm = $headerB;
                //parte elemntos del producto
                array_push($concat, $elm);
  }
  $separado_por_comas = implode(",", $concat);
  $body=str_replace(',','',$separado_por_comas); 
  $header = '<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Sisal</title>
            </head>
            <body>';
  $footer = '</body>
      </html>';
$codigo = $header.$cabecera.$body.$footer;

$codigo = utf8_decode($codigo);
$dompdf = new DOMPDF();
//$dompdf->load_html($codigo);
$dompdf->load_html($codigo);
ini_set("memory_limit", "1024M");
$dompdf->render();
$dompdf->stream("sisal.pdf");
?>