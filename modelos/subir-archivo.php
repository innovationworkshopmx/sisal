<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/sisal/wp-blog-header.php') ;
	if ( ! function_exists( 'wp_handle_upload' ) ) {
    	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

	$user = $_POST['user'];
	$uploadedfile = $_FILES['uploadedfile'];
	$namearray = $uploadedfile['name'];
	//echo $user;
	$type = $uploadedfile['type'];
	$explode = explode("/", $type);
	$name = "00".$user.".".$explode[1];
	$tmp_name = $uploadedfile['tmp_name'];
	$error = $uploadedfile['error'];
	$size = $uploadedfile['size'];
	$foto_formada = array(
		'name'	  	=> $name,
		'type'	  	=> $type,
		'tmp_name' 	=> $tmp_name,
		'error' => $error,
		'size' => $size
	);

	$path = ABSPATH.'wp-content/themes/default/img/logo.png'; 
	
	$upload_overrides = array( 'test_form' => false );
	$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
	$ulrimagen = $movefile['url'];

	if ( $movefile && ! isset( $movefile['error'] ) ) {
	    echo "1";
	} else {
	    echo $movefile['error'];
	}
	if($name!=""){
		update_user_meta( $user, 'url_imagen', esc_attr($ulrimagen) );
	}
?>