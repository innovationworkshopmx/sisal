<?php
	class Header{
		public $logo,$color,$menu;
		public function Header(){
			$this->logo = get_field('logo', 'option');
			$this->logo = $this->logo['url'];
			$this->color = get_field('color', 'option');
			$this->menu = get_field('menu', 'option');
		}
	}
	class Footer{
		public $logo_footer,$color_footer,$direccion,$telefono,$aviso_de_privacidad,$copyright;
		public function Footer(){
			$this->logo_footer = get_field('logo_footer', 'option');
			$this->color_footer = get_field('color_footer', 'option');
			$this->direccion = get_field('direccion', 'option');
			$this->telefono = get_field('telefono', 'option');
			$this->aviso_de_privacidad = get_field('aviso_de_privacidad', 'option');
			$this->copyright = get_field('copyright', 'option');
		}
	}
?>