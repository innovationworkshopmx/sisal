<?php 
	get_header(); 
	wp_reset_query();
	//obtener get
	$arra_post = array();
	foreach($_GET as $nombre_campo => $valor){
	   $asignacion = "\$" . $nombre_campo . "='" . $valor . "';";
	   array_push($arra_post, $valor);
	   eval($asignacion);
	}
	//obtener todas las taxonomies
	$taxonomies = get_taxonomies();
	$all_tax=array();
	foreach ($taxonomies as $tax) {
		if($tax!='category' && $tax!="post_tag" && $tax!="nav_menu" && $tax!="link_category" && $tax!="post_format"){
			array_push($all_tax, $tax);
		}
	}
	//creacion de array concepts (que contiene los datos de las categorias obtenidas en el GET)
	$cont_post=0;
	$concepts = array();
	foreach ($arra_post as $value) {
		if($value!=""){
			//comparar si es numerico esto para el buscador de lugares
			if(is_numeric($value)){
				$num_tax = $value;
			}else{
				//operaciones para el buscador de lugares
				$explode=explode(",", $value);
				$tam=count($explode);

				if($tam>=2){
					$get=$tam-1;
					$zona_hijo=$explode[0];
					$zona_hijo = trim($zona_hijo);
					$zona_padre=$explode[$get];
					$zona_padre = trim($zona_padre);
					$zonaP = get_term_by( 'name', $zona_padre, 'zona' );
					$idzonaP = $zonaP->term_id;
					//$name_padre_z = $zonaP->name;
					$taxonomy_name = 'zona';
					$termchildren = get_term_children( $idzonaP, $taxonomy_name );
					foreach ( $termchildren as $child ) {
						$term = get_term_by( 'id', $child, $taxonomy_name );
						$nombre = $term->name;
						if($nombre==$zona_hijo){
							$num_tax = $term->term_id;
						}
					}
				}else{
					$zona1 = get_term_by( 'name', $value, 'zona' );
					$num_tax = $zona1->term_id;
				}
			}
			//ingreso de datos al array concepts
			foreach ($all_tax as $taxo) {
				$term = get_term_by( 'id', $num_tax, $taxo );
				if($term->taxonomy=="tipo"){
					$tax="Tipo";
				}else{
					$tax=$term->taxonomy;
				}
				print_r($term);
				$concept = array( 
                    'term_id' => $term->term_id,
                    'name' => $term->name,
                    'taxonomy' => $tax,
                    'parent' => $term->parent,
                    'count' => $term->count
                );
                $concepts[] = $concept;
			}
			$cont_post++;
		}
	}
	//cantidad de get con valor
	if( $cont_post >= 2 ){
		$consulta='AND';
	}else{
		$consulta="OR";
	}
	//creacion de array loop
	$myloops = array();
	foreach ($concepts as $key ) {
		if($key['term_id']!=""){
			$myloop = [
                'taxonomy' => $key['taxonomy'],
				'field' => 'id',
				'terms' => $key['term_id']
            ];
            $myloops [] = $myloop;
		}
	}
	//Query a base de datos
	$args = array(
		'tax_query' => $myloops
	);
	$myquery1 = new WP_Query($args);
	//contar elementos resultante de la busqueda
	$contelementos=0;
	if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
		$contelementos++;
	endwhile; endif;
?>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin div-busqueda">
	<div class="large-3 medium-3 small-12 columns">
		<h3 class="tipografia gray info-prop"><i class="fa fa-angle-double-right"></i> Filtros Aplicados</h3>
		<div class="large-12 medium-12 small-12 columns">
			<div class="p-left">
				<?php 	foreach ($concepts as $key ) { 
							if($key['term_id']!=""){ 
								if($key['parent']!=0){ 
									$term = get_term_by( 'id', $key['parent'], $key['taxonomy'] );
									?>
									<a class="tipografia gray clav-search delet" id="<?php echo $key['parent']; ?>"><?php echo $term->name; ?> <i class="fa fa-times"></i></a>
									<a class="tipografia gray clav-search son-lett delet" id="<?php echo $key['term_id']; ?>"><?php echo $key['name']; ?> <i class="fa fa-times"></i></a>
								<?php
								}else{ ?>
									<a class="tipografia gray clav-search delet" id="<?php echo $key['term_id']; ?>"><?php echo $key['name']; ?> <i class="fa fa-times"></i></a>
								<?php
								}
							} 
						}	?>
			</div>
		</div>
		<div class="large-12 medium-12 small-12 columns box-filtrado">
			<div>
				<?php 
					$cont=0;
					foreach ($concepts as $tax_entrantes ) { 
						if($tax_entrantes['term_id']!=""){ 
							if($key['parent']!=0){  
								//$term = get_term_by( 'id', $tax_entrantes['parent'], $tax_entrantes['taxonomy'] );
							}else{
								$term = get_term_by( 'id', $tax_entrantes['term_id'], $tax_entrantes['taxonomy'] );
								$idpadre = $term->term_id;
								$nombrepadre = $term->name;
								$termchildren = get_term_children( $idpadre, $tax_entrantes['taxonomy'] ); ?>
								<div class="panel-acc">
									<div class="sub-panel-acc">
										<a class="tipografia gray clav-search words-filtrado words-padre" id="<?php echo $cont; ?>"><?php echo $nombrepadre; ?> <i class="fa fa-chevron-down black"></i></a>
										<div id="p<?php echo $cont; ?>" class="acc1">
											<?php
											foreach ( $termchildren as $child ) {
												$term = get_term_by( 'id', $child, $tax_entrantes['taxonomy'] );
												$nombre = $term->name; ?>
													<a class="tipografia gray clav-search words-filtrado"><?php echo $nombre; ?></a>
											<?php
											} ?>
										</div>
									</div>
								</div>
							<?php
							}
						}
					$cont++;
					}
					?>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="tipografia gray clav-search words-filtrado words-padre" id="ejem1">Tipo de operación <i class="fa fa-chevron-down black"></i></a>
							<div id="pejem1" class="acc1">
								<a class="tipografia gray clav-search words-filtrado">Renta</a>
								<a class="tipografia gray clav-search words-filtrado">Venta</a>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="tipografia gray clav-search words-filtrado words-padre" id="ejem2">Recámaras <i class="fa fa-chevron-down black"></i></a>
							<div id="pejem2" class="acc1">
								<a class="tipografia gray clav-search words-filtrado">2-3</a>
								<a class="tipografia gray clav-search words-filtrado">4-5</a>
								<a class="tipografia gray clav-search words-filtrado">Más de 5</a>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="tipografia gray clav-search words-filtrado words-padre" id="ejem3">Estacionamiento <i class="fa fa-chevron-down black"></i></a>
							<div id="pejem3" class="acc1">
								<a class="tipografia gray clav-search words-filtrado">SI</a>
								<a class="tipografia gray clav-search words-filtrado">NO</a>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="tipografia gray clav-search words-filtrado words-padre" id="ejem4">Baños <i class="fa fa-chevron-down black"></i></a>
							<div id="pejem4" class="acc1">
								<a class="tipografia gray clav-search words-filtrado">2-3</a>
								<a class="tipografia gray clav-search words-filtrado">4-5</a>
								<a class="tipografia gray clav-search words-filtrado">Más de 5</a>
							</div>
						</div>
					</div>
					<div class="panel-acc">
						<div class="sub-panel-acc">
							<a class="tipografia gray clav-search words-filtrado words-padre" id="ejem5">Metros totales <i class="fa fa-chevron-down black"></i></a>
							<div id="pejem5" class="acc1">
								<a class="tipografia gray clav-search words-filtrado">1,100m2 - 1,600m2</a>
								<a class="tipografia gray clav-search words-filtrado">1,700m2 - 2,000m2</a>
								<a class="tipografia gray clav-search words-filtrado">Más de 2,000m2</a>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<div class="large-9 medium-9 small-12 columns">
		<div class="large-3 medium-3 small-12 columns padding0">
			<h3 class="tipografia gray"><?php echo $contelementos; ?> Resultados</h3>
		</div>
		<div class="large-8 medium-8 small-12 columns search-h-right padding0">
			<div class="large-5 medium-6 small-12 columns padding0">
				<div class="div-s">
					<h5 class="tipografia gray">ordenar por:</h5>
				</div>
				<div class="div-s">
					<select id="precio-orden">
						<option>Mayor precio</option>
						<option>Menor precio</option>
					</select>
				</div>
			</div>
			<div class="large-7 medium-6 small-12 columns padding0">
				<div class="div-s">
					<h5 class="tipografia gray">ver:</h5>
				</div>
				<div class="div-s divs-rigth">
					<div class="div-sub-s">
						<a><img src="<?php bloginfo('template_url'); ?>/img/listaIcono.png"></a>
					</div>
					<div class="div-sub-s">
						<a><img src="<?php bloginfo('template_url'); ?>/img/mapaIcono.png"></a>
					</div>
					<div class="div-sub-s">
						<a><img src="<?php bloginfo('template_url'); ?>/img/cuadrosIconos.png"></a>
					</div>
					<div class="div-sub-s">
						<a><img src="<?php bloginfo('template_url'); ?>/img/masCuadrosIcono.png"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="vistas-modelos" id="listaIcono">
			<?php 
				if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
					$IDP=$post->ID;
					$id_p=get_field('id',$IDP);
					$precio=get_field('precio',$IDP);
					if($precio!=""){
						$precio = number_format($precio, 2);
					} 
					$precio_renta=get_field('precio_renta',$IDP);
					if($precio_renta!=""){ 
						$precio_renta = number_format($precio_renta, 2);
					}
					$recamaras=get_field('recamaras',$IDP); 
					$banos=get_field('ba',$IDP); 
					$temperatura=get_field('temperatura',$IDP); 
					$galeria=get_field('galeria',$IDP); 
					$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
					<div class="large-12 medium-12 small-12 columns padding0 marg-elem">
						<div class="large-4 medium-4 small-12 columns">
							<img src="<?php echo $imagen[0]; ?>">
						</div>
						<div class="large-8 medium-8 small-12 columns">
							<div class="large-4 medium-4 small-12 columns padding0">
								<h3 class="tipografia black"><i class="fa fa-angle-double-right"></i> <?php echo get_the_title(); ?></h3>
							</div>
							<div class="large-8 medium-8 small-12 columns padding0">
								<h5 class="tipografia gray">id# <?php echo $id_p; ?></h5>
							</div>
							<div class="large-12 medium-12 small-12 columns padding0">
								<?php if($precio!=""){ ?>
									<label class="tipografia gray search-price"><span class="beige">Precio Venta:</span> $<?php echo $precio; ?> MXN</label>
								<?php } ?>
								<?php if($precio_renta!=""){ ?>
									<label class="tipografia gray search-price"><span class="beige">Precio Renta:</span> $<?php echo $precio_renta; ?> MXN</label>
								<?php } ?>
							</div>
							<?php if($recamaras!="" && $banos!="" && $temperatura!=""){ ?>
								<div class="large-12 medium-12 small-12 columns padding0">
									<div class="large-4 medium-4 small-12 columns padding0">
										<label class="gray"><i class="fa fa-bed pad-rec"></i> <span class="beige"><?php echo $recamaras; ?> Recamaras</span></label>
									</div>
									<div class="large-4 medium-4 small-12 columns padding0">
										<label class="gray"><i class="fa fa-shopping-basket pad-rec"></i> <span class="beige"><?php echo $banos; ?> Baños</span></label>
									</div>
									<div class="large-4 medium-4 small-12 columns padding0">
										<label class="gray"><i class="fa fa-sun-o pad-rec"></i> <span class="beige"><?php echo $temperatura; ?></span></label>
									</div>
								</div>
							<?php } ?>
							<div class="large-12 medium-12 small-12 columns padding0 div-descrips-search">
								<label class="tipografia gray info-prop2"><?php the_excerpt(); ?></label>
							</div>
							<div class="large-12 medium-12 small-12 columns padding0 div-prog-search">
								<a class="tipografia gray progm-visita">Programar Visita</a>
							</div>
						</div>
					</div>
				<?php
				endwhile; endif;
			?>
		</div>
		<div class="vistas-modelos" id="cuadrosIconos">
			<div class="large-12 medium-12 small-12 columns">
				<?php $my_recoment = new WP_Query( 'post_type=propiedades&meta_key=es_una_propiedad_recomendada&meta_value=1' ); 
					if (have_posts()) : while ( $myquery1->have_posts() ) : $myquery1->the_post();
						$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
						$zona = get_the_terms( $post->ID, 'zona' );
						$nombre_ubicacion=$zona[0]->name;
						$nombre_ubicacion_padre=$zona[1]->name;
						$precio=get_field('precio',$IDP);
						if($precio!=""){
							$precio = number_format($precio, 2);
						} 
						$precio_renta=get_field('precio_renta',$IDP);
						if($precio_renta!=""){ 
							$precio_renta = number_format($precio_renta, 2);
						}
					?>
						<div class="cuadros-prop">
							<div class="bg-destacados2" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
								<div class="capa-filter2">
									<div class="info-destacada2">
										<a href="<?php echo the_permalink(); ?>" class="blanco"><i class="fa fa-map-marker"></i></a>
										<a href="<?php echo the_permalink(); ?>" class="tipografia blanco title-cuadros"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?> $<?php echo $precio; ?> USD</a>
									</div>
								</div>
							</div>
						</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>
<?php
	//array 1 parametros categorias
	$catego_sends = array();
	foreach ($concepts as $key ) {
		if($key['term_id']!=""){
				/*$catego_send = [
	                'taxonomy' => $key['taxonomy'],
					'field' => 'id',
					'terms' => $key['term_id']
	            ];
	            $catego_sends [] = $catego_send;*/
			//}
	            $catego_send = [
					'taxonomy' => $key['taxonomy'],
					'field' => 'id',
					"terms" => array(
				         "padre" => $key['parent'],
				         "hijo" => $key['term_id']
				    )
			    ];
			    $catego_sends [] = $catego_send;
		}
	}
?>
<?php get_sidebar('destacados'); ?>
<?php get_footer(); ?>
<script>
	var addressValue;
	var gets = <?php echo json_encode($catego_sends); ?>;
	var cant = gets.length;
	var arr = [];
	for (var i=0; i<cant; i++) {
	    var obj = gets[i];
	    var terms = obj.terms;
	    var padrea = terms.padre;
	    var hijoa = terms.hijo;
	    arr.push(padrea);
	    arr.push(hijoa);
	}
	$('.words-padre').click(function(){
		var ID = $(this).attr("id");
		$('#p'+ID+'').slideToggle(300);
	});
	$('.delet').click(function(){
		var ID = $(this).attr("id");
		var removeItem = ID;
		arr = jQuery.grep(arr, function(value) {
		  return value != removeItem;
		});
		var cont = 0;
		var arr_url = [];
		$(arr).each(function( index, element ) {
			//var res = element.split("_");
			console.log(element);
			var indxado = "el"+cont+"="+element;
			arr_url.push(indxado);
			cont++;
		});
		var tamau = arr_url.length;
		for (var i=0; i<cant; i++) {
			var asignacion = "b=" + arr_url;
		}
		var res1 = asignacion.replace("b=", "");
		var res2 = res1.replace(",", "&"); 
		addressValue ="<?php echo bloginfo('url') ?>/search/?"+res2; 
		console.log(addressValue);
		//document.location.href = addressValue;
	});
	
</script>
