var bg_url;
var ultimo = total_galeria-1;
//por galeria
$('.div-showimg').click(function(){
	$('.div-showimg').removeClass('div-showimghover');
	bg_url = $(this).find('img').attr('src');
	var id = $(this).attr('id');
	//$('#'+id).addClass('div-showimghover');
	$('.show-img-big').removeAttr('id');
	//$('.show-img-big').addClass('animated bounceOut');
	$(".show-img-big").attr("id","imgp_"+id);
	$('.show-img-big').css("background-image", "url("+bg_url+")");  
	//$('.show-img-big').attr("src", bg_url);
});
//next
$('#next').click(function(){
	var id_img = $('.show-img-big').attr('id');
	var explode = id_img.split("_");
	var id_galeria = explode[1]; 
	id_galeria = parseInt(id_galeria);
	$('#'+id_galeria).removeClass('div-showimghover');
	var id_next = id_galeria+1;
	//si es menor al total de elementos del forearch
	if(id_next<total_galeria){
		$('#'+id_next).addClass('div-showimghover');
		bg_url = $('#'+id_next).find('img').attr('src');
		//procedimiento de cambio
		$('.show-img-big').removeAttr('id');
		$(".show-img-big").attr("id","imgp_"+id_next);
		//$('.show-img-big').attr("src", bg_url);
		$('.show-img-big').css("background-image", "url("+bg_url+")"); 
	}else{
		id_next = -1;
		$('#'+id_next).addClass('div-showimghover');
		bg_url = $('#'+id_next).find('img').attr('src');
		//procedimiento de cambio
		$('.show-img-big').removeAttr('id');
		$(".show-img-big").attr("id","imgp_"+id_next);
		//$('.show-img-big').attr("src", bg_url);
		$('.show-img-big').css("background-image", "url("+bg_url+")"); 
	}
});
//prev
$('#prev').click(function(){
	var id_img = $('.show-img-big').attr('id');
	var explode = id_img.split("_");
	var id_galeria = explode[1]; 
	id_galeria = parseInt(id_galeria);
	$('#'+id_galeria).removeClass('div-showimghover');
	var id_next = id_galeria-1;
	if(id_next==-2){
		$('#'+ultimo).addClass('div-showimghover');
		bg_url = $('#'+ultimo).find('img').attr('src');
		//procedimiento de cambio
		$('.show-img-big').removeAttr('id');
		$(".show-img-big").attr("id","imgp_"+ultimo);
		//$('.show-img-big').attr("src", bg_url);
		$('.show-img-big').css("background-image", "url("+bg_url+")"); 
	}else{
		$('#'+id_next).addClass('div-showimghover');
		bg_url = $('#'+id_next).find('img').attr('src');
		//procedimiento de cambio
		$('.show-img-big').removeAttr('id');
		$(".show-img-big").attr("id","imgp_"+id_next);
		//$('.show-img-big').attr("src", bg_url);
		$('.show-img-big').css("background-image", "url("+bg_url+")"); 
	}
});
//agrandar
$('#agrandar').click(function(){
	$('.div-img-change').addClass('galeria-modal');
	$('.show-img-big').addClass('imagen-modal');
	$('#closegale').show();
	$(this).hide();
});
$('#closegale').click(function(){
	$('.div-img-change').removeClass('galeria-modal');
	$('.show-img-big').removeClass('imagen-modal');
	$('#closegale').hide();
	$('#agrandar').show();
});
//cambiar con teclado
function callkeydownhandler(evnt) {
   var ev = (evnt) ? evnt : event;
   var code=(ev.which) ? ev.which : event.keyCode;
   console.log(code);
   if(code==39){
   		var id_img = $('.show-img-big').attr('id');
		var explode = id_img.split("_");
		var id_galeria = explode[1]; 
		id_galeria = parseInt(id_galeria);
		$('#'+id_galeria).removeClass('div-showimghover');
		var id_next = id_galeria+1;
		//si es menor al total de elementos del forearch
		if(id_next<total_galeria){
			$('#'+id_next).addClass('div-showimghover');
			bg_url = $('#'+id_next).find('img').attr('src');
			//procedimiento de cambio
			$('.show-img-big').removeAttr('id');
			$(".show-img-big").attr("id","imgp_"+id_next);
			//$('.show-img-big').attr("src", bg_url);
			$('.show-img-big').css("background-image", "url("+bg_url+")"); 
		}else{
			id_next = -1;
			$('#'+id_next).addClass('div-showimghover');
			bg_url = $('#'+id_next).find('img').attr('src');
			//procedimiento de cambio
			$('.show-img-big').removeAttr('id');
			$(".show-img-big").attr("id","imgp_"+id_next);
			//$('.show-img-big').attr("src", bg_url);
			$('.show-img-big').css("background-image", "url("+bg_url+")"); 
		}
   }
   if(code==37){
   		var id_img = $('.show-img-big').attr('id');
		var explode = id_img.split("_");
		var id_galeria = explode[1]; 
		id_galeria = parseInt(id_galeria);
		$('#'+id_galeria).removeClass('div-showimghover');
		var id_next = id_galeria-1;
		if(id_next==-2){
			$('#'+ultimo).addClass('div-showimghover');
			bg_url = $('#'+ultimo).find('img').attr('src');
			//procedimiento de cambio
			$('.show-img-big').removeAttr('id');
			$(".show-img-big").attr("id","imgp_"+ultimo);
			//$('.show-img-big').attr("src", bg_url);
			$('.show-img-big').css("background-image", "url("+bg_url+")"); 
		}else{
			$('#'+id_next).addClass('div-showimghover');
			bg_url = $('#'+id_next).find('img').attr('src');
			//procedimiento de cambio
			$('.show-img-big').removeAttr('id');
			$(".show-img-big").attr("id","imgp_"+id_next);
			//$('.show-img-big').attr("src", bg_url);
			$('.show-img-big').css("background-image", "url("+bg_url+")"); 
		}
   }
   if(code==27){
   		$('.div-img-change').removeClass('galeria-modal');
		$('.show-img-big').removeClass('imagen-modal');
		$('#closegale').hide();
		$('#agrandar').show();
   }
}
if (window.document.addEventListener) {
   window.document.addEventListener("keydown", callkeydownhandler, false);
} else {
   window.document.attachEvent("onkeydown", callkeydownhandler);
}
