<?php
	get_header();
	$valor = $_GET['v'];
	global $current_user; 
	$role_user = $current_user->roles[0];
?>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin div-busqueda ">
	<div class="large-12 medium-12 small-12 columns">
		<?php if($valor!=""){ ?>
				<h3 class="tipografia gray info-prop light"><i class="fa fa-angle-double-right right-filtros"></i> ¡Bienvenido!</h3>
		<?php }else{ ?>
				<h3 class="tipografia gray info-prop light"><i class="fa fa-angle-double-right right-filtros"></i> Tutorial</h3>
		<?php } ?>
	</div>
	<div class="row row-tutorial">
		<div class="large-12 medium-12 small-12 columns genera-div">
			<?php
			if($role_user=="agentes_inmobiliarios"){ ?>
				<h3 class="bold beige">Genera PDF's</h3>
			<?php
			}else{ ?>
				<h3 class="bold beige">Crea tus favoritos</h3>
			<?php
			}
			?>
		</div>
		<div class="large-12 medium-12 small-12 columns cuadros-exp-tutorial">
			<?php
			if($role_user=="agentes_inmobiliarios"){ ?>
			<div class="large-4 medium-4 small-12 columns">
				<label class="tipografia gray-light exp-p1">1. Al pasar el mouse sobre una de nuestras propiedades verás este botón <i class="fa fa-file-pdf-o" aria-hidden="true"></i> que te permitirá añadir esta propiedad a la cola del generador de PDF´s</label>
				<img class="img-generar" src="<?php bloginfo('template_url'); ?>/img/generpdf.PNG">
			</div>
			<div class="large-4 medium-4 small-12 columns">
				<label class="tipografia gray-light exp-p1">2. En la barra de navegación (parte superior derecha) encontrarás los controles necesarios para administrar las propiedades en cola y generar tu PDF.</label>
				<img class="img-generar" src="<?php bloginfo('template_url'); ?>/img/menu.png">
			</div>
			<div class="large-4 medium-4 small-12 columns">
				<label class="tipografia gray-light exp-p1">3. Una vez que termines y al dar clic en "Generar PDF" te proporcionaremos un solo archivo con las propiedades que haz seleccionado.</label>
				<div class="text-center">
					<img class="img-generar" src="<?php bloginfo('template_url'); ?>/img/PDF.png">
				</div>
			</div>
			<?php
			}else{ ?>
				<div class="large-4 medium-4 small-12 columns">
					<label class="tipografia gray-light exp-p1">1. Al pasar el mouse sobre una de nuestras propiedades verás este botón <i class="fa fa-star-o spac-icon" aria-hidden="true"></i> que te permitirá añadir esta propiedad a tus favoritos</label>
					<img class="img-generar" src="<?php bloginfo('template_url'); ?>/img/favoritos.PNG">
				</div>
				<div class="large-4 medium-4 small-12 columns">
					<label class="tipografia gray-light exp-p1">2. En la barra de navegación (parte superior derecha) encontrarás los controles necesarios para administrar las propiedades agregadas a tus favoritos.</label>
					<img class="img-generar" src="<?php bloginfo('template_url'); ?>/img/menu.png">
				</div>
				<div class="large-4 medium-4 small-12 columns">
				<br>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>