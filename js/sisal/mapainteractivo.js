$('#showmapaIcono').click(function(){
	$('.vistas-modelos').fadeOut(300);
	$('#mapaIcono').fadeIn(300);

	var directorio = "<?php bloginfo('template_url'); ?>/img/"
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 10,
	  center: new google.maps.LatLng(20.6122835, -100.4802583),
	  mapTypeId: google.maps.MapTypeId.ROADMAP,
	  scrollwheel: false,
	});
	var locations = [
	  ['IW antes', 20.5922759, -100.38529249999999, 4],
	  ['Templo la cruz', 20.5968277, -100.3778742, 5],
	  ['Plaza de armas', 20.592998, -100.3918433, 3],
	  ['Plaza de toros', 20.5749239, -100.4074537, 2],
	  ['Plaza boulevares', 20.6147129, -100.3894566, 1]
	];
	var infowindow = new google.maps.InfoWindow();
	var image = { url: ''+directorio+'marker_blue.png' };
	for (i = 0; i < locations.length; i++) {  
	  marker = new google.maps.Marker({
	    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	    icon: image,
	    map: map
	  });
	  google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    var popup = new google.maps.InfoWindow({
	        content:'<div id="hook">'+locations[i][0]+'</div>'
	    });
	    return function() {
	      console.log(locations[i][0]);
	      console.log(popup.content);
	      infowindow.setContent(popup.content);
	      infowindow.open(map, marker);
	    }
	  })(marker, i));
	}
});