var drop = $("input");
drop.on('dragenter', function (e) {
  $(".cont").css({
    "border": "4px dashed #09f",
    "background": "rgba(0, 153, 255, .05)"
  });
  $(".cont").css({
    "color": "#09f"
  });
}).on('dragleave dragend mouseout drop', function (e) {
  $(".cont").css({
    "border": "3px dashed #DADFE3",
    "background": "transparent"
  });
  $(".cont").css({
    "color": "#8E99A5"
  });
});

function handleFileSelect(evt) {
  var files = evt.target.files; // FileList object
  // Loop through the FileList and render image files as thumbnails.
  var name;
  for (var i = 0, f; f = files[i]; i++) {
    // Only process image files.
    if (!f.type.match('image.*')) {
      continue;
    }
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        // Render thumbnail.
        var span = document.createElement('span');
        var textHTML = ['<img class="thumb" src="', e.target.result,
                          '" title="', escape(theFile.name), '"/>'].join('');

        $('.cont').html('');
        $('.cont').html(textHTML);
        name = theFile.name
        $('#subirarchivo').click(function(){
          $('.modal-black').fadeIn(400);
          $('.send-info').fadeIn(400);
          var serlia = $('#form-upl').serialize();
          var formData = new FormData($("#form-upl")[0]);
          $.ajax({
            url: url_send,  
            type: 'POST',
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
            },
            success: function(data){
              $('.sending').hide();
              $('.mensaje').fadeIn(400);
              if(data=="1"){
                $('#titulo').text('Gracias tu logo se ha subido correctamente.');
              }else{
                $('#titulo').text('Error al subir tu logo');
                $('#explicacion').html('Inténtalo nuevamente.');
              }
            }
          });
        });
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
  }
}
$('#files').change(handleFileSelect);
$('.close-modal').click(function(){
    $('.modal-black').fadeOut(400);
    $('.send-info').fadeOut(400);
    setTimeout(function(){ 
      $('.sending').show();
      $('.mensaje').hide();
    }, 400);
  });

