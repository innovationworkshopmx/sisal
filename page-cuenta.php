<?php
	get_header(); 
	global $current_user; 
	$role_user = $current_user->roles[0];
	$userview = $current_user->ID;
	$sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $userview ";
	$consulta = $wpdb->get_results( $sqlc );
	//
	$IDuser=$current_user->ID;
	$all_meta_for_user = get_user_meta( $current_user->ID );
	$url_imagen = $all_meta_for_user['url_imagen'][0];
	//funcion recortar
	function getSubString($string, $length=NULL)
	{
	    //Si no se especifica la longitud por defecto es 50
	    if ($length == NULL)
	        $length = 50;
	    //Primero eliminamos las etiquetas html y luego cortamos el string
	    $stringDisplay = substr(strip_tags($string), 0, $length);
	    //Si el texto es mayor que la longitud se agrega puntos suspensivos
	    if (strlen(strip_tags($string)) > $length)
	        $stringDisplay .= ' ...';
	    return $stringDisplay;
	}
?>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin div-busqueda ">
	<div class="large-7 medium-7 small-12 columns">
		<div class="large-12 medium-12 small-12 columns">
			<?php
			if($role_user=="agentes_inmobiliarios"){ ?>
				<h3 class="tipografia gray info-prop light float-left"><i class="fa fa-angle-double-right right-filtros"></i> MI PDF(<span class="total-items"><?php echo count($consulta); ?></span>)</h3>
				<div class="boxgenerar">
					<?php
					if(count($consulta)>0){
						if($url_imagen!=""){ ?>
							<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexsi.php'); ?>" class="tipografia gray registrarm1 float-left" user="<?php echo $userview ; ?>">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						<?php
						}else{ ?>
							<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/index.php'); ?>" target="_blank" class="tipografia gray registrarm1 float-left" user="<?php echo $userview ; ?>">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						<?php
						}
					}
					?>
				</div>
				<a class="tipografia remove-mylist float-left borrar-add" remo="0" inmueble="" user="<?php echo $current_user->ID; ?>">Borrar todo <i class="fa fa-trash" aria-hidden="true"></i></a>
			<?php
			}else{ ?>
				<h3 class="tipografia gray info-prop light float-left"><i class="fa fa-angle-double-right right-filtros"></i> Mis favoritos(<span class="total-items"><?php echo count($consulta); ?></span>)</h3>
				<a class="tipografia borrar-add float-left">Borrar todo <i class="fa fa-trash" aria-hidden="true"></i></a>
			<?php
			} ?>
		</div>
	</div>
	<div class="large-5 medium-5 small-12 columns di-right-cuenta">
		<div class="iconos-fil">
			<div class="div-s ver-div-c">
				<h5 class="tipografia light gray vericons">ver:</h5>
			</div>
			<div class="div-s divs-rigth">
				<div class="div-sub-s">
					<a id="showlistaIcono"><img src="<?php bloginfo('template_url'); ?>/img/listaIcono.png"></a>
				</div>
				<div class="div-sub-s">
					<a id="showmapaIcono"><img src="<?php bloginfo('template_url'); ?>/img/mapaIcono.png"></a>
				</div>
				<div class="div-sub-s">
					<a id="showcuadrosIconos"><img src="<?php bloginfo('template_url'); ?>/img/cuadrosIconos.png"></a>
				</div>
				<div class="div-sub-s">
					<a id="showmasCuadrosIcono"><img src="<?php bloginfo('template_url'); ?>/img/masCuadrosIcono.png"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="large-12 medium-12 small-12 columns view-mis-inmubles">
		<div class="large-12 medium-12 small-12 columns vistas-modelos" id="listaIcono">
			<?php
			foreach ($consulta as $key => $milist) {
				$IDP = $milist->id_inmueble;
				$id_p=get_field('id',$IDP);
				$precio=get_field('precio',$IDP);
				if($precio!=""){
					$precio = number_format($precio, 2);
				} 
				$precio_renta=get_field('precio_renta',$IDP);
				if($precio_renta!=""){ 
					$precio_renta = number_format($precio_renta, 2);
				}
				$recamaras=get_field('recamaras',$IDP); 
				$banos=get_field('ba',$IDP); 
				$temperatura=get_field('temperatura',$IDP); 
				$superficie_construccion=get_field('superficie_construccion',$IDP);
				$galeria=get_field('galeria',$IDP);
				$elementos = get_field('elementos',$IDP);
				$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
				//contenido
				$content_post = get_post($IDP);
				$content = $content_post->post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				$t = getSubString($content, 200);
				$text = $t; ?>
				<div class="large-12 medium-12 small-12 columns padding0 marg-elem elmh padding0 elem-list<?php echo $IDP; ?>">
					<div class="large-4 medium-4 small-12 columns padding0L divimgop1">
						<a href="<?php echo esc_url( get_permalink($IDP) ); ?>">
							<div class="bgop1" style="background: url('<?php echo $imagen[0]; ?>')no-repeat;">
								<?php if ( is_user_logged_in() ) { ?>
										<div class="add-milist">
											<a class="tipografia blanco remove-mylist" remo="1" user="<?php echo $current_user->ID; ?>" inmueble="<?php echo $IDP; ?>">Borrar <i class="fa fa-trash" aria-hidden="true"></i></a>
										</div>
								<?php } ?>
							</div>
						</a>
					</div>
					<div class="large-6 medium-6 small-12 columns margi-lft-elmnt">
						<div class="large-12 medium-12 small-12 columns padding0">
							<a href="<?php echo esc_url( get_permalink($IDP) ); ?>"><h3 class="tipografia black title-1"><i class="fa fa-angle-double-right flech-lft"></i> <?php echo get_the_title($IDP); ?></h3></a>
							<h5 class="tipografia gray-light id1">id# <?php echo $id_p; ?></h5>
						</div>
						<div class="large-12 medium-12 small-12 columns padding0 cont-search-price">
							<?php if($precio!=""){ ?>
								<label class="tipografia gray-light search-price"><span class="beige">Precio Venta:</span> $<?php echo $precio; ?> MXN</label>
							<?php } ?>
							<?php if($precio_renta!=""){ ?>
								<label class="tipografia gray-light search-price"><span class="beige">Precio Renta:</span> $<?php echo $precio_renta; ?> MXN</label>
							<?php } ?>
						</div>
						
						<?php if($recamaras!="" && $banos!="" && $temperatura!="" &&  $superficie_construccion!=""){ ?>
							<div class="large-12 medium-12 small-12 columns padding0">
								<div class="large-3 medium-4 small-12 columns padding0">
									<label class="gray"><i class="fa fa-bed pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $recamaras; ?> Recamaras</span></label>
								</div>
								<div class="large-3 medium-4 small-12 columns padding0">
									<label class="gray"><i class="fa fa-shopping-basket pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $banos; ?> Baños</span></label>
								</div>
								<div class="large-3 medium-4 small-12 columns padding0">
									<label class="gray"><i class="fa fa-sun-o pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $temperatura; ?> ft2</span></label>
								</div>
								<div class="large-3 medium-4 small-12 columns padding0">
									<label class="gray"><i class="fa fa-sun-o pad-rec gray-light"></i> <span class="beige des-elem"><?php echo $superficie_construccion; ?> ft2</span></label>
								</div>
							</div>
						<?php } ?>
						
						<div class="large-12 medium-12 small-12 columns padding0 div-descrips-search">
							<label class="tipografia gray-light info-prop2"><?php echo $text; ?></label>
						</div>
						<div class="large-12 medium-12 small-12 columns padding0 div-prog-search margin-consvi">
							<a href="<?php echo esc_url( get_permalink($IDP) ); ?>" class="tipografia gray progm-visita">Ver inmueble</a>
						</div>
					</div>
					<div class="large-2 medium-2 small-12 columns"></div>
				</div>
			<?php
			}
			?>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos" id="mapaIcono">
			<div id="mapc"></div>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos" id="cuadrosIconos">
			<?php
			foreach ($consulta as $key => $cs) {
				$IDP = $cs->id_inmueble;
				$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
				$zona = get_the_terms( $IDP, 'zona' );
				$nombre_ubicacion=$zona[0]->name;
				$getidpadre = $zona[1]->term_id;
				$nombre_ubicacion_padre = get_field('abreviatura',"zona_".$getidpadre);
				$precio=get_field('precio',$IDP);
				if($precio!=""){
					$precio = number_format($precio, 2);
				} 
				$precio_renta=get_field('precio_renta',$IDP);
				if($precio_renta!=""){ 
					$precio_renta = number_format($precio_renta, 2);
				} ?>
				<a class="liga-single-cuenta" href="<?php echo esc_url( get_permalink($IDP) ); ?>">
					<div class="cuadros-prop1 elem-list<?php echo $IDP; ?>">
						<div class="bg-destacados2" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
							<div class="capa-filter2">
								<div class="info-destacada2">
									<a href="<?php echo esc_url( get_permalink($IDP) ); ?>" class="tipografia blanco title-cuadros"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?> <span class="preci-cadros">$<?php echo $precio; ?> USD</span></a>
								</div>
							</div>
						</div>
						<?php if ( is_user_logged_in() ) { ?>
								<div class="add-milist">
									<a class="tipografia blanco remove-mylist" remo="2" user="<?php echo $current_user->ID; ?>" inmueble="<?php echo $IDP; ?>">Borrar <i class="fa fa-trash" aria-hidden="true"></i></a>
								</div>
						<?php } ?>
					</div>
				</a>
			<?php
			}
			?>
		</div>
		<div class="large-12 medium-12 small-12 columns vistas-modelos" id="masCuadrosIcono">
			<?php
			foreach ($consulta as $key => $moc) {
					$IDP = $moc->id_inmueble;
					$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
					$id_p=get_field('id',$IDP);
					$precio=get_field('precio',$IDP);
					$zona = get_the_terms( $IDP, 'zona' );
					$getidpadre = $zona[1]->term_id;
					$nombre_ubicacion_padre = get_field('abreviatura',"zona_".$getidpadre);
					if($precio!=""){
						$precio = number_format($precio, 2);
					} 
					$precio_renta=get_field('precio_renta',$IDP);
					if($precio_renta!=""){ 
						$precio_renta = number_format($precio_renta, 2);
					}
					$recamaras=get_field('recamaras',$IDP); 
					$banos=get_field('ba',$IDP); 
					$temperatura=get_field('temperatura',$IDP); 
					$galeria=get_field('galeria',$IDP); 
					//contenido
					$content_post = get_post($IDP);
					$content = $content_post->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]&gt;', $content);
					$t = getSubString($content, 140);
					$text = $t;
					?>
					<a class="liga-single-cuenta" href="<?php echo esc_url( get_permalink($IDP) ); ?>">
						<div class="cuadros-prop elem-list<?php echo $IDP; ?>">
							<div class="bg-destacados3" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
								<?php if ( is_user_logged_in() ) { ?>
									<div class="add-milist">
										<a class="tipografia blanco remove-mylist" remo="3" user="<?php echo $current_user->ID; ?>" inmueble="<?php echo $IDP; ?>">Borrar <i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
								<?php } ?>
							</div>
							<div class="large-12 medium-12 small-12 columns padding0 bg-cafe">
								<div class="large-7 medium-7 small-12 columns padding0">
									<a href="<?php echo esc_url( get_permalink($IDP) ); ?>" class="tipografia blanco tamh5"><?php echo get_the_title($IDP); ?></a>
								</div>
								<div class="large-5 medium-5 small-12 columns text-right padding0">
									<label class="light blanco">id#<?php echo $id_p; ?></label>
								</div>
								<div class="large-6 medium-6 small-12 columns padding0">
									<label class="light blanco precio-morecuadros">Renta: <?php echo $precio_renta; ?></label>
								</div>
								<div class="large-6 medium-6 small-12 columns padding0">
									<label class="light blanco precio-morecuadros">Venta: <?php echo $precio; ?></label>
								</div>
								<div class="large-12 medium-12 small-12 columns padding0">
									<label class="light blanco"><?php echo $text; ?></label>
								</div>
								<div class="large-12 medium-12 small-12 columns padding0 text-right">
									<a href="<?php echo esc_url( get_permalink($IDP) ); ?>" class="light blanco"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?></a>
								</div>
							</div>
						</div>
					</a>
			<?php
			} ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
	$('#showmapaIcono').click(function(){
		var url_map = "<?php echo content_url('themes/sisal.git/modelos/mapa.php'); ?>";
		var user = "<?php echo $userview ?>";
		var datos = ('user='+user);
		$.ajax({
			type:'POST',
			url:''+url_map+'',
			data: datos,
			beforeSend:function(){
			},
			success:function(resp){
				var jsonString = JSON.stringify(resp);
				jsonString = eval(jsonString);
				var jsonObject = jQuery.parseJSON( jsonString );
				var locations = [];
				$.each(jsonObject, function( key, value ) {
					var imagen = value.imagen;
					var title = value.title;
					var idp = value.idp;
					var precio = value.precio;
					var precio_renta = value.precio_renta;
					var recamaras = value.recamaras;
					var banos = value.banos;
					var temperatura = value.temperatura;
					var descipcion_corta = value.descipcion_corta;
					var permalink = value.permalink;
					var lat = value.lat;
					var lng = value.lng;
					lat = parseFloat(lat);
					lng = parseFloat(lng);
					var contenido = '<div class="large-12 medium-12 small-12 columns padding0 padding0" style="height: 310px;">'+
						'<div class="large-12 medium-12 small-12 columns">'+
							'<div class="large-12 medium-12 small-12 columns backg-modal" style="background: url('+imagen+')no-repeat;"></div>'+
						'</div>'+
						'<div class="large-12 medium-12 small-12 columns">'+
							'<div class="large-12 medium-12 small-12 columns padding0">'+
								'<h3 class="tipografia black title-1"><i class="fa fa-angle-double-right"></i>'+title+'</h3>'+
								'<h5 class="tipografia gray-light id1">id# '+idp+'</h5>'+
							'</div>'+
							'<div class="large-12 medium-12 small-12 columns padding0">'+
								'<label class="tipografia gray-light search-price"><span class="beige">Precio Venta:</span> $'+precio+' MXN</label>'+
								'<label class="tipografia gray-light search-price"><span class="beige">Precio Renta:</span> $'+precio_renta+' MXN</label>'+
							'</div>'+
							
							'<div class="large-12 medium-12 small-12 columns">'+
								'<div class="large-4 medium-4 small-12 columns padding0">'+
									'<label class="gray contien-modal">'+
										'<i class="fa fa-bed pad-rec gray-light"></i>'+ 
										'<span class="beige">'+recamaras+' Recamaras</span>'+
									'</label>'+
								'</div>'+
								'<div class="large-4 medium-4 small-12 columns padding0">'+
									'<label class="gray contien-modal">'+
										'<i class="fa fa-shopping-basket pad-rec gray-light"></i>'+ 
										'<span class="beige">'+banos+' Baños</span>'+
									'</label>'+
								'</div>'+
								'<div class="large-4 medium-4 small-12 columns padding0">'+
									'<label class="gray contien-modal">'+
										'<i class="fa fa-sun-o pad-rec gray-light"></i>'+
										'<span class="beige">'+temperatura+'</span>'+
									'</label>'+
								'</div>'+
							'</div>'+
							'<div class="large-12 medium-12 small-12 columns div-decrmodal">'+
								'<label class="light gray-light">'+descipcion_corta+'</label>'+
							'</div>'+
							'<div class="large-12 medium-12 small-12 columns div-prog-search">'+
								'<a href="<?php echo bloginfo('url') ?>/propiedades/'+permalink+'" class="tipografia gray progm-visita">Ver más detalles</a>'+
							'</div>'+
						'</div>'+
					'</div>';
					locations[key] = [contenido,lat,lng]; 
				});
				$('.vistas-modelos').fadeOut(300);
				$('#mapaIcono').fadeIn(300);
			
				var directorio = "<?php bloginfo('template_url'); ?>/img/";
				var map = new google.maps.Map(document.getElementById('mapc'), {
				  zoom: 11,
				  center: new google.maps.LatLng(20.6122835, -100.4802581),
				  styles: [{"elementType":"geometry","stylers":[{"hue":"#ff4400"},{"saturation":-68},{"lightness":-4},{"gamma":0.72}]},{"featureType":"road","elementType":"labels.icon"},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"hue":"#0077ff"},{"gamma":3.1}]},{"featureType":"water","stylers":[{"hue":"#00ccff"},{"gamma":0.44},{"saturation":-33}]},{"featureType":"poi.park","stylers":[{"hue":"#44ff00"},{"saturation":-23}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"hue":"#007fff"},{"gamma":0.77},{"saturation":65},{"lightness":99}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"gamma":0.11},{"weight":5.6},{"saturation":99},{"hue":"#0091ff"},{"lightness":-86}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"lightness":-48},{"hue":"#ff5e00"},{"gamma":1.2},{"saturation":-23}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"saturation":-64},{"hue":"#ff9100"},{"lightness":16},{"gamma":0.47},{"weight":2.7}]}],
				  mapTypeId: google.maps.MapTypeId.ROADMAP,
				  scrollwheel: false,
				});
				var contenedor = '<p><img src="'+directorio+'/masCuadrosIcono.png"></img>IW antes</p>';
				var infowindow = new google.maps.InfoWindow();
				var image = { url: ''+directorio+'marker_blue.png' };
				for (i = 0; i < locations.length; i++) {  
				  marker = new google.maps.Marker({
				    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				    icon: image,
				    map: map
				  });
				  google.maps.event.addListener(marker, 'click', (function(marker, i) {
				    var popup = new google.maps.InfoWindow({
				        content:'<div id="hook">'+locations[i][0]+'</div>'
				    });
				    return function() {
				      infowindow.setContent(popup.content);
				      infowindow.open(map, marker);
				    }
				  })(marker, i));
				}
				

			}
		});
		
	});
</script>
