<?php
	get_header();
	$content_post = get_post($ID);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
?>
<div class="wrapper large-12 medium-12 small-12 columns primer-margin div-busqueda ">
	<div class="large-12 medium-12 small-12 columns">
		<h3 class="tipografia gray info-prop light"><i class="fa fa-angle-double-right right-filtros"></i> Aviso de privacidad</h3>
	</div>
	<div class="row">
		<label class="tipografia beige"><?php echo $content; ?></label>
	</div>
</div>
<?php get_footer(); ?>