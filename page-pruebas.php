<?php

  $role_user = $current_user->roles[0];
  $userview = $current_user->ID;
  $user = $_POST['user'];
  $sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $userview ";
  $consulta = $wpdb->get_results( $sqlc );
  $logo = "img/logoSisal.png";
  $concat = array();
  $cabecera = '<TABLE width="700px" align="center">
  <tr>
      <td valign="top" width="350px">
          <div>
            <img src="'.$logo.'" style="width: 100px;margin-bottom: 13px;">
            <h2 style="border-bottom:1px solid gray; color:#262626;font-family: sans-serif;">SISAL BIENES RAICES</h2>
            <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/phone.PNG">(442) 223-8285</em>
            <em style="display:block;font-family: sans-serif;color:gray;"><img src="img/email.PNG">bienesraíces@sisal.com.mx</em>
          </div>
      </td>
      <td width="350px">
        <div></div>
      </td>
  </tr>
</TABLE>';
  foreach ($consulta as $key => $milist) { 
    $IDP = $milist->id_inmueble;
    $id_p=get_field('id',$IDP);
    $precio=get_field('precio',$IDP);
    if($precio!=""){
      $precio = number_format($precio, 2);
    } 
    $precio_renta=get_field('precio_renta',$IDP);
    if($precio_renta!=""){ 
      $precio_renta = number_format($precio_renta, 2);
    }
    //elementos a prop
    $elementos_cat = get_field('elementos_cat',$IDP);
    //campos obrigatorios
    $recamaras=get_field('recamaras',$IDP); 
    if($recamaras!=""){
      $recam = $recamaras;
    }
    $banos=get_field('ba',$IDP);
    if($banos!=""){
      $ban = $banos;
    }
    $temperatura=get_field('temperatura',$IDP); 
    if($temperatura!=""){
      $terreno = $temperatura;
    }
    $superficie_construccion=get_field('superficie_construccion',$IDP);
    if($superficie_construccion!=""){
      $contrunccion = $superficie_construccion;
    }
    
    $galeriag=get_field('galeria',$IDP);
    $galeria = array($galeriag[0]['url'],$galeriag[1]['url'],$galeriag[2]['url']);
    $imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $IDP ), 'single-post-thumbnail' );
    //contenido
    $content_post = get_post($IDP);
    $content = $content_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $text = get_field('descripcion_corta',$IDP);
    $title = get_the_title($IDP);
    $zona = get_the_terms( $IDP, 'zona' );
    $nombre_ubicacion=$zona[0]->name;
    $nombre_ubicacion_padre = $zona[1]->name;
    $urlpermalink = esc_url( get_permalink($IDP) );
    $headerB = '<TABLE width="700px" align="center">
                <tr>
                    <td valign="">
                      <div>
                          <img src="'.$imagen[0].'" style="width: 315px; height: 200px;">
                        </div>
                    </td>
                    <td valign="top">
                        <h3 style="font-weight:bold; color:#262626;font-family: sans-serif;">'.$title.'</h3>
                        <label style="font-weight:bold; color:#262626;font-family: sans-serif;">id#'.$id_p.'</label>&nbsp;&nbsp;
                        <label style="font-weight:bold; color:red;font-family: sans-serif;">$'.$precio.'</label>
                        <div>
                          <a><strong style="color:#262626;font-family: sans-serif;">Recamaras: </strong><span style="font-family: sans-serif;color:gray">'.$recam.'</span></a>
                          <a><strong style="color:#262626;font-family: sans-serif;">Baños: </strong><span style="font-family: sans-serif;color:gray">'.$ban.'</span><a>
                          <a><strong style="color:#262626;font-family: sans-serif;">Sup Terreno: </strong><span style="font-family: sans-serif;color:gray">'.$terreno.'mt2</span><a>
                          <a><strong style="color:#262626;font-family: sans-serif;">Sub Contruida: </strong><span style="font-family: sans-serif;color:gray">'.$contrunccion.'mt2</span><a>
                        </div>
                        <label style="display:block;font-family: sans-serif;color:gray">'.$text.'</label>
                    </td>
                </tr>
              </TABLE>';
                $elm = $headerB;
                //parte elemntos del producto
                array_push($concat, $elm);
                print_r($elm);
  }
  $separado_por_comas = implode(",", $concat);
  $body=str_replace(',','',$separado_por_comas); 
  $header = '<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Sisal</title>
            </head>
            <body>
            ';
  $footer = '</body>
      </html>';
  $codigo = $header.$cabecera.$body.$footer;
echo $codigo;
?>