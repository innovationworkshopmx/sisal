<?php 
	$_SESSION['urls'] ="";
	get_header(); 
	wp_reset_query();
	$ID=$post->ID; 
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
	//This is page id or post id
	$content_post = get_post($ID);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	//
	$color_desarrollo = get_field('color_desarrollo',$ID);
	$logo_desarrollo = get_field('logo_desarrollo',$ID);
	$url_website = get_field('url_website',$ID);
	$galeria = get_field('galeria',$ID);
	$location = get_field('mapa_desarrollo',$ID);
	$lat = $location['lat'];
	$lng = $location['lng'];
	$desarrollos_relacionados = get_field('desarrollos_relacionados',$ID);
?>
<style>
	.div-descripcion-sing2{
		
		background: rgba(255,255,255,1);
		background: -moz-linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, <?php echo $color_desarrollo; ?> 25%, <?php echo $color_desarrollo; ?> 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,0)), color-stop(25%, <?php echo $color_desarrollo; ?>), color-stop(100%, <?php echo $color_desarrollo; ?>));
		background: -webkit-linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, <?php echo $color_desarrollo; ?> 25%, <?php echo $color_desarrollo; ?> 100%);
		background: -o-linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, <?php echo $color_desarrollo; ?> 25%, <?php echo $color_desarrollo; ?> 100%);
		background: -ms-linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, <?php echo $color_desarrollo; ?> 25%, <?php echo $color_desarrollo; ?> 100%);
		background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, <?php echo $color_desarrollo; ?> 25%, <?php echo $color_desarrollo; ?> 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='<?php echo $color_desarrollo; ?>', GradientType=0 );
	}
</style>
<div class="wrapper large-12 medium-12 small-12 columns padding0 bg-single-des" style="background: url(<?php echo $image[0]; ?>)no-repeat;">
	<div class="div-descripcion-sing2">
		<div class="large-12 medium-12 small-12 columns sub-descr-sg">
			<div class="large-8 medium-8 small-12 columns marginsm">
				<div class="large-3 medium-3 small-12 columns text-center">
					<img src="<?php echo $logo_desarrollo['url']; ?>">
				</div>
				<div class="large-9 medium-9 small-12 columns">
					<h3 class="tipografia blanco"><?php the_title(); ?></h3>
					<label class="tipografia blanco"><?php echo $content; ?></label>
					<div class="visit-div">
						<a href="<?php echo $url_website; ?>" target="_blank" class="show-web-site" style="color:<?php echo $color_desarrollo; ?>">Visitar página web</a>
					</div>
				</div>
			</div>
			<div class="large-4 medium-4 small-12 columns">
				<div class="zoom-gallery">
				 <?php foreach ($galeria as $depas) { 
				 		$title = $depas['title'];
				 		$thumbnail = $depas['sizes']['thumbnail']; 
				 		$large = $depas['sizes']['large']; ?>
				 		<a class="marca-des" href="<?php echo $large; ?>" title="<?php echo $title; ?>" style="width:193px;height:125px;">
							<img class="marca-des img-thr" src="<?php echo $thumbnail; ?>" style="padding-bottom: 5px;padding-right: 5px;">
						</a>
				 <?php } ?>
			</div>
			</div>
		</div>
	</div>
</div>
<div class="wrapper large-12 medium-12 small-12 columns padding0">
	<div id="mapd"></div>
</div>
<div class="large-12 medium-12 small-12 columns div-destacados">
	<div class="large-12 medium-12 small-12 columns">
		<h3 class="light text-center gray title-destacados"><i class="fa fa-angle-double-right"></i> Modelos <?php the_title(); ?></h3>
		<?php 

			$args = array(
			    'numberposts' => -1,
			    'post_type'   => 'propiedades',
			    'meta_key'    => 'desarrollo_al_que_pertenece',
			    'meta_value'   => $ID
			  );
			$my_recoment = new WP_Query($args);
			if (have_posts()) : while ( $my_recoment->have_posts() ) : $my_recoment->the_post();
				$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				$zona = get_the_terms( $post->ID, 'zona' );
				$nombre_ubicacion=$zona[0]->name;
				$nombre_ubicacion_padre=$zona[1]->name;
			?>
				<div class="large-3 medium-4 small-12 columns div-destac-c" id="<?php echo the_permalink(); ?>">
					<div class="bg-destacados" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
						<div class="capa-filter">
							<div class="info-destacada">
								<a href="<?php echo the_permalink(); ?>" class="light blanco"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?></a>
							</div>
						</div>
					</div>
				</div>
		<?php endwhile; endif; ?>
	</div>
</div>
<div class="large-12 medium-12 small-12 columns div-destacados" style="margin-top: 0px;">
	<div class="large-12 medium-12 small-12 columns">
		<h3 class="light text-center gray title-destacados"><i class="fa fa-angle-double-right"></i> Otros desarrollos</h3>
		<?php 
			foreach ($desarrollos_relacionados as $others_des) { //print_r($others_des);
				$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $others_des->ID ), 'single-post-thumbnail' );
				$direccion=get_field('direccion',$others_des->ID);
				$namepost = $others_des->post_name; ?>
				<div class="large-3 medium-4 small-12 columns div-destac-c" id="<?php echo home_url(); ?>/desarrollos/<?php echo $namepost; ?>">
					<div class="bg-destacados" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
						<div class="capa-filter">
							<div class="info-destacada">
								<a href="<?php echo home_url(); ?>/desarrollos/<?php echo $namepost; ?>" class="light blanco"><?php echo $direccion; ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php
			}
		?>
	</div>
</div>
<?php get_footer(); ?>
<script>
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
	});
</script>
<script>
		var lat = "<?php echo $lat ?>";
  		var lng = "<?php echo $lng ?>";
  		var color = "<?php echo $color_desarrollo ?>";
  		lat = parseFloat(lat);
		lng = parseFloat(lng);
	      function initMap() {
	        var myLatLng = {lat: lat, lng: lng};
	        var map = new google.maps.Map(document.getElementById('mapd'), {
	          zoom: 15,
	          center: myLatLng,
	          styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#6195a0"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#E8E8E8"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#e6f3d6"},{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":""+color+""},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#262626"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#262626"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#eaf6f8"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#81F1FF"}]}],
	          scrollwheel: false,
	        });

	        var marker = new google.maps.Marker({
	          position: myLatLng,
	          map: map,
	          title: 'Hello World!'
	        });
	      }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2QpIePlSoi0C6XzY3qJp7egB9BJfSS1M&callback=initMap"></script>