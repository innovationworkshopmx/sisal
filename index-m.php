<?php 
$_SESSION['urls'] =""; 
get_header();
$banner_small = get_field('banner_small', 'option'); ?>
<div class="wrapper large-12 medium-12 small-12 columns padding0">
	<div class="bg-bannersm" style="background: url(<?php echo $banner_small; ?>)100% 100% no-repeat; ?>;">
		<div class="capa-small">
			<div class="large-12 medium-12 small-12 columns div-search" data-0="bottom: 50px;" data-420="bottom: 260px;">
				<form action="<?php bloginfo('url') ?>/search" method="post" id="formsearch">
					<div class="large-7 medium-12 large-centered small-12 columns">
						<div class="large-2 medium-3 small-12 columns padding0">
							<label class="addlabels">
							<select id="cat" name="tipo_padre" class="click-form">
								<?php $tipos = get_terms('Tipo',array('hide_empty'=>false,'parent' =>0));
									foreach ($tipos as $cat) { 
										$id_cat = $cat->term_id;
										$name = $cat->name; ?>
										<option class="optionCat beige" value="<?php echo $id_cat; ?>"><?php echo $name; ?></option>
								<?php } ?>
							</select>
							</label>
						</div>
						<div class="large-3 medium-3 small-12 columns padding0" id="subcat">
							<label class="labels2">
								<select id="sub-cat" name="tipo_hijo" class="click-form">
									<option value="">Tipo de propiedad</option>
								</select>
							</label>
						</div>
						<div class="large-5 medium-6 small-12 columns padding0">
					        <input id="hero-demo" class="click-form" autofocus type="text" name="q" placeholder="Zona, Colonia" style="text-align: center">
						</div>
						<div class="large-2 medium-12 small-12 columns padding0 text-center">
							<div class="msj-vacio">
								<div class="pico"></div>
								<label class="tipografia msj-error">Ingresa alguna opción</label>
							</div>
							
							<!--<input type="search" id="search" placeholder="Search..." />-->
							<a class="button beige" id="search">Buscar <span class="icon"><i class="fa fa-search"></i></span></a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="large-12 medium-12 small-12 columns div-skrll-destacados" data-0="margin-top: 0px;" data-420="margin-top: -160px;">
	<?php get_sidebar('destacados'); ?>
	<?php get_sidebar('desarrollos'); ?>
</div>
<?php
	$zona_array = array();
	$zona = get_terms('zona',array('hide_empty'=>false));
	foreach ($zona as $value) {
		$name = $value->name;
		$parent = $value->parent;
		if($parent==0){
			$buscar=$name;
		}else{
			$term = get_term_by( 'id', $parent, 'zona' );
			$name_padre=$term->name;
			$buscar = $name.", ".$name_padre;
		}
		array_push($zona_array, $buscar);
	}
?>
<?php get_footer(); ?>
<script>
	var urlajax="<?php bloginfo('url') ?>/switch-ajax";
	//categoria propiedad
	var propiedad = $('.optionCat').val();
	var data=('padre='+propiedad);
	$.ajax({
        type:'POST',
        url:''+urlajax+'',
        data: data,
        beforeSend:function(){
        },
        success:function(resp){
        if (resp!="") {
                $('#subcat').html('');
                $('#subcat').html(resp);
            }
          }
    });
	$('select#cat').on('change',function(){
       var propiedad = $(this).val();
       var data=('padre='+propiedad);
       $.ajax({
            type:'POST',
            url:''+urlajax+'',
            data: data,
            beforeSend:function(){
            },
            success:function(resp){
            if (resp!="") {
                    $('#subcat').html('');
                    $('#subcat').html(resp);
                }
              }
        });
    });
    //aucompletado busqueda
    $(function(){
        $('#hero-demo').autoComplete({
            minChars: 1,
            source: function(term, suggest){
                term = term.toLowerCase();
                var choices = <?php echo json_encode($zona_array);?>;
                var suggestions = [];
                for (i=0;i<choices.length;i++)
                    if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                suggest(suggestions);
            }
        });

    });
    //search 
    $('#search').click(function(){
    	var addressValue;
    	var tipo_padre = $('#cat option:selected').val();
		var tipo_hijo = $('#sub-cat option:selected').val();
		var name_zona = $('#hero-demo').val();
		if(tipo_padre!="" || name_zona!="" ){
			if(tipo_hijo!=""){
				addressValue ="<?php echo bloginfo('url') ?>/search/?tipo="+tipo_hijo+"&zona="+name_zona;
			}else{
				addressValue ="<?php echo bloginfo('url') ?>/search/?tipo="+tipo_padre+"&zona="+name_zona;
			}
			document.location.href = addressValue;
		}else{
			$('.msj-vacio').show();
			$('.msj-vacio').addClass('animated pulse');
		}
    });
    $('.click-form').click(function(){
		$('.msj-vacio').hide();
		$('.msj-vacio').removeClass('animated pulse');
    });
    //after
    
    /* Get browser */
    //$.browser = /chrome/.test(navigator.userAgent.toLowerCase());
    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
    {
      	
    }
    else if(navigator.userAgent.indexOf("Chrome") != -1 )
    {
       $('.addlabels').addClass('labels');
    }
    else if(navigator.userAgent.indexOf("Safari") != -1)
    {
       
    }
    else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
         $('.addlabels').addClass('labelsf');
         $('#cat').css('text-indent','10px');
         $('#sub-cat').css('text-indent','10px');
    }
    else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
    {
  
    }  
    else 
    {
       
    }

    console.log($.browser);
    /*
    if(browser == "Firefox" ){
    	$('.addlabels').addClass('labelsf');
    }else{
    	$('.addlabels').addClass('labels');
    }*/
</script>