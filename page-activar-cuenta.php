<!DOCTYPE html>
<html>
<head>
	<title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
	<meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/foundation.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery.auto-complete.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/logo.ico" type="image/x-icon">
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/img/logo.ico" type="image/x-icon">
	<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/magnific-popup.css">
</head>
<body>
	<?php
		include 'funciones.php';
		$Header = new Header();
		if($post->post_type=="desarrollos"){
			$class = "single-header";
		}
		$id = $_GET['idconf'];
		$type = $_GET['type'];
	?>
	<div class="wrapper large-12 medium-12 small-12 columns header <?php echo $class; ?>" 
		<?php if (is_page('index') ) {  echo ' id="home-index"';}?> >
		<div class="large-3 medium-3 small-12 columns hd-left">
			<a href="<?php echo home_url(); ?>"><img src="<?php echo $Header->logo; ?>"></a>
		</div>

	</div>
	<div class="wrapper large-12 medium-12 small-12 columns margin-activar padd-bott text-center">
		<div class="row">
			<h3 class="bold beige">Gracias, estas apunto de activar tu cuenta con <span class="amarillo">Sisal</span> bienes raíces.</h3>
			<label class="gray tipografia pulsa">Pulsa activar para empezar tu cuenta SISAL.</label>
			<div>
				<a class="button" id="activar">Activar</a>
			</div>
		</div>
	</div>
	<div class="modal-black close-modal"></div>
	<div class="send-info">
		<div class="sending">
			<div class="spinner">
			  <div class="cube1"></div>
			  <div class="cube2"></div>
			</div>
			<h5 class="tipografia blanco">Activando...</h5>
		</div>
		<div class="mensaje text-center">
			<h4 class="bold" id="titulo"></h4>
			<label class="tipografia" id="explicacion"></label>
			<a class="button secondary close-modal text-right cerrar-b">Cerrar</a>
		</div>
	</div>
<?php get_footer(); ?>
<script>
var addressValue ="<?php echo bloginfo('url') ?>/tutorial/?v=1";
var url_add = "<?php echo content_url('themes/sisal.git/modelos/registro-usuario.php'); ?>";
	$('#activar').click(function(){
		$('.modal-black').fadeIn(400);
		$('.send-info').fadeIn(400);
		$('#titulo').text('');
		$('#explicacion').html('');
		var id = <?php echo $id; ?>;
		var type = "<?php echo $type ?>";
		var datos = ('id='+id+'&type='+type);
		$.ajax({
			type:'POST',
			url:''+url_add+'',
			data: datos,
			beforeSend:function(){
			},
			success:function(resp){
				if(resp=="redireccionar"){
					document.location.href = addressValue;
				}
				if(resp=="hubo un problema"){
					$('.sending').hide();
					$('.mensaje').fadeIn(400);
					$('#titulo').text('Error al activar el usuario');
					$('#explicacion').html('Te pedimos una disculpa por el inconveniente pero no se ha podido activar tu cuenta de usuario, te pedimos que vuelvas a intentarlo y de seguir con el problema contactarte a los números de SISAl.');
				}
			}
		});
	});
	$('.close-modal').click(function(){
		$('.modal-black').fadeOut(400);
		$('.send-info').fadeOut(400);
		setTimeout(function(){ 
			$('.sending').show();
			$('.mensaje').hide();
		}, 400);
	});
</script>