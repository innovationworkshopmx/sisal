<!DOCTYPE html>
<html>
<head>
	<title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
	<meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/foundation.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery.auto-complete.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/logo.ico" type="image/x-icon">
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/img/logo.ico" type="image/x-icon">
	<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/magnific-popup.css">
</head>
<body>
	<?php
		include 'funciones.php';
		$Header = new Header();
		if($post->post_type=="desarrollos"){
			$class = "single-header";
		}
		global $current_user; 
		$display_name = $current_user->display_name;
		$role_user = $current_user->roles[0];
		$userview = $current_user->ID;
		$sqlc = "SELECT * FROM usuarios_inmuebles WHERE id_usuario = $userview ";
		$consulta = $wpdb->get_results( $sqlc );
		//
		$IDuser=$current_user->ID;
		$all_meta_for_user = get_user_meta( $current_user->ID );
		$url_imagen = $all_meta_for_user['url_imagen'][0];
	?>
	<div class="wrapper large-12 medium-12 small-12 columns header <?php echo $class; ?>" 
		<?php if (is_page('index') ) {  echo ' id="home-index"';}?> >
		<div class="large-3 medium-3 small-6 columns hd-left zindexalt">
			<a href="<?php echo home_url(); ?>"><img src="<?php echo $Header->logo; ?>"></a>
		</div>
		<div class="small-6 columns text-right show-for-small-only zindexalt">
			<a class="bold beige menu-hambur"><i class="fa fa-bars" aria-hidden="true"></i></a>
		</div>
		<div class="large-9 medium-9 small-12 columns text-right header-right hide-for-small-only">
			<?php 
			if ( is_user_logged_in() ) { ?>
				<a class="tipografia menu-ligas show-name"><?php echo $display_name; ?></a>
				<div class="user-toggle view-name">
					<div class="pico2"></div>
					<a class="tipografia beige men-sm" href="<?php echo wp_logout_url( home_url() ); ?>">Cerrar sesión</a>
					<a href="<?php echo bloginfo('url') ?>/perfil" class="tipografia beige men-sm">Editar perfil</a>
					<a href="<?php echo bloginfo('url') ?>/tutorial" class="tipografia beige men-sm">Ayuda</a>
				</div>
				<?php
				if($role_user=="agentes_inmobiliarios"){ ?>
					<a class="tipografia menu-ligas mibandeja show-cuenta1">MI PDF(<span class="total-items"><?php echo count($consulta); ?></span>)</a>
					<div class="user-toggleg view-prop-add1">
						<div class="pico2"></div>
						<div class="toggl-generar">
							<a href="<?php echo bloginfo('url') ?>/cuenta" class="tipografia beige men-sm">Ver propiedades (<span class="total-items"><?php echo count($consulta); ?></span>) </a>
							<a class="tipografia remove-mylist borrar-add" remo="0" inmueble="" user="15">Borrar todo <i class="fa fa-trash" aria-hidden="true"></i></a>
						</div>
						<div class="boxgenerar">
							<?php 
							if(count($consulta)>0){
							?>
								
								<?php
								if($url_imagen!=""){ ?>
									<form class="text-left" id="formradio" name="formradio">
									  <input type="radio" name="generar" value="milogo">Mi logo<br>
									  <input type="radio" checked name="generar" value="logosisal" class="tipografia beige">Logo de Sisal<br>
									  <input type="radio" name="generar" value="ninguno" class="tipografia beige">Ninguno
									</form>
									<a class="tipografia gray registrarm1 men-sm generar-toggle generarpdf no-show show-1">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
									<a  class="tipografia gray registrarm1 men-sm generar-toggle generarpdf no-show show-2">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
									<a  class="tipografia gray registrarm1 men-sm generar-toggle generarpdf no-show show-3">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
								<?php 
								}else{ ?>
									<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/index.php'); ?>" target="_blank" class="tipografia gray registrarm1 men-sm generar-toggle generarpdf">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
								<?php
								}
							}
							?>
						</div>
					</div>
				<?php
				}else{ ?>
					<a href="<?php echo bloginfo('url') ?>/cuenta" class="tipografia menu-ligas mibandeja">Mis favoritos(<span class="total-items"><?php echo count($consulta); ?></span>)</a>
				<?php
				} ?>
				
			<?php
			}
			?>
			<?php
				$menu = $Header->menu;
				foreach ($menu as $valuem) {
					$nombre=$valuem['nombre'];
					$url=$valuem['url'];
					$id=$valuem['id'];
					if($id=="cel"){ ?>
						<a class="tipografia menu-ligas cel-header"><i class="fa fa-phone"></i> <?php echo $nombre; ?></a>
					<?php }
					if ( is_user_logged_in() ) { ?>

						
					<?php
					}else{
						if($id!="menu" && $id!="cel" && $id!="login"){ ?>
							<a class="tipografia menu-ligas" href="<?php echo $url; ?>"><?php echo $nombre; ?></a>
						<?php }
						if($id=="login"){ ?>
							<a class="tipografia menu-ligas loginc"><?php echo $nombre; ?></a>
							<div class="user-toggle view-login">
								<div class="pico2"></div>
								<div class="columns">
									<h5 class="tipografia gray info-prop light title-login"><i class="fa fa-angle-double-right right-filtros"></i> Ingresar</h5>
								</div>
								<form id="form-loginuser">
									<label class="bold beige opac-class">Usuario</label>
									<input type="text" class="opac-class" name="emaillogin" id="emaillogin" placeholder="Usuario" />
									<label class="bold beige opac-class">Contraseña</label>
									<input type="password" class="opac-class" name="passwordlogin" id="passwordlogin" placeholder="********" />
									<a class="tipografia showrecuperar">Olvide mi contraseña</a>
									<div class="olvide_passwd">
										<div class="text-center">
											<a class="showrecuperar subir"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
										</div>
										<label class="bold beige">Escribe el email con el que activaste tu cuenta Sisal</label>
										<input type="text" name="email-recuperar" id="email-recuperarb" placeholder="E-mail">
										<a class="tipografia gray buttonam men-sm recuperar" headetype="big">Recuperar</a>
										<div class="text-center div-waitbck">
											<img src="<?php bloginfo('template_url'); ?>/img/loading.gif" />
										</div>
										<div class="okrecuperado text-center">
											<h5 class="bold beige">Gracias te hemos enviado un correo electrónico.</h5>
										</div>
									</div>
								</form>
								<div class="float-login log-left opac-class">
									<a class="tipografia gray buttonam men-sm loginuse" id="">Ingresar</a>
								</div>
								<div class="float-login log-right opac-class">
									<a href="<?php echo bloginfo('url') ?>/registro" class="bold beige">¿No tienes cuenta? <span class="borlin">Haz click aquí</span></a>
								</div>
							</div>
						<?php
						}
					}
					if($id=="menu"){ ?>
						<a class="tipografia menu-ligas"><i class="fa fa-bars"></i><?php echo $nombre; ?></a>
						<?php	
					}
					
				}
			?>
		</div>
		<div class="menu-hamb text-center">
			<?php 
			if ( is_user_logged_in() ) { ?>
				<a class="tipografia menu-ligas show-name"><?php echo $display_name; ?></a>
				<div class="user-toggle view-name">
					<div class="pico2"></div>
					<a class="tipografia beige men-sm" href="<?php echo wp_logout_url( home_url() ); ?>">Cerrar sesión</a>
					<a href="<?php echo bloginfo('url') ?>/perfil" class="tipografia beige men-sm">Editar perfil</a>
					<a href="<?php echo bloginfo('url') ?>/tutorial" class="tipografia beige men-sm">Ayuda</a>
				</div>
				<?php
				if($role_user=="agentes_inmobiliarios"){ ?>
					<a class="tipografia menu-ligas mibandeja show-cuenta1">MI PDF(<span class="total-items"><?php echo count($consulta); ?></span>)</a>
					<div class="user-toggleg view-prop-add1">
						<div class="pico2"></div>
						<div class="toggl-generar">
							<a href="<?php echo bloginfo('url') ?>/cuenta" class="tipografia beige men-sm">Ver propiedades (<span class="total-items"><?php echo count($consulta); ?></span>) </a>
							<a class="tipografia remove-mylist borrar-add" remo="0" inmueble="" user="15">Borrar todo <i class="fa fa-trash" aria-hidden="true"></i></a>
						</div>
						<div class="boxgenerar">
							<?php 
							if(count($consulta)>0){
							?>
								
								<?php
								if($url_imagen!=""){ ?>
									<form class="text-left" id="formradio" name="formradio">
									  <input type="radio" name="generar" value="milogo">Mi logo<br>
									  <input type="radio" checked name="generar" value="logosisal" class="tipografia beige">Logo de Sisal<br>
									  <input type="radio" name="generar" value="ninguno" class="tipografia beige">Ninguno
									</form>
									<a class="tipografia gray registrarm1 men-sm generar-toggle generarpdf no-show show-1">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
									<a  class="tipografia gray registrarm1 men-sm generar-toggle generarpdf no-show show-2">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
									<a  class="tipografia gray registrarm1 men-sm generar-toggle generarpdf no-show show-3">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
								<?php 
								}else{ ?>
									<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/index.php'); ?>" target="_blank" class="tipografia gray registrarm1 men-sm generar-toggle generarpdf">Generar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
								<?php
								}
							}
							?>
						</div>
					</div>
				<?php
				}else{ ?>
					<a href="<?php echo bloginfo('url') ?>/cuenta" class="tipografia menu-ligas mibandeja">Mis favoritos(<span class="total-items"><?php echo count($consulta); ?></span>)</a>
				<?php
				} ?>
				
			<?php
			}
			?>
			<?php
				$menu = $Header->menu;
				foreach ($menu as $valuem) {
					$nombre=$valuem['nombre'];
					$url=$valuem['url'];
					$id=$valuem['id'];
					if ( is_user_logged_in() ) { ?>

						
					<?php
					}else{
						if($id!="menu" && $id!="cel" && $id!="login"){ ?>
							<a class="tipografia menu-ligas" href="<?php echo $url; ?>"><?php echo $nombre; ?></a>
						<?php }
						if($id=="login"){ ?>
							<a class="tipografia menu-ligas loginc"><?php echo $nombre; ?></a>
							<div class="user-toggle view-login">
								<div class="pico2"></div>
								<div class="columns">
									<h5 class="tipografia gray info-prop light title-login"><i class="fa fa-angle-double-right right-filtros"></i> Ingresar</h5>
								</div>
								<form id="form-loginuser">
									<label class="bold beige opac-class">Usuario</label>
									<input type="text" class="opac-class" name="emaillogin" id="emaillogin" placeholder="Usuario" />
									<label class="bold beige opac-class">Contraseña</label>
									<input type="password" class="opac-class" name="passwordlogin" id="passwordlogin" placeholder="********" />
									<a class="tipografia showrecuperar">Olvide mi contraseña</a>
									<div class="olvide_passwd">
										<div class="text-center">
											<a class="showrecuperar subir"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
										</div>
										<label class="bold beige">Escribe el email con el que activaste tu cuenta Sisal</label>
										<input type="text" name="email-recuperar" id="email-recuperars" placeholder="E-mail">
										<a class="tipografia gray buttonam men-sm recuperar" headetype="small">Recuperar</a>
										<div class="text-center div-waitbck">
											<img src="<?php bloginfo('template_url'); ?>/img/loading.gif" />
										</div>
										<div class="okrecuperado text-center">
											<h5 class="bold beige">Gracias te hemos enviado un correo electrónico.</h5>
										</div>
									</div>
								</form>
								<div class="float-login log-left opac-class">
									<a class="tipografia gray buttonam men-sm loginuse" id="">Ingresar</a>
								</div>
								<div class="float-login log-right opac-class">
									<a href="<?php echo bloginfo('url') ?>/registro" class="bold beige">¿No tienes cuenta? <span class="borlin">Haz click aquí</span></a>
								</div>
							</div>
						<?php
						}
					}
					if($id=="menu"){ ?>
						<a class="tipografia menu-ligas"><i class="fa fa-bars"></i><?php echo $nombre; ?></a>
						<?php	
					}
					
				}
			?>
		</div>
	</div>