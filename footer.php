<?php $Footer = new Footer(); ?>
<div class="wrapper large-12 medium-12 small-12 columns footer">
	<div class="large-10 large-centered medium-12 medium-centered small-12 columns">
		<div class="large-5 medium-5 small-12 columns large-text-right medium-text-center small-text-left div-footer-left">
			<img src="<?php echo $Footer->logo_footer['url']; ?>" class="img-foot">
		</div>
		<div class="large-7 medium-12 medium-text-center small-12 columns div-footer-right">
			<label class="tipografia lefooter" style="color:<?php echo $Footer->color_footer; ?>"><?php echo $Footer->direccion; ?></label>
			<label class="tipografia telefono-footer lefooter" style="color:<?php echo $Footer->color_footer; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $Footer->telefono; ?></label>
			<div class="aviso">
				<a href="<?php echo bloginfo('url') ?>/aviso-de-privacidad" class="tipografia lefooter" style="color:<?php echo $Footer->color_footer; ?>"><?php echo $Footer->aviso_de_privacidad; ?></a>
			</div>
			<label class="tipografia copy lefooter" style="color:<?php echo $Footer->color_footer; ?>"><?php echo $Footer->copyright; ?></label>
		</div>
	</div>
</div>
<div class="modal-black close-modal closepd"></div>
<!-- generar pdf -->
<!-- con logo sisal -->
<div class="modales-pdf pdflogo-sisal">
	<div class="large-12 medium-12 small-12 columns text-center">
		<h4 class="tipografia blanco">Selecciona el tipo de formato para tu archivo PDF</h4>
	</div>
	<div class="large-12 medium-12 small-12 columns">
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/index.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/1.png"></a>
		</div>
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexop2.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/2.png"></a>
		</div>
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexop3.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/3.png"></a>
		</div>
	</div>
	<a class="closepd closepdf"><i class="fa fa-times" aria-hidden="true"></i></a>
</div>
<!-- con logo usuario -->
<div class="modales-pdf pdflogo-milogo">
	<div class="large-12 medium-12 small-12 columns text-center">
		<h4 class="tipografia blanco">Selecciona el tipo de formato para tu archivo PDF</h4>
	</div>
	<div class="large-12 medium-12 small-12 columns">
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexsi.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/1.png"></a>
		</div>
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexsiop2.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/2.png"></a>
		</div>
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexsiop3.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/3.png"></a>
		</div>
	</div>
	<a class="closepd closepdf"><i class="fa fa-times" aria-hidden="true"></i></a>
</div>
<!-- sin logo -->
<div class="modales-pdf pdflogo-ninguno">
	<div class="large-12 medium-12 small-12 columns text-center">
		<h4 class="tipografia blanco">Selecciona el tipo de formato para tu archivo PDF</h4>
	</div>
	<div class="large-12 medium-12 small-12 columns">
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexno.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/1.png"></a>
		</div>
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexnop2.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/2.png"></a>
		</div>
		<div class="large-4 medium-4 small-12 columns">
			<a href="<?php echo content_url('themes/sisal.git/modelos/generarpdf/indexnop3.php'); ?>"><img class="selec-img" src="<?php bloginfo('template_url'); ?>/img/3.png"></a>
		</div>
	</div>
	<a class="closepd closepdf"><i class="fa fa-times" aria-hidden="true"></i></a>
</div>
<!--consulta usuario-->
<div class="send-info2">
	<div class="sending">
		<div class="spinner">
		  <div class="cube1"></div>
		  <div class="cube2"></div>
		</div>
		<h5 class="tipografia blanco">Consultando usuario...</h5>
	</div>
	<div class="mensaje text-center">
		<h4 class="bold" id="titulo"></h4>
		<label class="tipografia" id="explicacion"></label>
		<a class="button secondary close-modal text-right cerrar-b">Cerrar</a>
	</div>
</div>
<script src="<?php bloginfo('template_url'); ?>/js/vendor/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/foundation.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/swiper.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/skrollr.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/TweenMax.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/TimelineMax.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.auto-complete.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src="<?php bloginfo('template_url'); ?>/js/index.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.magnific-popup.js"></script>
<script type="text/javascript" src="https://mandrillapp.com/api/docs/js/mandrill.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/livevalidation.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/prefixfree.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/drag.js"></script>
<script>
	//show recuperar
	var m = new mandrill.Mandrill('lFwDXkU9JM-pHtBjKKUV9g');
	var url_recuperar = "<?php echo content_url('themes/sisal.git/modelos/recuperar-password.php'); ?>";
	var url_recuperar_malling = "<?php echo content_url('themes/sisal.git/modelos/malling-recuperar.php'); ?>";
	$('.showrecuperar').click(function(){
		$('.olvide_passwd').slideToggle(300);
		$('.opac-class').toggleClass('opac-class1');
		$('.div-waitbck').hide();
		$('.okrecuperado').hide();
	});
	$('.recuperar').click(function(){
		var headetype = $(this).attr('headetype');
		if(headetype=="big"){
			var emailVr = new LiveValidation('email-recuperarb');
			emailVr.add( Validate.Presence );
			emailVr.add( Validate.Email );
			var email_recu = $('#email-recuperarb').val();
		}
		if(headetype=="small"){
			var emailVr = new LiveValidation('email-recuperars');
			emailVr.add( Validate.Presence );
			emailVr.add( Validate.Email );
			var email_recu = $('#email-recuperars').val();
		}
		
		var areAllValid = LiveValidation.massValidate( [emailVr] );
		if(areAllValid!=false){ 
			
			$('.div-waitbck').show();
			var data=('recuperar='+email_recu);
			$.ajax({
				type:'POST',
				url:''+url_recuperar+'',
				data: data,
				beforeSend:function(){
				},
				success:function(resp){
					var datac=('codigo='+resp);
					$.ajax({
						type:'POST',
						url:''+url_recuperar_malling+'',
						data: datac,
						beforeSend:function(){
						},
						success:function(resp2){
							$('.div-waitbck').hide();
							$('.okrecuperado').show();
							setTimeout(function(){ 
								$('.olvide_passwd').slideUp(300);
								$('.opac-class').removeClass('opac-class1');
							}, 3000);
							
							var hmtlCod = resp2;
							var params = {
							  "message": {
							      "from_email":'bienesraices@sisal.com.mx',
							      "to":[{"email":""+email_recu+""}],
							      "subject": "Registro sisal: ",
							       "html": hmtlCod
							  }
							};
							m.messages.send(params, function(res) {
							  console.log(res);
							  $("#email-recuperar").val("");
							}, function(err) {
							  console.log(err);
							});
						}
					});
				}
			});
			//$('.modal-black').fadeIn(400);
		}
	});
	//fin recuperar contraseña
	var swiper1 = new Swiper('.s1', {
        paginationClickable: false,
        simulateTouch:false,
        //autoplay: 3500,
        nextButton: '.swn',
        prevButton: '.swp',
        spaceBetween: 30
    });
    //nice scroll
    $(document).ready(
		function() {
		$("html").niceScroll();
		}
	);
	//skroll
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	 // some code..
	}else{
		var s = skrollr.init({forceHeight: false});
	}
	//menu small
	$('.menu-hambur').click(function(){
		$('.menu-hamb').slideToggle(300);
	});
	//fin menu small
	//ver filtro en small
	$('.show-filtrado').click(function(){
		$('.box-filtrado').slideToggle(300);
	});
	//fin de ver filtros en small
	//sustituir iconos
	var directoriov = "<?php bloginfo('template_url'); ?>/img/";
	$('.fa-bed').addClass('bed');
	$('.fa-bed').removeClass('fa fa-bed');
	$('.bed').html('<img src="'+directoriov+'recamara.png"/>');
	//
	$('.fa-shopping-basket').addClass('ban');
	$('.fa-shopping-basket').removeClass('fa fa-shopping-basket');
	$('.ban').html('<img src="'+directoriov+'regadera.png"/>');
	//
	$('.fa-sun-o').addClass('terreno');
	$('.fa-sun-o').removeClass('fa fa-sun-o');
	$('.terreno').html('<img src="'+directoriov+'terreno.png"/>');
	//
	$('.div-destac-c').click(function(){
		var url = $(this).attr('id');
		window.location.href =''+url+'';
	});
	//add to my list
	var url_items = "<?php echo content_url('themes/sisal.git/modelos/total-items.php'); ?>";
	$('.add-mylist').click(function(){
		var url_ui = "<?php echo content_url('themes/sisal.git/modelos/usuario-inmueble.php'); ?>";
		var user=$(this).attr( "user" );
		var inmueble=$(this).attr( "inmueble" );
		var numadd = $(this).attr( "numadd" );
		//add to table
		var datos = ('user='+user+'&inmueble='+inmueble);
		$.ajax({
			type:'POST',
			url:''+url_ui+'',
			data: datos,
			beforeSend:function(){
			},
			success:function(resp){
				var dato = ('user='+user);
				$.ajax({
					type:'POST',
					url:''+url_items+'',
					data: dato,
					beforeSend:function(){
					},
					success:function(respi){
						var respint = parseInt(respi);
						if(respint>0){
							$('.boxgenerar').show();
						}
						$('.total-items').text('');
						$('.total-items').text(respi);
					}
				});
			}
		});
		//efecto add
		var cart = $('.mibandeja');
		var imgtodrag;
		if(numadd=="1"){
			imgtodrag = $(".this_"+inmueble+"").eq(0);
		}
		if(numadd=="2"){
			imgtodrag = $(".this2_"+inmueble+"").eq(0);
		}
		if(numadd=="3"){
			imgtodrag = $(".this3_"+inmueble+"").eq(0);
		}
		if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.7',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '1110'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 5,
                    'left': cart.offset().left + 30,
                    'width': 20,
                    'height': 20
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                $('.opacity-div').fadeIn(300);
                $('.divcar').slideDown(300);
                $('.mibandeja').addClass('animated fadeIn');
            }, 700);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }
        setTimeout(function () {
        	$('.mibandeja').removeClass('animated fadeIn');
        },1000);
	});
	$('.remove-mylist').click(function(){
		var url_rm = "<?php echo content_url('themes/sisal.git/modelos/remove.php'); ?>";
		var user=$(this).attr( "user" );
		var inmueble=$(this).attr( "inmueble" );
		var remo=$(this).attr( "remo" );
		var datos = ('user='+user+'&inmueble='+inmueble);
		$.ajax({
			type:'POST',
				url:''+url_rm+'',
				data: datos,
				beforeSend:function(){
				},
				success:function(resp){
					if(remo!="0"){
						if(remo=="1"){
							$('.elem-list'+inmueble).addClass('animated bounceOutLeft');
						}
						if(remo=="2" || remo=="3"){
							$('.elem-list'+inmueble).addClass('animated bounceOutUp');
						}
						setTimeout(function () {
				        	$('.elem-list'+inmueble).hide();
				        },700);
					}else{
						$('.view-mis-inmubles').fadeOut(400);
					}
					var dato = ('user='+user);
					$.ajax({
						type:'POST',
						url:''+url_items+'',
						data: dato,
						beforeSend:function(){
						},
						success:function(respi){
							var respint = parseInt(respi);
							if(respint==0){
								$('.boxgenerar').hide();
							}
							$('.total-items').text('');
							$('.total-items').text(respi);
						}
					});
				}
		});
	});
	//generar pdf
	$("input[name=generar]:radio").change(function () {
		var val = $(this).val();
		console.log(val);
		if(val=="milogo"){
			$('.no-show').hide();
			$('.show-2').show();
		}
		if(val=="logosisal"){
			$('.no-show').hide();
			$('.show-1').show();
		}
		if(val=="ninguno"){
			$('.no-show').hide();
			$('.show-3').show();
		}
	});
	$('.show-1').click(function(){
		$('.modal-black').fadeIn(300);
		$('.pdflogo-sisal').fadeIn(300);
	});
	$('.show-2').click(function(){
		$('.modal-black').fadeIn(300);
		$('.pdflogo-milogo').fadeIn(300);
	});
	$('.show-3').click(function(){
		$('.modal-black').fadeIn(300);
		$('.pdflogo-ninguno').fadeIn(300);
	});
	$('.closepd').click(function(){
		$('.modal-black').fadeOut(300);
		$('.modales-pdf').fadeOut(300);
	});
</script>
<script>
	$('#showlistaIcono').click(function(){
		$('.vistas-modelos').hide();
		$('#listaIcono').fadeIn(400);
	});
	$('#showcuadrosIconos').click(function(){
		$('.vistas-modelos').hide();
		$('#cuadrosIconos').fadeIn(400);
	});
	$('#showmasCuadrosIcono').click(function(){
		$('.vistas-modelos').hide();
		$('#masCuadrosIcono').fadeIn(400);
	});
	//togle header 
	$('.show-name').click(function(){
		$('.view-prop-add1').hide();
		$('.view-login').hide();
		$('.view-name').slideToggle(300);
	});
	$('.show-cuenta1').click(function(){
		$('.view-name').hide();
		$('.view-login').hide();
		$('.view-prop-add1').slideToggle(300);
	});
	$('.loginc').click(function(){
		$('.view-name').hide();
		$('.view-prop-add1').hide();
		$('.view-login').slideToggle(300);
	});
	$(document).mouseup(function (e)
	{
		/*
	    var container = $('.show-name');
	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        $(".view-name").slideUp(300);
	    }
	    var container = $('.show-cuenta1');
	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        $(".view-prop-add1").slideUp(300);
	    }*/
	});
	
	$('.loginuse').click(function(){
		var emailoV = new LiveValidation('emaillogin');
		emailoV.add( Validate.Presence );
		//emailoV.add( Validate.Email );
		var passloV = new LiveValidation('passwordlogin');
		passloV.add( Validate.Presence );
		var url_login = "<?php echo content_url('themes/sisal.git/modelos/login.php'); ?>";
		var addressValue ="<?php echo bloginfo('url') ?>/tutorial/?v=1";
		var areAllValid = LiveValidation.massValidate( [emailoV,passloV] );
		if(areAllValid!=false){ 
			$('.modal-black').fadeIn(400);
			$('.send-info2').fadeIn(400);
			$('#titulo').text('');
			$('#explicacion').html('');
			var dato = $('#form-loginuser').serialize();
			$.ajax({
				type:'POST',
				url:''+url_login+'',
				data: dato,
				beforeSend:function(){
				},
				success:function(resp){
					$('.sending').hide();
					$('.mensaje').fadeIn(400);
					if(resp==1){
						$('.sending').show();
						$('.mensaje').hide();
						document.location.href = addressValue;
					}else{
						$('#titulo').text(resp);
					}
				}
			});
		}
	});
	$('.close-modal').click(function(){
		$('.modal-black').fadeOut(400);
		$('.send-info').fadeOut(400);
		$('.send-info2').fadeOut(400);
		setTimeout(function(){ 
			$('.sending').show();
			$('.mensaje').hide();
		}, 400);
	});
</script>
