<?php 
	get_header(); 
	wp_reset_query();
	$ID=$post->ID;
	$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
	$id_p=get_field('id',$ID); 
	$precio=get_field('precio',$ID); 
	$precio = number_format($precio, 2);
	$precio_renta=get_field('precio_renta',$ID); 
	$precio_renta = number_format($precio_renta, 2);
	//
	$temperatura=get_field('temperatura',$ID);
	$superficie_construccion=get_field('superficie_construccion',$ID);
	$recamaras=get_field('recamaras',$ID); 
	$banos=get_field('ba',$ID);
	$medios_baños=get_field('medios_baños',$ID);
	$caja_de_estacionamiento=get_field('caja_de_estacionamiento',$ID);
	$cuarto_de_servicio=get_field('cuarto_de_servicio',$ID);
	//
	$galeria=get_field('galeria',$ID); 
	$elementos = get_field('elementos_cat',$ID);
	//
	$informacion_de_contacto=get_field('informacion_de_contacto',$ID); 
	$persona_asignada=get_field('persona_asignada',$ID); 
	$numero_de_telefono_asignado=get_field('numero_de_telefono_asignado',$ID); 
	$desarrollo_al_que_pertenece = get_field('desarrollo_al_que_pertenece',$ID);
	$desarrollo = $desarrollo_al_que_pertenece->post_title;
	$slug_desarrollo = $desarrollo_al_que_pertenece->post_name;
	$location = get_field('mapa',$ID);
	$lat = $location['lat'];
	$lng = $location['lng'];
	//
	$tiene_street_view = get_field('tiene_street_view',$ID);
	$propiedades_similares = get_field('propiedades_similares',$ID);
	//get session
	$link_search=$_SESSION['urls'];
?>
<div class="wrapper large-12 medium-12 small-12 columns div-container-single primer-margin">
	<div class="large-8 medium-8 small-12 columns">
		<?php 
		$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
		foreach ($crumbs as $key => $crumb) {
			if($crumb=="sisal"){ ?>
			  <a class="tipografia blue" href="<?php echo home_url(); ?>">Home/</a>
			<?php
			}else{ 
				if($crumb=="propiedades"){ 
					if($link_search!=""){ ?>
						<a class="tipografia blue" href="<?php echo $link_search; ?>">Busqueda/</a>
					<?php
					}
				}else{?>
				<a class="tipografia gray-light"><?php echo $crumb; ?></a>
		<?php
				}
			}
		}
		?>
		<div class="large-12 columns div-img-change padding0">
			<!--<div class="show-img-big" id="imgp_-1" style="background:url('<?php echo $imagen[0]; ?>')no-repeat;"></div>-->
			<img class="show-img-big" id="imgp_-1" src="<?php echo $imagen[0]; ?>">
			<div class="flechas">
				<div class="tipografia" id="prev"><label><i class="fa fa-angle-left" aria-hidden="true"></i></label></div>
				<div class="tipografia" id="next"><label><i class="fa fa-angle-right" aria-hidden="true"></i></label></div>
			</div>
			<div id="agrandar"><a><i class="fa fa-expand" aria-hidden="true"></i></a></div>
			<div id="closegale"><a><i class="fa fa-times" aria-hidden="true"></i></a></div>
		</div>
		<div class="large-12 medium-12 small-12 columns padding0 galimg">
			<div class="large-7 medium-7 small-12 columns padding0">
				<div class="large-12 medium-12 small-12 columns padding0 div-descrips info-prop div-galimg">
					<div class="div-showimg" id="-1" style="background: url(<?php echo $imagen[0]; ?>);">
						<img src="<?php echo $imagen[0]; ?>" style="display: none;">
					</div>
				<?php
					$cont = 0;
					$heightimg;
					foreach ($galeria as $imag) { 
						$heightimg = $imag['height']; ?>
						<div class="div-showimg" id="<?php echo $cont; ?>" style="background: url(<?php echo $imag['url']; ?>);">
							<img src="<?php echo $imag['url']; ?>" style="display: none;">
						</div>
				<?php 
					$cont++;
					} ?>
				</div>
				<div class="large-12 medium-12 small-12 columns padding0 contenedor-mapas">
					<h4 class="light gray info-prop"><i class="fa fa-angle-double-right"></i> Ubicación</h4>
					<div class="large-12 medium-12 small-12 columns padding0 contenedor-select">
						<div class="select-map activemap" id="map-div" onclick="initialize(1);">
							<a>Mapa</a>
						</div>
						<?php if($tiene_street_view==1){ ?>
								<div class="select-map" id="pano-div" onclick="initialize(2);">
									<a>Street view</a>
								</div>
						<?php } ?>
					</div>
					<div class="large-12 medium-12 small-12 columns padding0 contenedor-view">
						<div class="map-div mapasview">
							<div class="all-maps" id="maps"></div>
							<a onclick="initialize(3);" class="tipografia gray-light" id="vermap">Ver en Google Maps</a>
						</div>
						<div class="pano-div mapasview">
	    					<div class="all-maps" id="pano"></div>
	    				</div>
    				</div>
				</div>
			</div>
			<div class="large-5 medium-5 small-12 columns ">
				<div class="large-12 medium-12 small-12 columns div-serv-sg">
					<?php if($temperatura!=""){ ?>
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/terreno.png"/> <span class="beige light desc-sigelmnt"><?php echo $temperatura; ?> mt2</span></label>
					<?php } ?>
					<?php if($superficie_construccion!=""){ ?>
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/terreno.png"/> <span class="beige light desc-sigelmnt"><?php echo $superficie_construccion; ?> mt2</span></label>
					<?php } ?>
					<?php if($recamaras!=""){ ?>
	
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/recamara.png"/>  <span class="beige light desc-sigelmnt"><?php echo $recamaras; ?> Recamaras</span></label>
					<?php } ?>
					<?php if($banos!=""){ ?>
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/regadera.png"/> <span class="beige light desc-sigelmnt"><?php echo $banos; ?> baños</span></label>
					<?php } ?>
					<?php if($medios_baños!=""){ ?>
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/bano.png"/> <span class="beige light desc-sigelmnt"><?php echo $medios_baños; ?>  medios baños</span></label>
					<?php } ?>
					<?php if($caja_de_estacionamiento!=""){ ?>
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/garage.png"/> <span class="beige light desc-sigelmnt">SI</span></label>
					<?php } ?>
					<?php if($cuarto_de_servicio!=""){ ?>
						<label class="info-prop2 label-cant gray-light"><img src="<?php bloginfo('template_url'); ?>/img/service.png"/> <span class="beige light desc-sigelmnt">SI</span></label>
					<?php } ?>
					<?php
					if (is_array($elementos) || is_object($elementos)){
						foreach ($elementos as $ele) {
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $ele), 'single-post-thumbnail' );
							 ?>
							<label class="info-prop2 label-cant gray-light"><img src="<?php echo $image[0]; ?>" class="img-elem"> <span class="beige light desc-sigelmnt"><?php echo get_the_title($ele); ?></span></label>
						<?php
						}
					}
					?> 
				</div>
				<div class="large-12 medium-12 small-12 columns formulario-single info-prop2">
					<h4 class="light gray"><i class="fa fa-angle-double-right"></i> Formulario de Contacto</h4>
					<input class="input-form" type="text" placeholder="Nombre" id="nombre"></input>
					<input class="input-form" type="text" placeholder="Teléfono" id="telefono"></input>
					<input class="input-form" type="text" placeholder="Email" id="email"></input>
					<div class="visita-div">
						<a class="tipografia gray progm-visita">Enviar</a>
					</div>
				</div>
				<div class="large-12 medium-12 small-12 columns">
					<h4 class="light gray info-prop"><i class="fa fa-angle-double-right"></i> <?php echo $informacion_de_contacto; ?></h4>
				</div>
				<div class="large-12 medium-12 small-12 columns div-contdesc-prop-2 info-prop">
					<label class="tipografia gray label-cant"><img src="<?php bloginfo('template_url'); ?>/img/email.PNG" class="pad-rec">bienesraíces@sisal.com.mx</span></label>
					<label class="tipografia gray label-cant"><img src="<?php bloginfo('template_url'); ?>/img/phone.PNG" class="pad-rec"> <span class="light beige">(442) 223-8285</span></label>
				</div>
			</div>
		</div>
	</div>
	<div class="large-4 medium-4 small-12 columns padding0 paddinsm">
		<h3 class="tipografia black"><i class="fa fa-angle-double-right"></i> <?php echo get_the_title(); ?></h3>
		<div class="large-12 medium-12 small-12 columns div-contdesc-prop">
			<h5 class="light gray">id# <?php echo $id_p; ?></h5>
			<span  class="light beige pertenece-a">Propiedad de: <a href="<?php echo home_url(); ?>/desarrollos/<?php echo $slug_desarrollo; ?>"><?php echo $desarrollo; ?></a></span>
			<div class="large-12 medium-12 small-12 columns padding0 share">
				<label class="gray light pleft">Compartir:</label>
				<div class="shear-p">
				<a class="shear"><i class="fa fa-facebook"></i></a>
				</div>
				<div class="shear-p">
				<a class="shear"><i class="fa fa-twitter"></i></a>
				</div>
				<div class="shear-p">
				<a class="shear"><i class="fa fa-google-plus"></i></a>
				</div>
				<div class="shear-p">
				<a class="shear"><i class="fa fa-envelope"></i></a>
				</div>
			</div>
			<div class="div-info-prop">
				<?php if($precio!=""){ ?>
					<label class="light precios-single gray"><span class="beige">Precio Venta:</span> $<?php echo $precio; ?> MXN</label>
				<?php } ?>
				<?php if($precio_renta!=""){ ?>
					<label class="light precios-single gray"><span class="beige">Precio Renta:</span> $<?php echo $precio_renta; ?> MXN</label>
				<?php } ?>
			</div>
			
			<div class="large-12 medium-12 small-12 columns padding0 div-descrips">
				<label class="light gray info-prop2"><?php the_content(); ?></label>
			</div>
		</div>
	</div>
</div>
<div class="large-12 medium-12 small-12 columns div-destacados">
	<div class="large-12 medium-12 small-12 columns" style="border-top: 1px solid #D6D2D2;padding-top: 14px;">
		<?php if($propiedades_similares!=""){ ?>
				<h3 class="light text-center gray title-destacados" style="margin-bottom: 14px;"><i class="fa fa-angle-double-right"></i> Propiedades Similares</h3>
				<?php
					foreach ($propiedades_similares as $simi) {
						$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $simi->ID ), 'single-post-thumbnail' );
						$zona = get_the_terms( $simi->ID, 'zona' );
						$nombre_ubicacion=$zona[0]->name;
						$nombre_ubicacion_padre=$zona[1]->name;
					?>
						<div class="large-3 medium-4 small-12 columns div-destac-c" id="<?php echo bloginfo('url') ?>/propiedades/<?php echo $simi->post_name; ?>">
							<div class="bg-destacados" style="background: url(<?php echo $imagen[0]; ?>)no-repeat;">
								<div class="capa-filter">
									<div class="info-destacada">
										<a href="<?php echo bloginfo('url') ?>/propiedades/<?php echo $simi->post_name; ?>" class="light blanco"><?php echo $nombre_ubicacion; ?>, <?php echo $nombre_ubicacion_padre; ?></a>
									</div>
								</div>
							</div>
						</div>
				<?php } ?>
		<?php } ?>
	</div>
</div>
<div class="capmodal" onclick="initialize(4);"></div>
<div class="modal" id="modalmapa">
	<div id="mapbig"></div>
</div>
<!-- array para mapa interactivo -->
<?php
	$myqueryp1 =new WP_Query( 'post_type=propiedades' );
	$elementos = array();
	if (have_posts()) : while ( $myqueryp1->have_posts() ) : $myqueryp1->the_post();
		if($post->ID!=$ID){
			$imagen=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			$title = $post->post_title;
			$IDP=$post->ID;
			$id_p=get_field('id',$IDP);
			$precio=get_field('precio',$IDP);
			if($precio!=""){
				$precio = number_format($precio, 2);
			} 
			$precio_renta=get_field('precio_renta',$IDP);
			if($precio_renta!=""){ 
				$precio_renta = number_format($precio_renta, 2);
			}
			$recamaras=get_field('recamaras',$IDP); 
			$banos=get_field('ba',$IDP); 
			$temperatura=get_field('temperatura',$IDP); 
			$descipcion_corta = get_the_excerpt($IDP);
			$permalink = $post->post_name;
			$location = get_field('mapa',$IDP);
			if($location!=""){
				$lat2 = $location['lat'];
				$lng2 = $location['lng'];
			}else{
				$lat2 = "";
				$lng2 = "";
			}
			$elemento = [
				'imagen' => $imagen[0],
	            'title' => $title,
				'idp' => $id_p,
				'precio' => $precio,
				'precio_renta' => $precio_renta,
				'recamaras' => $recamaras,
				'banos' => $banos,
				'temperatura' => $temperatura,
				'descipcion_corta' => $descipcion_corta,
				'permalink' => $permalink,
				'lat' => $lat2,
				'lng' => $lng2,
	        ];
	        $elementos [] = $elemento;
    	}
	endwhile; endif;
?>
<!-- fin array para mapa interactivo -->
<?php get_footer(); ?>
<script>
	/*
	$(function(){
		var p=(".info-prop");
		TweenMax.staggerFrom(p, 1, 
			{
				opacity: 0,
				top:-20,

			}, .5);
	});
	$(function(){
		var p=(".info-prop2");
		TweenMax.staggerFrom(p, 1, 
			{
				opacity: 0,
				top:-20,

			}, .5);
	});
	*/
	var total_galeria = "<?php echo $cont; ?>" ;
</script>
<script src="<?php bloginfo('template_url'); ?>/js/sisal/galeria.js"></script>
<script>
	$('.select-map').click(function(){
		$('.select-map').removeClass('activemap');
		$(this).addClass('activemap');
		var id = $(this).attr('id');
	});
</script>
<script>
	var directorio = "<?php bloginfo('template_url'); ?>/img/";
	var elementos = <?php echo json_encode($elementos); ?>;
	var lat = <?php echo $lat ?>;
	var lng = <?php echo $lng ?>;
	var locations = [];
	$.each( elementos, function( key, value ) {
		var imagen = value.imagen;
		var title = value.title;
		var idp = value.idp;
		var precio = value.precio;
		var precio_renta = value.precio_renta;
		var recamaras = value.recamaras;
		var banos = value.banos;
		var temperatura = value.temperatura;
		var descipcion_corta = value.descipcion_corta;
		var permalink = value.permalink;
		var lat = value.lat;
		var lng = value.lng;
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		var contenido = '<div class="large-12 medium-12 small-12 columns padding0 padding0" style="height: 310px;">'+
			'<div class="large-12 medium-12 small-12 columns">'+
				'<div class="large-12 medium-12 small-12 columns backg-modal" style="background: url('+imagen+')no-repeat;height: 120px;"></div>'+
			'</div>'+
			'<div class="large-12 medium-12 small-12 columns">'+
				'<div class="large-12 medium-12 small-12 columns padding0">'+
					'<h3 class="tipografia black title-1"><i class="fa fa-angle-double-right"></i>'+title+'</h3>'+
					'<h5 class="tipografia gray-light id1">id# '+idp+'</h5>'+
				'</div>'+
				'<div class="large-12 medium-12 small-12 columns padding0">'+
					'<label class="tipografia gray-light search-price"><span class="beige">Precio Venta:</span> $'+precio+' MXN</label>'+
					'<label class="tipografia gray-light search-price"><span class="beige">Precio Renta:</span> $'+precio_renta+' MXN</label>'+
				'</div>'+
				
				'<div class="large-12 medium-12 small-12 columns">'+
					'<div class="large-4 medium-4 small-12 columns padding0">'+
						'<label class="gray contien-modal">'+
							'<i class="fa fa-bed pad-rec gray-light"></i>'+ 
							'<span class="beige">'+recamaras+' Recamaras</span>'+
						'</label>'+
					'</div>'+
					'<div class="large-4 medium-4 small-12 columns padding0">'+
						'<label class="gray contien-modal">'+
							'<i class="fa fa-shopping-basket pad-rec gray-light"></i>'+ 
							'<span class="beige">'+banos+' Baños</span>'+
						'</label>'+
					'</div>'+
					'<div class="large-4 medium-4 small-12 columns padding0">'+
						'<label class="gray contien-modal">'+
							'<i class="fa fa-sun-o pad-rec gray-light"></i>'+
							'<span class="beige">'+temperatura+'</span>'+
						'</label>'+
					'</div>'+
				'</div>'+
				'<div class="large-12 medium-12 small-12 columns div-prog-search" style="margin-top: 17px;margin-bottom: 10px;">'+
					'<a href="<?php echo bloginfo('url') ?>/propiedades/'+permalink+'" class="tipografia gray progm-visita">Ver más detalles</a>'+
				'</div>'+
			'</div>'+
		'</div>';
		locations[key] = [contenido,lat,lng]; 
	});
	function initialize(id) {
	 	if(id==1){
			$('#pano').hide();
			$('#vermap').show();
			$('#maps').show();
		}
		if(id==2){
			$('#maps').hide();
			$('#vermap').hide();
			$('#pano').show();
		}
		if(id==3){
			$(".capmodal").fadeIn(300);
			$('#vermap').hide();
			$(".map-div").addClass("mapabsulute");
		}
		if(id==4){
			$(".capmodal").fadeOut(300);
			$('#vermap').show();
			$(".map-div").removeClass("mapabsulute");
		}
	  	var fenway = {lat: lat, lng: lng};
	  	var map = new google.maps.Map(document.getElementById('maps'), {
	    	zoom: 12,
	    	center: fenway,
	    	styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#6195a0"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#e6f3d6"},{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f4d2c5"},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#f4f4f4"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#787878"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#eaf6f8"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#eaf6f8"}]}],
	    	scrollwheel: false,
	  	});
	  	var image1 = { url: ''+directorio+'marker_red.png' };
	  	var marker = new google.maps.Marker({
	    	position: fenway,
	    	icon: image1,
	    	map: map,
	    	title: 'Hello World!'
	  	});
	  	var panorama = new google.maps.StreetViewPanorama(
	      document.getElementById('pano'), {
	        position: fenway,
	        pov: {
	          heading: 34,
	          pitch: 10
	        }
	      });

	  	var infowindow = new google.maps.InfoWindow();
		var image = { url: ''+directorio+'point.png' };
		for (i = 0; i < locations.length; i++) {  
		  marker = new google.maps.Marker({
		    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		    icon: image,
		    map: map
		  });
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
		    var popup = new google.maps.InfoWindow({
		        content:'<div id="hook">'+locations[i][0]+'</div>'
		    });
		    return function() {
		      infowindow.setContent(popup.content);
		      infowindow.open(map, marker);
		    }
		  })(marker, i));
		}
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2QpIePlSoi0C6XzY3qJp7egB9BJfSS1M&signed_in=true&callback=initialize"></script>